# CroqueMotel *(Extra)*

Original assets from Motion Twin's CroqueMotel game ([hotel.muxxu.com](http://hotel.muxxu.com))

Licensed under [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)