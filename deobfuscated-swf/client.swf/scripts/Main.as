package
{
   import flash.Lib;
   import flash.display.MovieClip;
   import flash.system.Security;
   
   public class Main
   {
       
      
      public function Main()
      {
      }
      
      public static function main() : void
      {
         Security.allowDomain("hotel.local.muxxu.com");
         Security.allowDomain("hotel.muxxu.com");
         Security.allowDomain("hotel.es.muxxu.com");
         Security.allowDomain("hotel.Fo.muxxu.com");
         Security.allowDomain("hotel.de.muxxu.com");
         var _loc1_:MovieClip = new MovieClip();
         Lib.current.addChild(_loc1_);
         var _loc2_:Game = new Game(_loc1_);
      }
   }
}
