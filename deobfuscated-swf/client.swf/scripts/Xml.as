package
{
   import flash.Boot;
   
   public class Xml
   {
      
      public static var Element:String = "element";
      
      public static var PCData:String = "pcdata";
      
      public static var CData:String = "cdata";
      
      public static var Comment:String = "comment";
      
      public static var DocType:String = "doctype";
      
      public static var Prolog:String = "prolog";
      
      public static var Document:String = "document";
       
      
      public var parent:Xml;
      
      public var nodeType:String;
      
      public var _node:XML;
      
      public function Xml()
      {
      }
      
      public static function parse(param1:String) : Xml
      {
         var _loc5_:* = null as TypeError;
         var _loc6_:* = null as EReg;
         var _loc7_:* = null as String;
         XML.ignoreWhitespace = false;
         XML.ignoreProcessingInstructions = false;
         XML.ignoreComments = false;
         var _loc3_:String = "<__document";
         var _loc4_:XML = null;
         while(_loc4_ == null)
         {
            try
            {
               _loc4_ = new XML(_loc3_ + ">" + param1 + "</__document>");
            }
            catch(_loc_e_:TypeError)
            {
               _loc5_ = _loc_e_;
               if(_loc5_ as Error)
               {
                  Boot.lastError = _loc5_;
               }
               _loc6_ = new EReg("\"([^\"]+)\"","");
               if(_loc5_.errorID == 1083 && Boolean(_loc6_.match(_loc5_.message)))
               {
                  _loc7_ = _loc6_.matched(1);
                  _loc3_ = _loc3_ + (" xmlns:" + _loc7_ + "=\"@" + _loc7_ + "\"");
                  continue;
               }
               Boot.lastError = new Error();
               throw _loc5_;
            }
         }
         return Xml.wrap(_loc4_,Xml.Document);
      }
      
      public static function compare(param1:Xml, param2:Xml) : Boolean
      {
         return param1 == null ? param2 == null : (param2 == null ? false : param1._node == param2._node);
      }
      
      public static function createElement(param1:String) : Xml
      {
         return Xml.wrap(new XML("<" + param1 + "/>"),Xml.Element);
      }
      
      public static function createPCData(param1:String) : Xml
      {
         XML.ignoreWhitespace = false;
         return Xml.wrap(new XML(param1),Xml.PCData);
      }
      
      public static function createCData(param1:String) : Xml
      {
         return Xml.wrap(new XML("<![CDATA[" + param1 + "]]>"),Xml.CData);
      }
      
      public static function createComment(param1:String) : Xml
      {
         XML.ignoreComments = false;
         return Xml.wrap(new XML("<!--" + param1 + "-->"),Xml.Comment);
      }
      
      public static function createDocType(param1:String) : Xml
      {
         return Xml.wrap(new XML("<!DOCTYPE " + param1 + ">"),Xml.DocType);
      }
      
      public static function createProlog(param1:String) : Xml
      {
         XML.ignoreProcessingInstructions = false;
         return Xml.wrap(new XML("<?" + param1 + "?>"),Xml.Prolog);
      }
      
      public static function createDocument() : Xml
      {
         return Xml.wrap(<__document/>,Xml.Document);
      }
      
      public static function getNodeType(param1:XML) : String
      {
         var _loc2_:String = param1.nodeKind();
         if(_loc2_ == "element")
         {
            return Xml.Element;
         }
         if(_loc2_ == "text")
         {
            return Xml.PCData;
         }
         if(_loc2_ == "processing-instruction")
         {
            return Xml.Prolog;
         }
         if(_loc2_ == "comment")
         {
            return Xml.Comment;
         }
         Boot.lastError = new Error();
         throw "unimplemented node type: " + Std.string(param1.nodeType);
      }
      
      public static function wrap(param1:XML, param2:String = undefined) : Xml
      {
         var _loc3_:Xml = new Xml();
         _loc3_._node = param1;
         _loc3_.nodeType = param2 != null ? param2 : Xml.getNodeType(param1);
         return _loc3_;
      }
      
      public function wraps(param1:XMLList) : Array
      {
         var _loc5_:int = 0;
         var _loc2_:Array = [];
         var _loc3_:int = 0;
         var _loc4_:int = int(param1.length());
         while(_loc3_ < _loc4_)
         {
            _loc5_ = _loc3_++;
            _loc2_.push(Xml.wrap(param1[_loc5_]));
         }
         return _loc2_;
      }
      
      public function toString() : String
      {
         var _loc1_:* = null as String;
         XML.prettyPrinting = false;
         if(nodeType == Xml.Document)
         {
            _loc1_ = _node.toXMLString();
            _loc1_ = _loc1_.substr(int(_loc1_.indexOf(">")) + 1);
            return _loc1_.substr(0,_loc1_.length - 13);
         }
         return _node.toXMLString();
      }
      
      public function §\f\x1f\x1eZ\x03§(param1:String) : String
      {
         var _loc5_:int = 0;
         var _loc6_:* = null as XMLList;
         var _loc2_:String = nodeType;
         var _loc3_:Xml = null;
         if(_loc2_ == Xml.Element || _loc2_ == Xml.Document)
         {
            Boot.lastError = new Error();
            throw "bad nodeType";
         }
         if(_loc2_ == Xml.PCData)
         {
            _loc3_ = Xml.createPCData(param1);
         }
         else if(_loc2_ == Xml.CData)
         {
            _loc3_ = Xml.createCData(param1);
         }
         else if(_loc2_ == Xml.Comment)
         {
            _loc3_ = Xml.createComment(param1);
         }
         else if(_loc2_ == Xml.DocType)
         {
            _loc3_ = Xml.createDocType(param1);
         }
         else
         {
            _loc3_ = Xml.createProlog(param1);
         }
         var _loc4_:XML = _node.parent();
         if(_loc4_ != null)
         {
            _loc4_.insertChildAfter(_node,_loc3_._node);
            _loc5_ = int(_node.childIndex());
            _loc6_ = _loc4_.children();
            delete _loc6_[Reflect.fields(_loc6_)[_loc5_]];
         }
         _node = _loc3_._node;
         return param1;
      }
      
      public function §|@&\x01\x01§(param1:String) : String
      {
         if(nodeType != Xml.Element)
         {
            Boot.lastError = new Error();
            throw "bad nodeType";
         }
         var _loc2_:Array = param1.split(":");
         if(int(_loc2_.length) == 1)
         {
            _node.setLocalName(param1);
         }
         else
         {
            _node.setLocalName(_loc2_[1]);
            _node.setNamespace(_node.namespace(_loc2_[0]));
         }
         return param1;
      }
      
      public function set(param1:String, param2:String) : void
      {
         var _loc4_:* = null as Namespace;
         var _loc5_:* = null as XMLList;
         if(nodeType != Xml.Element)
         {
            Boot.lastError = new Error();
            throw "bad nodeType";
         }
         var _loc3_:Array = param1.split(":");
         if(_loc3_[0] == "xmlns")
         {
            _loc4_ = _node.namespace(_loc3_[1] == null ? "" : _loc3_[1]);
            if(_loc4_ != null)
            {
               Boot.lastError = new Error();
               throw "Can\'t modify namespace";
            }
            if(_loc3_[1] == null)
            {
               Boot.lastError = new Error();
               throw "Can\'t set default namespace";
            }
            _node.addNamespace(new Namespace(_loc3_[1],param2));
            return;
         }
         if(int(_loc3_.length) == 1)
         {
            _node["@" + param1] = param2;
         }
         else
         {
            _loc5_ = getAttribNS(_node,_loc3_);
            _loc5_[0] = param2;
         }
      }
      
      public function removeChild(param1:Xml) : Boolean
      {
         if(nodeType != Xml.Element && nodeType != Xml.Document)
         {
            Boot.lastError = new Error();
            throw "bad nodeType";
         }
         var _loc2_:XMLList = _node.children();
         if(_node != param1._node.parent())
         {
            return false;
         }
         var _loc3_:int = int(param1._node.childIndex());
         delete _loc2_[Reflect.fields(_loc2_)[_loc3_]];
         return true;
      }
      
      public function remove(param1:String) : void
      {
         if(nodeType != Xml.Element)
         {
            Boot.lastError = new Error();
            throw "bad nodeType";
         }
         var _loc2_:Array = param1.split(":");
         if(int(_loc2_.length) == 1)
         {
            Reflect.deleteField(_node,"@" + param1);
         }
         else
         {
            delete getAttribNS(_node,_loc2_)[0];
         }
      }
      
      public function iterator() : Object
      {
         if(nodeType != Xml.Element && nodeType != Xml.Document)
         {
            Boot.lastError = new Error();
            throw "bad nodeType";
         }
         var _loc1_:XMLList = _node.children();
         var wrappers:Array = wraps(_loc1_);
         var cur:int = 0;
         return {
            "\n\x1cT[\x02":function():Boolean
            {
               return cur < int(wrappers.length);
            },
            "next":function():Xml
            {
               var _loc1_:int;
               cur = (_loc1_ = cur) + 1;
               return wrappers[_loc1_];
            }
         };
      }
      
      public function insertChild(param1:Xml, param2:int) : void
      {
         if(nodeType != Xml.Element && nodeType != Xml.Document)
         {
            Boot.lastError = new Error();
            throw "bad nodeType";
         }
         var _loc3_:XMLList = _node.children();
         if(param2 < int(_loc3_.length()))
         {
            _node.insertChildBefore(_loc3_[param2],param1._node);
         }
         else
         {
            _node.appendChild(param1._node);
         }
      }
      
      public function §uTjZ\x01§() : Xml
      {
         var _loc1_:XML = _node.parent();
         return _loc1_ == null ? null : Xml.wrap(_loc1_);
      }
      
      public function §\x07\x15~U§() : String
      {
         var _loc1_:String = nodeType;
         if(_loc1_ == Xml.Element || _loc1_ == Xml.Document)
         {
            Boot.lastError = new Error();
            throw "bad nodeType";
         }
         if(_loc1_ == Xml.Comment)
         {
            return _node.toString().substr(4,-7);
         }
         return _node.toString();
      }
      
      public function §\\p\x19g§() : String
      {
         if(nodeType != Xml.Element)
         {
            Boot.lastError = new Error();
            throw "bad nodeType";
         }
         var _loc1_:Namespace = _node.namespace();
         return _loc1_.prefix == "" ? _node.localName() : Std.string(_loc1_.prefix) + ":" + Std.string(_node.localName());
      }
      
      public function getAttribNS(param1:XML, param2:Array) : XMLList
      {
         var _loc4_:* = null as XML;
         var _loc3_:Namespace = param1.namespace(param2[0]);
         if(_loc3_ == null)
         {
            _loc4_ = param1.parent();
            if(_loc4_ != null)
            {
               return getAttribNS(_loc4_,param2);
            }
            _loc3_ = new Namespace(param2[0],"@" + param2[0]);
            param1.addNamespace(_loc3_);
         }
         return _node.attribute(new QName(_loc3_,param2[1]));
      }
      
      public function get(param1:String) : String
      {
         var _loc3_:* = null as Namespace;
         if(nodeType != Xml.Element)
         {
            Boot.lastError = new Error();
            throw "bad nodeType";
         }
         var _loc2_:Array = param1.split(":");
         if(_loc2_[0] == "xmlns")
         {
            _loc3_ = _node.namespace(_loc2_[1] == null ? "" : _loc2_[1]);
            return _loc3_ == null ? null : _loc3_.uri;
         }
         if(int(_loc2_.length) == 1)
         {
            if(!Reflect.§wXJ\x01§(_node,"@" + param1))
            {
               return null;
            }
            return Reflect.field(_node,"@" + param1);
         }
         var _loc4_:XMLList = getAttribNS(_node,_loc2_);
         return int(_loc4_.length()) == 0 ? null : _loc4_.toString();
      }
      
      public function firstElement() : Xml
      {
         if(nodeType != Xml.Element && nodeType != Xml.Document)
         {
            Boot.lastError = new Error();
            throw "bad nodeType";
         }
         var _loc1_:XMLList = _node.elements();
         if(int(_loc1_.length()) == 0)
         {
            return null;
         }
         return Xml.wrap(_loc1_[0]);
      }
      
      public function firstChild() : Xml
      {
         if(nodeType != Xml.Element && nodeType != Xml.Document)
         {
            Boot.lastError = new Error();
            throw "bad nodeType";
         }
         var _loc1_:XMLList = _node.children();
         if(int(_loc1_.length()) == 0)
         {
            return null;
         }
         return Xml.wrap(_loc1_[0]);
      }
      
      public function §=Vr§(param1:String) : Boolean
      {
         if(nodeType != Xml.Element)
         {
            Boot.lastError = new Error();
            throw "bad nodeType";
         }
         var _loc2_:Array = param1.split(":");
         if(_loc2_[0] == "xmlns")
         {
            return _node.namespace(_loc2_[1] == null ? "" : _loc2_[1]) != null;
         }
         if(int(_loc2_.length) == 1)
         {
            return Boolean(Reflect.§wXJ\x01§(_node,"@" + param1));
         }
         return int(getAttribNS(_node,_loc2_).length()) > 0;
      }
      
      public function elementsNamed(param1:String) : Object
      {
         var _loc3_:* = null as XMLList;
         var _loc4_:int = 0;
         var _loc5_:* = null as Array;
         var _loc6_:* = null as Xml;
         if(nodeType != Xml.Element && nodeType != Xml.Document)
         {
            Boot.lastError = new Error();
            throw "bad nodeType";
         }
         var _loc2_:Array = param1.split(":");
         if(int(_loc2_.length) == 1)
         {
            _loc3_ = _node.elements(param1);
         }
         else
         {
            _loc3_ = _node.elements();
         }
         var wrappers:Array = wraps(_loc3_);
         if(int(_loc2_.length) != 1)
         {
            _loc4_ = 0;
            _loc5_ = wrappers.copy();
            while(_loc4_ < int(_loc5_.length))
            {
               _loc6_ = _loc5_[_loc4_];
               _loc4_++;
               if(_loc6_._node.localName() != _loc2_[1] || _loc6_._node.namespace().prefix != _loc2_[0])
               {
                  wrappers.remove(_loc6_);
               }
            }
         }
         var cur:int = 0;
         return {
            "\n\x1cT[\x02":function():Boolean
            {
               return cur < int(wrappers.length);
            },
            "next":function():Xml
            {
               var _loc1_:int;
               cur = (_loc1_ = cur) + 1;
               return wrappers[_loc1_];
            }
         };
      }
      
      public function elements() : Object
      {
         if(nodeType != Xml.Element && nodeType != Xml.Document)
         {
            Boot.lastError = new Error();
            throw "bad nodeType";
         }
         var _loc1_:XMLList = _node.elements();
         var wrappers:Array = wraps(_loc1_);
         var cur:int = 0;
         return {
            "\n\x1cT[\x02":function():Boolean
            {
               return cur < int(wrappers.length);
            },
            "next":function():Xml
            {
               var _loc1_:int;
               cur = (_loc1_ = cur) + 1;
               return wrappers[_loc1_];
            }
         };
      }
      
      public function attributes() : Object
      {
         if(nodeType != Xml.Element)
         {
            Boot.lastError = new Error();
            throw "bad nodeType";
         }
         var §^\x03bG§:XMLList = _node.attributes();
         var names:Array = Reflect.fields(§^\x03bG§);
         var cur:int = 0;
         return {
            "\n\x1cT[\x02":function():Boolean
            {
               return cur < int(names.length);
            },
            "next":function():Object
            {
               var _loc1_:int;
               cur = (_loc1_ = cur) + 1;
               return §^\x03bG§[Std.parseInt(names[_loc1_])].name();
            }
         };
      }
      
      public function addChild(param1:Xml) : void
      {
         if(nodeType != Xml.Element && nodeType != Xml.Document)
         {
            Boot.lastError = new Error();
            throw "bad nodeType";
         }
         var _loc2_:XMLList = _node.children();
         _node.appendChild(param1._node);
      }
   }
}
