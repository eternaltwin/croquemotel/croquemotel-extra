package
{
   import flash.Boot;
   
   public final class _CampaignEffect
   {
      
      public static const __isenum:Boolean = true;
      
      public static var __constructs__ = ["zeIj\x02","\x10\x031\x1c\x01"];
       
      
      public var tag:String;
      
      public var index:int;
      
      public var params:Array;
      
      public const __enum__:Boolean = true;
      
      public function _CampaignEffect(param1:String, param2:int, param3:*)
      {
         tag = param1;
         index = param2;
         params = param3;
      }
      
      public static function §zeIj\x02§(param1:int) : _CampaignEffect
      {
         return new _CampaignEffect("zeIj\x02",0,[param1]);
      }
      
      public static function §\x10\x031\x1c\x01§(param1:int) : _CampaignEffect
      {
         return new _CampaignEffect("\x10\x031\x1c\x01",1,[param1]);
      }
      
      public final function toString() : String
      {
         return Boot.enum_to_string(this);
      }
   }
}
