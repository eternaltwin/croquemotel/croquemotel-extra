package
{
   public class _Hotel
   {
      
      public static var §\x19\t0I\x01§:int = 6;
      
      public static var §E\x0b\x1eX\x01§:int = 12;
       
      
      public var _width:int;
      
      public var _v:int;
      
      public var _uniqId:int;
      
      public var _uniqClientId:int;
      
      public var _stars:int;
      
      public var _staff:List;
      
      public var _rpoints:int;
      
      public var _rooms:Array;
      
      public var _questId:int;
      
      public var _questGoals:List;
      
      public var _nextEvent:Date;
      
      public var _nextClient:Date;
      
      public var _name:String;
      
      public var _money:int;
      
      public var _maxInQueue:int;
      
      public var _level:int;
      
      public var _lastDate:Date;
      
      public var _lastClient:Date;
      
      public var _items:IntHash;
      
      public var _id:int;
      
      public var _gameLog:List;
      
      public var _friends:List;
      
      public var _floors:int;
      
      public var _fame:int;
      
      public var _design:IntHash;
      
      public var _deco:List;
      
      public var _debugLog:List;
      
      public var _debugDate:Date;
      
      public var _createDate:Date;
      
      public var _color:int;
      
      public var _clients:IntHash;
      
      public var _clientQueue:List;
      
      public var _campaigns:List;
      
      public var _build:Hash;
      
      public var _active:Boolean;
      
      public var _actionLog:List;
      
      public function _Hotel()
      {
      }
      
      public static function _canBuildRoom(param1:_TypeRoom, param2:int) : Boolean
      {
         switch(int(param1.index))
         {
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
               break;
            case 9:
               break;
            case 10:
               break;
            case 11:
               break;
            case 12:
               break;
            case 13:
         }
         param2 >= 2;
         param2 >= 1;
         param2 >= 3;
         param2 >= 4;
         param2 >= 5;
         param2 >= 1;
         return true;
      }
      
      public function §an9F\x01§() : Boolean
      {
         return _level >= 1;
      }
      
      public function §Qkr,\x01§() : int
      {
         return int(Const.§(X}Z\x01§[_color].U);
      }
      
      public function §Htd\b\x01§() : int
      {
         return int(Const.§(X}Z\x01§[_color].§1§);
      }
      
      public function §\x0bsDj\x03§() : int
      {
         return _level <= 0 ? 1 : _level + 1;
      }
      
      public function §QR\x1ds\x03§(param1:int) : Number
      {
         switch(param1)
         {
            case 0:
               break;
            case 1:
               break;
            case 2:
               break;
            case 3:
               break;
            case 4:
               break;
            case 5:
         }
         var _loc2_:Number = 0.75;
         Number(8);
         Number(5);
         Number(4);
         Number(3);
         Number(2);
         Number(1);
         return Number(DateTools.hours(_loc2_));
      }
      
      public function §?\x1cc\x01\x02§(param1:_TypeJob) : int
      {
         var job:_TypeJob = param1;
         return Lambda.filter(_staff,function(param1:_Staff):Boolean
         {
            return param1._job == job;
         }).length;
      }
      
      public function §Z9\x14V\x02§() : Boolean
      {
         return _questId >= 7;
      }
      
      public function §T&\x03(§(param1:_TypeRoom) : Boolean
      {
         return Boolean(_Hotel._canBuildRoom(param1,_level));
      }
   }
}
