package
{
   import flash.Boot;
   
   public final class _Item
   {
      
      public static const __isenum:Boolean = true;
      
      public static var __constructs__ = ["_BUFFET","_RADIATOR","_STINK_BOMB","_HUMIDIFIER","_HIFI_SYSTEM","_OLD_BUFFET","_DJ","_PRESENT","_LABY_CUPBOARD","_MATTRESS","_FIREWORKS","_WALLET","_FRIEND","_REPAIR","_MONEY","_ISOLATION","_RANDPAINT","_RANDBOTTOM","_RANDTEXTURE","_RANDDECO","_RANDPAINTWARM","_RANDPAINTCOOL","_RESEARCH","_RESEARCH_GOLD","_PRESENT_XL"];
      
      public static var __meta__ = {"fields":{
         "_BUFFET":{"\x0f#\b\x02":[10]},
         "_RADIATOR":{"\x0f#\b\x02":[10]},
         "_STINK_BOMB":{"\x0f#\b\x02":[10]},
         "_HUMIDIFIER":{"\x0f#\b\x02":[10]},
         "_HIFI_SYSTEM":{"\x0f#\b\x02":[10]},
         "_OLD_BUFFET":{"\x0f#\b\x02":[0]},
         "_DJ":{"\x0f#\b\x02":[0]},
         "_PRESENT":{"\x0f#\b\x02":[20]},
         "_LABY_CUPBOARD":{"\x0f#\b\x02":[4]},
         "_MATTRESS":{"\x0f#\b\x02":[4]},
         "_FIREWORKS":{"\x0f#\b\x02":[4]},
         "_WALLET":{"\x0f#\b\x02":[4]},
         "_FRIEND":{"\x0f#\b\x02":[10]},
         "_REPAIR":{"\x0f#\b\x02":[2]},
         "_MONEY":{"\x0f#\b\x02":[5]},
         "_ISOLATION":{"\x0f#\b\x02":[4]},
         "_RANDPAINT":{"\x0f#\b\x02":[0]},
         "_RANDBOTTOM":{"\x0f#\b\x02":[0]},
         "_RANDTEXTURE":{"\x0f#\b\x02":[0]},
         "_RANDDECO":{"\x0f#\b\x02":[2]},
         "_RANDPAINTWARM":{"\x0f#\b\x02":[0]},
         "_RANDPAINTCOOL":{"\x0f#\b\x02":[0]},
         "_RESEARCH":{"\x0f#\b\x02":[0]},
         "_RESEARCH_GOLD":{"\x0f#\b\x02":[0]},
         "_PRESENT_XL":{"\x0f#\b\x02":[0]}
      }};
      
      public static var _WALLET:_Item;
      
      public static var _STINK_BOMB:_Item;
      
      public static var _RESEARCH_GOLD:_Item;
      
      public static var _RESEARCH:_Item;
      
      public static var _REPAIR:_Item;
      
      public static var _RANDTEXTURE:_Item;
      
      public static var _RANDPAINTWARM:_Item;
      
      public static var _RANDPAINTCOOL:_Item;
      
      public static var _RANDPAINT:_Item;
      
      public static var _RANDDECO:_Item;
      
      public static var _RANDBOTTOM:_Item;
      
      public static var _RADIATOR:_Item;
      
      public static var _PRESENT_XL:_Item;
      
      public static var _PRESENT:_Item;
      
      public static var _OLD_BUFFET:_Item;
      
      public static var _MONEY:_Item;
      
      public static var _MATTRESS:_Item;
      
      public static var _LABY_CUPBOARD:_Item;
      
      public static var _ISOLATION:_Item;
      
      public static var _HUMIDIFIER:_Item;
      
      public static var _HIFI_SYSTEM:_Item;
      
      public static var _FRIEND:_Item;
      
      public static var _FIREWORKS:_Item;
      
      public static var _DJ:_Item;
      
      public static var _BUFFET:_Item;
       
      
      public var tag:String;
      
      public var index:int;
      
      public var params:Array;
      
      public const __enum__:Boolean = true;
      
      public function _Item(param1:String, param2:int, param3:*)
      {
         tag = param1;
         index = param2;
         params = param3;
      }
      
      public final function toString() : String
      {
         return Boot.enum_to_string(this);
      }
   }
}
