package
{
   import flash.Boot;
   
   public final class _Likes
   {
      
      public static const __isenum:Boolean = true;
      
      public static var __constructs__ = ["_NOISE","_WATER","_FIRE","_ODOR","_FOOD","_NEIGHBOR","_JOY","_LUX_ROOM"];
      
      public static var _WATER:_Likes;
      
      public static var _ODOR:_Likes;
      
      public static var _NOISE:_Likes;
      
      public static var _NEIGHBOR:_Likes;
      
      public static var _LUX_ROOM:_Likes;
      
      public static var _JOY:_Likes;
      
      public static var _FOOD:_Likes;
      
      public static var _FIRE:_Likes;
       
      
      public var tag:String;
      
      public var index:int;
      
      public var params:Array;
      
      public const __enum__:Boolean = true;
      
      public function _Likes(param1:String, param2:int, param3:*)
      {
         tag = param1;
         index = param2;
         params = param3;
      }
      
      public final function toString() : String
      {
         return Boot.enum_to_string(this);
      }
   }
}
