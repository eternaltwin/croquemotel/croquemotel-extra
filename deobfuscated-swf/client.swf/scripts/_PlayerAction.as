package
{
   import flash.Boot;
   
   public final class _PlayerAction
   {
      
      public static const __isenum:Boolean = true;
      
      public static var __constructs__ = ["^eB%","i_\nI","(I\x1bO"," 7 (\x02","\x1cvTl\x01","N06Q\x01","P\t\x19)\x03","GjBZ\x02","Qqh\x03\x03","\x06Wb4\x02","\x02`BG","J\x1d1\'\x01","K\x136Q\x02","B`6[\x03","YK@H","MNu \x03","\x19%Jj","lh^\r\x02","G%\x0b(\x01","l\x01*z","\n\x14u\x0b\x03","\x15d,J\x02"];
      
      public static var §\x06Wb4\x02§:_PlayerAction;
      
      public static var §\x15d,J\x02§:_PlayerAction;
      
      public static var § 7 (\x02§:_PlayerAction;
      
      public static var §G%\x0b(\x01§:_PlayerAction;
       
      
      public var tag:String;
      
      public var index:int;
      
      public var params:Array;
      
      public const __enum__:Boolean = true;
      
      public function _PlayerAction(param1:String, param2:int, param3:*)
      {
         tag = param1;
         index = param2;
         params = param3;
      }
      
      public static function §\x1cvTl\x01§(param1:int) : _PlayerAction
      {
         return new _PlayerAction("\x1cvTl\x01",4,[param1]);
      }
      
      public static function §P\t\x19)\x03§(param1:int, param2:int, param3:_Item) : _PlayerAction
      {
         return new _PlayerAction("P\t\x19)\x03",6,[param1,param2,param3]);
      }
      
      public static function §N06Q\x01§(param1:int, param2:int) : _PlayerAction
      {
         return new _PlayerAction("N06Q\x01",5,[param1,param2]);
      }
      
      public static function §(I\x1bO§(param1:int, param2:int, param3:_TypeRoom) : _PlayerAction
      {
         return new _PlayerAction("(I\x1bO",2,[param1,param2,param3]);
      }
      
      public static function §l\x01*z§(param1:int, param2:int) : _PlayerAction
      {
         return new _PlayerAction("l\x01*z",19,[param1,param2]);
      }
      
      public static function §lh^\r\x02§(param1:List) : _PlayerAction
      {
         return new _PlayerAction("lh^\r\x02",17,[param1]);
      }
      
      public static function §MNu \x03§(param1:int, param2:int) : _PlayerAction
      {
         return new _PlayerAction("MNu \x03",15,[param1,param2]);
      }
      
      public static function §GjBZ\x02§(param1:int, param2:int, param3:int) : _PlayerAction
      {
         return new _PlayerAction("GjBZ\x02",7,[param1,param2,param3]);
      }
      
      public static function §YK@H§(param1:int, param2:int, param3:_Item) : _PlayerAction
      {
         return new _PlayerAction("YK@H",14,[param1,param2,param3]);
      }
      
      public static function §^eB%§(param1:int, param2:int, param3:int, param4:int) : _PlayerAction
      {
         return new _PlayerAction("^eB%",0,[param1,param2,param3,param4]);
      }
      
      public static function §B`6[\x03§(param1:int, param2:int) : _PlayerAction
      {
         return new _PlayerAction("B`6[\x03",13,[param1,param2]);
      }
      
      public static function §K\x136Q\x02§(param1:int) : _PlayerAction
      {
         return new _PlayerAction("K\x136Q\x02",12,[param1]);
      }
      
      public static function §J\x1d1'\x01§(param1:int) : _PlayerAction
      {
         return new _PlayerAction("J\x1d1\'\x01",11,[param1]);
      }
      
      public static function §\x02`BG§(param1:int) : _PlayerAction
      {
         return new _PlayerAction("\x02`BG",10,[param1]);
      }
      
      public static function §\n\x14u\x0b\x03§(param1:int, param2:int) : _PlayerAction
      {
         return new _PlayerAction("\n\x14u\x0b\x03",20,[param1,param2]);
      }
      
      public static function §\x19%Jj§(param1:int) : _PlayerAction
      {
         return new _PlayerAction("\x19%Jj",16,[param1]);
      }
      
      public static function §Qqh\x03\x03§(param1:int, param2:int) : _PlayerAction
      {
         return new _PlayerAction("Qqh\x03\x03",8,[param1,param2]);
      }
      
      public static function §i_\nI§(param1:int, param2:int, param3:int) : _PlayerAction
      {
         return new _PlayerAction("i_\nI",1,[param1,param2,param3]);
      }
      
      public final function toString() : String
      {
         return Boot.enum_to_string(this);
      }
   }
}
