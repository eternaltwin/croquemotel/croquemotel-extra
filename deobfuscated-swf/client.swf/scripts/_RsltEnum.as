package
{
   import flash.Boot;
   
   public final class _RsltEnum
   {
      
      public static const __isenum:Boolean = true;
      
      public static var __constructs__ = [",E0,\x03","%(M\x17\x01","Q~\x13\x15\x03"];
       
      
      public var tag:String;
      
      public var index:int;
      
      public var params:Array;
      
      public const __enum__:Boolean = true;
      
      public function _RsltEnum(param1:String, param2:int, param3:*)
      {
         tag = param1;
         index = param2;
         params = param3;
      }
      
      public static function §%(M\x17\x01§(param1:Object) : _RsltEnum
      {
         return new _RsltEnum("%(M\x17\x01",1,[param1]);
      }
      
      public static function §,E0,\x03§(param1:Object) : _RsltEnum
      {
         return new _RsltEnum(",E0,\x03",0,[param1]);
      }
      
      public static function §Q~\x13\x15\x03§(param1:Boolean) : _RsltEnum
      {
         return new _RsltEnum("Q~\x13\x15\x03",2,[param1]);
      }
      
      public final function toString() : String
      {
         return Boot.enum_to_string(this);
      }
   }
}
