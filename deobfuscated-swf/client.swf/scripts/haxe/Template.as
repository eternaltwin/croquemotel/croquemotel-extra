package haxe
{
   import flash.Boot;
   import haxe._Template.§\x1bE_\x02§;
   
   public class Template
   {
      
      public static var init__:Boolean;
      
      public static var splitter:EReg;
      
      public static var §}\x15s \x03§:EReg;
      
      public static var §F\x12\x03o\x03§:EReg;
      
      public static var §\\c\x11\f\x01§:EReg;
      
      public static var §c~eD\x01§:EReg;
      
      public static var §iz{[§ = {};
       
      
      public var stack:List;
      
      public var macros;
      
      public var expr:§\x1bE_\x02§;
      
      public var context;
      
      public var buf:StringBuf;
      
      public function Template(param1:String = undefined)
      {
         if(Boot.skip_constructor)
         {
            return;
         }
         var _loc2_:List = §Yw&i\x02§(param1);
         expr = §\x02o\x06+\x03§(_loc2_);
         if(!_loc2_.isEmpty())
         {
            Boot.lastError = new Error();
            throw "Unexpected \'" + Std.string(Boolean(_loc2_.first().s)) + "N";
         }
      }
      
      public function run(param1:§\x1bE_\x02§) : void
      {
         var _loc4_:* = null as String;
         var _loc5_:* = null as Function;
         var _loc6_:* = null as §\x1bE_\x02§;
         var _loc7_:* = null as §\x1bE_\x02§;
         var _loc8_:* = null;
         var _loc9_:* = null as List;
         var _loc10_:* = null;
         var _loc11_:* = null;
         var _loc12_:* = null;
         var _loc13_:* = null as Array;
         var _loc14_:* = null as StringBuf;
         var _loc15_:* = null as Array;
         var _loc16_:* = null as String;
         var _loc17_:* = null as String;
         var _loc3_:Array = param1.params;
         switch(int(param1.index))
         {
            case 0:
               _loc4_ = _loc3_[0];
               buf.b = buf.b + Std.string(Std.string(resolve(_loc4_)));
               break;
            case 1:
               _loc5_ = _loc3_[0];
               buf.b = buf.b + Std.string(Std.string(_loc5_()));
               break;
            case 2:
               _loc5_ = _loc3_[0];
               _loc6_ = _loc3_[1];
               _loc7_ = _loc3_[2];
               _loc8_ = _loc5_();
               if(_loc8_ == null || _loc8_ == false)
               {
                  if(_loc7_ != null)
                  {
                     run(_loc7_);
                  }
               }
               else
               {
                  run(_loc6_);
               }
               break;
            case 3:
               _loc4_ = _loc3_[0];
               buf.b = buf.b + Std.string(_loc4_);
               break;
            case 4:
               _loc9_ = _loc3_[0];
               _loc8_ = _loc9_.iterator();
               while(_loc8_.§\n\x1cT[\x02§())
               {
                  _loc6_ = _loc8_.next();
                  run(_loc6_);
               }
               break;
            case 5:
               _loc5_ = _loc3_[0];
               _loc6_ = _loc3_[1];
               _loc8_ = _loc5_();
               try
               {
                  _loc10_ = _loc8_.iterator();
                  if(_loc10_.§\n\x1cT[\x02§ == null)
                  {
                     Boot.lastError = new Error();
                     throw null;
                  }
                  _loc8_ = _loc10_;
               }
               catch(_loc_e_:*)
               {
                  try
                  {
                     if(_loc8_.§\n\x1cT[\x02§ == null)
                     {
                        Boot.lastError = new Error();
                        throw null;
                     }
                  }
                  catch(_loc_e_:*)
                  {
                     Boot.lastError = new Error();
                     throw "Cannot iter on " + Std.string(_loc8_);
                  }
               }
               break;
            case 6:
               _loc4_ = _loc3_[0];
               _loc9_ = _loc3_[1];
               _loc8_ = Reflect.field(macros,_loc4_);
               _loc13_ = [];
               _loc14_ = buf;
               _loc13_.push(resolve);
               _loc10_ = _loc9_.iterator();
               while(_loc10_.§\n\x1cT[\x02§())
               {
                  _loc6_ = _loc10_.next();
                  _loc15_ = _loc6_.params;
                  switch(int(_loc6_.index))
                  {
                     case 0:
                        _loc16_ = _loc15_[0];
                        _loc13_.push(resolve(_loc16_));
                        break;
                     default:
                        buf = new StringBuf();
                        run(_loc6_);
                        _loc13_.push(buf.b);
                  }
               }
               buf = _loc14_;
               try
               {
                  buf.b = buf.b + Std.string(Std.string(_loc8_.apply(macros,_loc13_)));
                  break;
               }
               catch(_loc_e_:*)
               {
                  try
                  {
                     §§push(_loc13_.join(","));
                  }
                  catch(_loc_e_:*)
                  {
                     _loc16_ = §§pop();
                     _loc17_ = "Macro call " + _loc4_ + "h" + _loc16_ + ") failed (" + Std.string(_loc10_) + "U";
                     Boot.lastError = new Error();
                     throw _loc17_;
                  }
               }
         }
      }
      
      public function resolve(param1:String) : *
      {
         var _loc3_:* = null;
         if(Reflect.§wXJ\x01§(context,param1))
         {
            return Reflect.field(context,param1);
         }
         var _loc2_:* = stack.iterator();
         while(_loc2_.§\n\x1cT[\x02§())
         {
            _loc3_ = _loc2_.next();
            if(Reflect.§wXJ\x01§(_loc3_,param1))
            {
               return Reflect.field(_loc3_,param1);
            }
         }
         if(param1 == "__current__")
         {
            return context;
         }
         return Reflect.field(Template.§iz{[§,param1);
      }
      
      public function §Yw&i\x02§(param1:String) : List
      {
         var _loc3_:* = null;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:* = null;
         var _loc7_:* = null as Array;
         var _loc2_:List = new List();
         while(Template.splitter.match(param1))
         {
            _loc3_ = Template.splitter.matchedPos();
            if(int(_loc3_.pos) > 0)
            {
               _loc2_.add({
                  "J":param1.substr(0,int(_loc3_.pos)),
                  "s":true,
                  "l":null
               });
            }
            if(param1.charCodeAt(int(_loc3_.pos)) == 58)
            {
               _loc2_.add({
                  "J":param1.substr(int(_loc3_.pos) + 2,int(_loc3_.len) - 4),
                  "s":false,
                  "l":null
               });
               param1 = Template.splitter.matchedRight();
            }
            else
            {
               _loc4_ = int(_loc3_.pos) + int(_loc3_.len);
               _loc5_ = 1;
               while(_loc5_ > 0)
               {
                  _loc6_ = param1.charCodeAt(_loc4_);
                  if(_loc6_ == 40)
                  {
                     _loc5_++;
                  }
                  else if(_loc6_ == 41)
                  {
                     _loc5_--;
                  }
                  else if(_loc6_ == null)
                  {
                     Boot.lastError = new Error();
                     throw "Unclosed macro parenthesis";
                  }
                  _loc4_++;
               }
               _loc7_ = param1.substr(int(_loc3_.pos) + int(_loc3_.len),_loc4_ - (int(_loc3_.pos) + int(_loc3_.len)) - 1).split(",");
               _loc2_.add({
                  "J":Template.splitter.matched(2),
                  "s":false,
                  "l":_loc7_
               });
               param1 = param1.substr(_loc4_,param1.length - _loc4_);
            }
         }
         if(param1.length > 0)
         {
            _loc2_.add({
               "J":param1,
               "s":true,
               "l":null
            });
         }
         return _loc2_;
      }
      
      public function §1G\x1f\x02§(param1:String) : Function
      {
         var _loc4_:* = null;
         var _loc5_:int = 0;
         var _loc6_:* = null as String;
         var _loc3_:List = new List();
         var §m\x0bP,\x02§:String = param1;
         while(Template.§}\x15s \x03§.match(param1))
         {
            _loc4_ = Template.§}\x15s \x03§.matchedPos();
            _loc5_ = int(_loc4_.pos) + int(_loc4_.len);
            if(int(_loc4_.pos) != 0)
            {
               _loc3_.add({
                  "J":param1.substr(0,int(_loc4_.pos)),
                  "s":true
               });
            }
            _loc6_ = Template.§}\x15s \x03§.matched(0);
            _loc3_.add({
               "J":_loc6_,
               "s":int(_loc6_.indexOf("\"")) >= 0
            });
            param1 = Template.§}\x15s \x03§.matchedRight();
         }
         if(param1.length != 0)
         {
            _loc3_.add({
               "J":param1,
               "s":true
            });
         }
         try
         {
            var e:Function = §l\x04fD§(_loc3_);
            if(!_loc3_.isEmpty())
            {
               Boot.lastError = new Error();
               throw _loc3_.first().J;
            }
         }
         catch(_loc_e_:String)
         {
            _loc6_ = _loc_e_;
            Boot.lastError = new Error();
            throw "Unexpected \'" + _loc6_ + "\' in " + §m\x0bP,\x02§;
         }
         return function():*
         {
            var _loc2_:* = null;
            try
            {
               return e();
            }
            catch(_loc_e_:*)
            {
               Boot.lastError = new Error();
               throw "Error : " + Std.string(_loc2_) + " in " + §m\x0bP,\x02§;
            }
         };
      }
      
      public function §\x02o\x06+\x03§(param1:List) : §\x1bE_\x02§
      {
         var _loc3_:* = null;
         var _loc2_:List = new List();
         while(true)
         {
            _loc3_ = param1.first();
            if(_loc3_ == null)
            {
               break;
            }
            if(!_loc3_.s && (_loc3_.J == "end" || _loc3_.J == "else" || _loc3_.J.substr(0,7) == "elseif "))
            {
               break;
            }
            _loc2_.add(parse(param1));
         }
         if(_loc2_.length == 1)
         {
            return _loc2_.first();
         }
         return §\x1bE_\x02§.§\b\fD=§(_loc2_);
      }
      
      public function parse(param1:List) : §\x1bE_\x02§
      {
         var _loc4_:* = null as List;
         var _loc5_:int = 0;
         var _loc6_:* = null as Array;
         var _loc7_:* = null as String;
         var _loc8_:* = null as Function;
         var _loc9_:* = null as §\x1bE_\x02§;
         var _loc10_:* = null;
         var _loc11_:* = null as §\x1bE_\x02§;
         var _loc2_:* = param1.pop();
         var _loc3_:String = _loc2_.J;
         if(_loc2_.s)
         {
            return §\x1bE_\x02§.§\t5h;\x02§(_loc3_);
         }
         if(_loc2_.l != null)
         {
            _loc4_ = new List();
            _loc5_ = 0;
            _loc6_ = _loc2_.l;
            while(_loc5_ < int(_loc6_.length))
            {
               _loc7_ = _loc6_[_loc5_];
               _loc5_++;
               _loc4_.add(§\x02o\x06+\x03§(§Yw&i\x02§(_loc7_)));
            }
            return §\x1bE_\x02§.§\x10\x14L&\x03§(_loc3_,_loc4_);
         }
         if(_loc3_.substr(0,3) == "if ")
         {
            _loc3_ = _loc3_.substr(3,_loc3_.length - 3);
            _loc8_ = §1G\x1f\x02§(_loc3_);
            _loc9_ = §\x02o\x06+\x03§(param1);
            _loc10_ = param1.first();
            if(_loc10_ == null)
            {
               Boot.lastError = new Error();
               throw "Unclosed \'if\'";
            }
            if(_loc10_.J == "end")
            {
               param1.pop();
               _loc11_ = null;
            }
            else if(_loc10_.J == "else")
            {
               param1.pop();
               _loc11_ = §\x02o\x06+\x03§(param1);
               _loc10_ = param1.pop();
               if(_loc10_ == null || _loc10_.J != "end")
               {
                  Boot.lastError = new Error();
                  throw "Unclosed \'else\'";
               }
            }
            else
            {
               _loc10_.J = _loc10_.J.substr(4,_loc10_.J.length - 4);
               _loc11_ = parse(param1);
            }
            return §\x1bE_\x02§.§\x0e\x1cy\x03\x02§(_loc8_,_loc9_,_loc11_);
         }
         if(_loc3_.substr(0,8) == "foreach ")
         {
            _loc3_ = _loc3_.substr(8,_loc3_.length - 8);
            _loc8_ = §1G\x1f\x02§(_loc3_);
            _loc9_ = §\x02o\x06+\x03§(param1);
            _loc10_ = param1.pop();
            if(_loc10_ == null || _loc10_.J != "end")
            {
               Boot.lastError = new Error();
               throw "Unclosed \'foreach\'";
            }
            return §\x1bE_\x02§.§\x12\x1dh&\x02§(_loc8_,_loc9_);
         }
         if(Template.§}\x15s \x03§.match(_loc3_))
         {
            return §\x1bE_\x02§.§S04\x0f\x03§(§1G\x1f\x02§(_loc3_));
         }
         return §\x1bE_\x02§.§QX2p\x03§(_loc3_);
      }
      
      public function §j}d7\x02§(param1:Function, param2:List) : Function
      {
         var e:Function = param1;
         var _loc3_:* = param2.first();
         if(_loc3_ == null || _loc3_.J != ".")
         {
            return e;
         }
         param2.pop();
         var _loc4_:* = param2.pop();
         if(_loc4_ == null || !_loc4_.s)
         {
            Boot.lastError = new Error();
            throw _loc4_.J;
         }
         var l:String = _loc4_.J;
         Template.§F\x12\x03o\x03§.match(l);
         l = Template.§F\x12\x03o\x03§.matched(1);
         return §j}d7\x02§(function():*
         {
            return Reflect.field(e(),l);
         },param2);
      }
      
      public function §!\x1b6+\x03§(param1:List) : Function
      {
         var _loc4_:* = null;
         var _loc5_:* = null;
         var _loc6_:* = null as String;
         var _loc2_:* = param1.pop();
         if(_loc2_ == null)
         {
            Boot.lastError = new Error();
            throw "<eof>";
         }
         if(_loc2_.s)
         {
            return §H&ql\x01§(_loc2_.J);
         }
         var _loc3_:String = _loc2_.J;
         if(_loc3_ == "h")
         {
            var §}8§:Function = §l\x04fD§(param1);
            _loc4_ = param1.pop();
            if(_loc4_ == null || Boolean(_loc4_.s))
            {
               Boot.lastError = new Error();
               throw _loc4_.J;
            }
            if(_loc4_.J == "U")
            {
               return §}8§;
            }
            var DQ:Function = §l\x04fD§(param1);
            _loc5_ = param1.pop();
            if(_loc5_ == null || _loc5_.J != "U")
            {
               Boot.lastError = new Error();
               throw _loc5_.J;
            }
            _loc6_ = _loc4_.J;
            if(_loc6_ != "Cf")
            {
               if(_loc6_ != "q")
               {
                  if(_loc6_ != "*")
                  {
                     if(_loc6_ != "u")
                     {
                        if(_loc6_ != ">")
                        {
                           if(_loc6_ != "<")
                           {
                              if(_loc6_ != ">=")
                              {
                                 if(_loc6_ != "<=")
                                 {
                                    if(_loc6_ != "==")
                                    {
                                       if(_loc6_ != "!=")
                                       {
                                          if(_loc6_ != "&&")
                                          {
                                             if(_loc6_ != "||")
                                             {
                                                Boot.lastError = new Error();
                                                throw "Unknown operation " + _loc4_.J;
                                             }
                                          }
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
            return §§pop();
         }
         if(_loc3_ == "es")
         {
            var e:Function = §l\x04fD§(param1);
            return function():Boolean
            {
               var _loc1_:* = e();
               return _loc1_ == null || _loc1_ == false;
            };
         }
         if(_loc3_ == "q")
         {
            var j:Function = §l\x04fD§(param1);
            return function():Number
            {
               return -j();
            };
         }
         Boot.lastError = new Error();
         throw _loc2_.J;
      }
      
      public function §l\x04fD§(param1:List) : Function
      {
         return §j}d7\x02§(§!\x1b6+\x03§(param1),param1);
      }
      
      public function §H&ql\x01§(param1:String) : Function
      {
         var h:String = param1;
         Template.§F\x12\x03o\x03§.match(h);
         h = Template.§F\x12\x03o\x03§.matched(1);
         if(h.charCodeAt(0) == 34)
         {
            var str:String = h.substr(1,h.length - 2);
            return function():String
            {
               return str;
            };
         }
         if(Template.§\\c\x11\f\x01§.match(h))
         {
            var i:Object = Std.parseInt(h);
            return function():Object
            {
               return i;
            };
         }
         if(Template.§c~eD\x01§.match(h))
         {
            var l:Number = Std.parseFloat(h);
            return function():Number
            {
               return l;
            };
         }
         var me:Template = this;
         return function():*
         {
            return me.resolve(h);
         };
      }
      
      public function execute(param1:*, param2:* = undefined) : String
      {
         macros = param2 == null ? {} : param2;
         context = param1;
         stack = new List();
         buf = new StringBuf();
         run(expr);
         return buf.b;
      }
   }
}
