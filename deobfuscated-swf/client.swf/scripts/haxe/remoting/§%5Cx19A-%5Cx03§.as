package haxe.remoting
{
   import flash.Boot;
   
   public class §\x19A-\x03§
   {
       
      
      public var §;\x0bd§:Hash;
      
      public function §\x19A-\x03§()
      {
         if(Boot.skip_constructor)
         {
            return;
         }
         §;\x0bd§ = new Hash();
      }
      
      public static function share(param1:String, param2:Object) : §\x19A-\x03§
      {
         var _loc3_:§\x19A-\x03§ = new §\x19A-\x03§();
         _loc3_.addObject(param1,param2);
         return _loc3_;
      }
      
      public function call(param1:Array, param2:Array) : *
      {
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         if(int(param1.length) < 2)
         {
            Boot.lastError = new Error();
            throw "Invalid path \'" + param1.join(".") + "N";
         }
         var _loc3_:* = §;\x0bd§.get(param1[0]);
         if(_loc3_ == null)
         {
            Boot.lastError = new Error();
            throw "No such object " + param1[0];
         }
         var _loc4_:* = _loc3_.obj;
         var _loc5_:* = Reflect.field(_loc4_,param1[1]);
         if(int(param1.length) > 2)
         {
            if(!_loc3_.§$1\n\x01§)
            {
               Boot.lastError = new Error();
               throw "Can\'t access " + param1.join(".");
            }
            _loc6_ = 2;
            _loc7_ = int(param1.length);
            while(_loc6_ < _loc7_)
            {
               _loc8_ = _loc6_++;
               _loc4_ = _loc5_;
               _loc5_ = Reflect.field(_loc4_,param1[_loc8_]);
            }
         }
         if(!Reflect.isFunction(_loc5_))
         {
            Boot.lastError = new Error();
            throw "No such method " + param1.join(".");
         }
         return _loc5_.apply(_loc4_,param2);
      }
      
      public function addObject(param1:String, param2:Object, param3:Object = undefined) : void
      {
         §;\x0bd§.set(param1,{
            "obj":param2,
            "$1\n\x01":param3
         });
      }
   }
}
