package haxe.xml
{
   import flash.Boot;
   
   public class Proxy
   {
       
      
      public var __f:Function;
      
      public function Proxy(param1:Function = undefined)
      {
         if(Boot.skip_constructor)
         {
            return;
         }
         __f = param1;
      }
      
      public function resolve(param1:String) : Object
      {
         return __f(param1);
      }
   }
}
