package haxe.xml._Fast
{
   import flash.Boot;
   
   public dynamic class HasNodeAccess
   {
       
      
      public var __x:Xml;
      
      public function HasNodeAccess(param1:Xml = undefined)
      {
         if(Boot.skip_constructor)
         {
            return;
         }
         __x = param1;
      }
      
      public function resolve(param1:String) : Boolean
      {
         return Boolean(__x.elementsNamed(param1).§\n\x1cT[\x02§());
      }
   }
}
