package haxe.xml._Fast
{
   import flash.Boot;
   import haxe.xml.Fast;
   
   public dynamic class NodeAccess
   {
       
      
      public var __x:Xml;
      
      public function NodeAccess(param1:Xml = undefined)
      {
         if(Boot.skip_constructor)
         {
            return;
         }
         __x = param1;
      }
      
      public function resolve(param1:String) : Fast
      {
         var _loc3_:* = null as String;
         var _loc2_:Xml = __x.elementsNamed(param1).next();
         if(_loc2_ == null)
         {
            _loc3_ = __x.nodeType == Xml.Document ? "Document" : __x.§\\p\x19g§();
            Boot.lastError = new Error();
            throw _loc3_ + " is missing element " + param1;
         }
         return new Fast(_loc2_);
      }
   }
}
