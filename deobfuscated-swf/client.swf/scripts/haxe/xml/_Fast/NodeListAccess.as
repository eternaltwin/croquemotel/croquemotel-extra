package haxe.xml._Fast
{
   import flash.Boot;
   import haxe.xml.Fast;
   
   public dynamic class NodeListAccess
   {
       
      
      public var __x:Xml;
      
      public function NodeListAccess(param1:Xml = undefined)
      {
         if(Boot.skip_constructor)
         {
            return;
         }
         __x = param1;
      }
      
      public function resolve(param1:String) : List
      {
         var _loc4_:* = null as Xml;
         var _loc2_:List = new List();
         var _loc3_:* = __x.elementsNamed(param1);
         while(_loc3_.§\n\x1cT[\x02§())
         {
            _loc4_ = _loc3_.next();
            _loc2_.add(new Fast(_loc4_));
         }
         return _loc2_;
      }
   }
}
