package mt.deepnight
{
   import avm2.intrinsics.memory.li8;
   import avm2.intrinsics.memory.si8;
   import flash.Boot;
   import flash.display.BitmapData;
   import flash.display.Graphics;
   import flash.filters.ColorMatrixFilter;
   import flash.geom.ColorTransform;
   import flash.geom.Point;
   import flash.geom.Rectangle;
   import flash.system.ApplicationDomain;
   import flash.utils.ByteArray;
   
   public class Color
   {
      
      public static var BLACK:Object = {
         "Ec":0,
         "u":0,
         "b":0
      };
      
      public static var WHITE:Object = {
         "Ec":255,
         "u":255,
         "b":255
      };
      
      public static var MEDIAN_GRAY:Object = {
         "Ec":128,
         "u":128,
         "b":128
      };
       
      
      public function Color()
      {
      }
      
      public static function hexToRgb(param1:String) : Object
      {
         if(param1 == null)
         {
            Boot.lastError = new Error();
            throw "hexToColor with null";
         }
         if(int(param1.indexOf("#")) == 0)
         {
            param1 = param1.substr(1,999);
         }
         return {
            "Ec":Std.parseInt("0x" + param1.substr(0,2)),
            "u":Std.parseInt("0x" + param1.substr(2,2)),
            "b":Std.parseInt("0x" + param1.substr(4,2))
         };
      }
      
      public static function hexToInt(param1:String) : Object
      {
         return Std.parseInt("0x" + param1.substr(1,999));
      }
      
      public static function hexToInta(param1:String) : Object
      {
         return Std.parseInt("0xff" + param1.substr(1,999));
      }
      
      public static function rgbToInt(param1:Object) : int
      {
         return int(param1.Ec) << 16 | int(param1.u) << 8 | int(param1.b);
      }
      
      public static function §}=\n-\x03§(param1:Object) : String
      {
         var _loc2_:String = StringTools.hex(int(param1.Ec) << 16 | int(param1.u) << 8 | int(param1.b));
         while(_loc2_.length < 6)
         {
            _loc2_ = "n" + _loc2_;
         }
         return "#" + _loc2_;
      }
      
      public static function rgbToHsl(param1:Object) : Object
      {
         var _loc9_:Number = NaN;
         var _loc10_:Number = NaN;
         var _loc11_:Number = NaN;
         var _loc2_:Number = int(param1.Ec) / 255;
         var _loc3_:Number = int(param1.u) / 255;
         var _loc4_:Number = int(param1.b) / 255;
         var _loc5_:Number = _loc2_ <= _loc3_ && _loc2_ <= _loc4_ ? _loc2_ : (_loc3_ <= _loc4_ ? _loc3_ : _loc4_);
         var _loc6_:Number = _loc2_ >= _loc3_ && _loc2_ >= _loc4_ ? _loc2_ : (_loc3_ >= _loc4_ ? _loc3_ : _loc4_);
         var _loc7_:Number = _loc6_ - _loc5_;
         var _loc8_:* = {
            "P":0,
            "s":0,
            "l":0
         };
         _loc8_.l = _loc6_;
         if(_loc7_ != 0)
         {
            _loc8_.s = _loc7_ / _loc6_;
            _loc9_ = ((_loc6_ - _loc2_) / 6 + _loc7_ / 2) / _loc7_;
            _loc10_ = ((_loc6_ - _loc3_) / 6 + _loc7_ / 2) / _loc7_;
            _loc11_ = ((_loc6_ - _loc4_) / 6 + _loc7_ / 2) / _loc7_;
            if(_loc2_ == _loc6_)
            {
               _loc8_.P = _loc11_ - _loc10_;
            }
            else if(_loc3_ == _loc6_)
            {
               _loc8_.P = 1 / 3 + _loc9_ - _loc11_;
            }
            else if(_loc4_ == _loc6_)
            {
               _loc8_.P = 2 / 3 + _loc10_ - _loc9_;
            }
            if(Number(_loc8_.P) < 0)
            {
               _loc8_.P = Number(_loc8_.P) + 1;
            }
            if(Number(_loc8_.P) > 1)
            {
               _loc8_.P = Number(_loc8_.P) - 1;
            }
         }
         return _loc8_;
      }
      
      public static function hslToRgb(param1:Object) : Object
      {
         var _loc6_:int = 0;
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         var _loc10_:Number = NaN;
         var _loc2_:* = {
            "Ec":0,
            "u":0,
            "b":0
         };
         var _loc3_:Number = 0;
         var _loc4_:Number = 0;
         var _loc5_:Number = 0;
         if(Number(param1.s) == 0)
         {
            _loc2_.Ec = _loc2_.u = int(_loc2_.b = int(int(Math.round(param1.l * 255))));
         }
         else
         {
            _loc7_ = param1.P * 6;
            _loc6_ = int(Math.floor(_loc7_));
            _loc8_ = param1.l * (1 - param1.s);
            _loc9_ = param1.l * (1 - param1.s * (_loc7_ - _loc6_));
            _loc10_ = param1.l * (1 - param1.s * (1 - (_loc7_ - _loc6_)));
            if(_loc6_ == 0)
            {
               _loc3_ = Number(param1.l);
               _loc4_ = _loc10_;
               _loc5_ = _loc8_;
            }
            else if(_loc6_ == 1)
            {
               _loc3_ = _loc9_;
               _loc4_ = Number(param1.l);
               _loc5_ = _loc8_;
            }
            else if(_loc6_ == 2)
            {
               _loc3_ = _loc8_;
               _loc4_ = Number(param1.l);
               _loc5_ = _loc10_;
            }
            else if(_loc6_ == 3)
            {
               _loc3_ = _loc8_;
               _loc4_ = _loc9_;
               _loc5_ = Number(param1.l);
            }
            else if(_loc6_ == 4)
            {
               _loc3_ = _loc10_;
               _loc4_ = _loc8_;
               _loc5_ = Number(param1.l);
            }
            else
            {
               _loc3_ = Number(param1.l);
               _loc4_ = _loc8_;
               _loc5_ = _loc9_;
            }
            _loc2_.Ec = int(Math.round(_loc3_ * 255));
            _loc2_.u = int(Math.round(_loc4_ * 255));
            _loc2_.b = int(Math.round(_loc5_ * 255));
         }
         return _loc2_;
      }
      
      public static function rgbToMatrix(param1:Object) : Array
      {
         var _loc2_:Array = [];
         _loc2_ = _loc2_.concat([int(param1.Ec) / 255,0,0,0,0]);
         _loc2_ = _loc2_.concat([0,int(param1.u) / 255,0,0,0]);
         _loc2_ = _loc2_.concat([0,0,int(param1.b) / 255,0,0]);
         return _loc2_.concat([0,0,0,1,0]);
      }
      
      public static function intToHex(param1:int, param2:Object = undefined) : String
      {
         if(param2 == null)
         {
            param2 = 6;
         }
         var _loc3_:String = StringTools.hex(param1);
         while(_loc3_.length < param2)
         {
            _loc3_ = "n" + _loc3_;
         }
         return "#" + _loc3_;
      }
      
      public static function intToRgb(param1:int) : Object
      {
         return {
            "Ec":param1 >> 16,
            "u":param1 >> 8 & 255,
            "b":param1 & 255
         };
      }
      
      public static function intToRgba(param1:int) : Object
      {
         return {
            "a":param1 >> 24,
            "Ec":param1 >> 16 & 255,
            "u":param1 >> 8 & 255,
            "b":param1 & 255
         };
      }
      
      public static function intToHsl(param1:int) : Object
      {
         var _loc10_:Number = NaN;
         var _loc11_:Number = NaN;
         var _loc12_:Number = NaN;
         var _loc2_:* = {
            "Ec":param1 >> 16,
            "u":param1 >> 8 & 255,
            "b":param1 & 255
         };
         var _loc3_:Number = int(_loc2_.Ec) / 255;
         var _loc4_:Number = int(_loc2_.u) / 255;
         var _loc5_:Number = int(_loc2_.b) / 255;
         var _loc6_:Number = _loc3_ <= _loc4_ && _loc3_ <= _loc5_ ? _loc3_ : (_loc4_ <= _loc5_ ? _loc4_ : _loc5_);
         var _loc7_:Number = _loc3_ >= _loc4_ && _loc3_ >= _loc5_ ? _loc3_ : (_loc4_ >= _loc5_ ? _loc4_ : _loc5_);
         var _loc8_:Number = _loc7_ - _loc6_;
         var _loc9_:* = {
            "P":0,
            "s":0,
            "l":0
         };
         _loc9_.l = _loc7_;
         if(_loc8_ != 0)
         {
            _loc9_.s = _loc8_ / _loc7_;
            _loc10_ = ((_loc7_ - _loc3_) / 6 + _loc8_ / 2) / _loc8_;
            _loc11_ = ((_loc7_ - _loc4_) / 6 + _loc8_ / 2) / _loc8_;
            _loc12_ = ((_loc7_ - _loc5_) / 6 + _loc8_ / 2) / _loc8_;
            if(_loc3_ == _loc7_)
            {
               _loc9_.P = _loc12_ - _loc11_;
            }
            else if(_loc4_ == _loc7_)
            {
               _loc9_.P = 1 / 3 + _loc10_ - _loc12_;
            }
            else if(_loc5_ == _loc7_)
            {
               _loc9_.P = 2 / 3 + _loc11_ - _loc10_;
            }
            if(Number(_loc9_.P) < 0)
            {
               _loc9_.P = Number(_loc9_.P) + 1;
            }
            if(Number(_loc9_.P) > 1)
            {
               _loc9_.P = Number(_loc9_.P) - 1;
            }
         }
         return _loc9_;
      }
      
      public static function hslToInt(param1:Object) : int
      {
         var _loc7_:int = 0;
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         var _loc10_:Number = NaN;
         var _loc11_:Number = NaN;
         var _loc3_:* = {
            "Ec":0,
            "u":0,
            "b":0
         };
         var _loc4_:Number = 0;
         var _loc5_:Number = 0;
         var _loc6_:Number = 0;
         if(Number(param1.s) == 0)
         {
            _loc3_.Ec = _loc3_.u = int(_loc3_.b = int(int(Math.round(param1.l * 255))));
         }
         else
         {
            _loc8_ = param1.P * 6;
            _loc7_ = int(Math.floor(_loc8_));
            _loc9_ = param1.l * (1 - param1.s);
            _loc10_ = param1.l * (1 - param1.s * (_loc8_ - _loc7_));
            _loc11_ = param1.l * (1 - param1.s * (1 - (_loc8_ - _loc7_)));
            if(_loc7_ == 0)
            {
               _loc4_ = Number(param1.l);
               _loc5_ = _loc11_;
               _loc6_ = _loc9_;
            }
            else if(_loc7_ == 1)
            {
               _loc4_ = _loc10_;
               _loc5_ = Number(param1.l);
               _loc6_ = _loc9_;
            }
            else if(_loc7_ == 2)
            {
               _loc4_ = _loc9_;
               _loc5_ = Number(param1.l);
               _loc6_ = _loc11_;
            }
            else if(_loc7_ == 3)
            {
               _loc4_ = _loc9_;
               _loc5_ = _loc10_;
               _loc6_ = Number(param1.l);
            }
            else if(_loc7_ == 4)
            {
               _loc4_ = _loc11_;
               _loc5_ = _loc9_;
               _loc6_ = Number(param1.l);
            }
            else
            {
               _loc4_ = Number(param1.l);
               _loc5_ = _loc9_;
               _loc6_ = _loc10_;
            }
            _loc3_.Ec = int(Math.round(_loc4_ * 255));
            _loc3_.u = int(Math.round(_loc5_ * 255));
            _loc3_.b = int(Math.round(_loc6_ * 255));
         }
         var _loc2_:* = _loc3_;
         return int(_loc2_.Ec) << 16 | int(_loc2_.u) << 8 | int(_loc2_.b);
      }
      
      public static function rgbaToInt(param1:Object) : int
      {
         return int(param1.a) << 24 | int(param1.Ec) << 16 | int(param1.u) << 8 | int(param1.b);
      }
      
      public static function rgbaToRgb(param1:Object) : Object
      {
         return {
            "Ec":int(param1.Ec),
            "u":int(param1.u),
            "b":int(param1.b)
         };
      }
      
      public static function multiply(param1:Object, param2:Number) : Object
      {
         return {
            "Ec":int(int(param1.Ec) * param2),
            "u":int(int(param1.u) * param2),
            "b":int(int(param1.b) * param2)
         };
      }
      
      public static function saturation(param1:Object, param2:Number) : Object
      {
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         var _loc11_:Number = NaN;
         var _loc12_:Number = NaN;
         var _loc13_:Number = NaN;
         var _loc14_:int = 0;
         var _loc4_:Number = int(param1.Ec) / 255;
         var _loc5_:Number = int(param1.u) / 255;
         var _loc6_:Number = int(param1.b) / 255;
         _loc7_ = _loc4_ <= _loc5_ && _loc4_ <= _loc6_ ? _loc4_ : (_loc5_ <= _loc6_ ? _loc5_ : _loc6_);
         _loc8_ = _loc4_ >= _loc5_ && _loc4_ >= _loc6_ ? _loc4_ : (_loc5_ >= _loc6_ ? _loc5_ : _loc6_);
         _loc9_ = _loc8_ - _loc7_;
         var _loc10_:* = {
            "P":0,
            "s":0,
            "l":0
         };
         _loc10_.l = _loc8_;
         if(_loc9_ != 0)
         {
            _loc10_.s = _loc9_ / _loc8_;
            _loc11_ = ((_loc8_ - _loc4_) / 6 + _loc9_ / 2) / _loc9_;
            _loc12_ = ((_loc8_ - _loc5_) / 6 + _loc9_ / 2) / _loc9_;
            _loc13_ = ((_loc8_ - _loc6_) / 6 + _loc9_ / 2) / _loc9_;
            if(_loc4_ == _loc8_)
            {
               _loc10_.P = _loc13_ - _loc12_;
            }
            else if(_loc5_ == _loc8_)
            {
               _loc10_.P = 1 / 3 + _loc11_ - _loc13_;
            }
            else if(_loc6_ == _loc8_)
            {
               _loc10_.P = 2 / 3 + _loc12_ - _loc11_;
            }
            if(Number(_loc10_.P) < 0)
            {
               _loc10_.P = Number(_loc10_.P) + 1;
            }
            if(Number(_loc10_.P) > 1)
            {
               _loc10_.P = Number(_loc10_.P) - 1;
            }
         }
         var _loc3_:* = _loc10_;
         _loc3_.s = Number(Number(_loc3_.s) + param2);
         if(Number(_loc3_.s) > 1)
         {
            _loc3_.s = 1;
         }
         if(Number(_loc3_.s) < 0)
         {
            _loc3_.s = 0;
         }
         _loc10_ = {
            "Ec":0,
            "u":0,
            "b":0
         };
         _loc4_ = 0;
         _loc5_ = 0;
         _loc6_ = 0;
         if(Number(_loc3_.s) == 0)
         {
            _loc10_.Ec = _loc10_.u = int(_loc10_.b = int(int(Math.round(_loc3_.l * 255))));
         }
         else
         {
            _loc7_ = _loc3_.P * 6;
            _loc14_ = int(Math.floor(_loc7_));
            _loc8_ = _loc3_.l * (1 - _loc3_.s);
            _loc9_ = _loc3_.l * (1 - _loc3_.s * (_loc7_ - _loc14_));
            _loc11_ = _loc3_.l * (1 - _loc3_.s * (1 - (_loc7_ - _loc14_)));
            if(_loc14_ == 0)
            {
               _loc4_ = Number(_loc3_.l);
               _loc5_ = _loc11_;
               _loc6_ = _loc8_;
            }
            else if(_loc14_ == 1)
            {
               _loc4_ = _loc9_;
               _loc5_ = Number(_loc3_.l);
               _loc6_ = _loc8_;
            }
            else if(_loc14_ == 2)
            {
               _loc4_ = _loc8_;
               _loc5_ = Number(_loc3_.l);
               _loc6_ = _loc11_;
            }
            else if(_loc14_ == 3)
            {
               _loc4_ = _loc8_;
               _loc5_ = _loc9_;
               _loc6_ = Number(_loc3_.l);
            }
            else if(_loc14_ == 4)
            {
               _loc4_ = _loc11_;
               _loc5_ = _loc8_;
               _loc6_ = Number(_loc3_.l);
            }
            else
            {
               _loc4_ = Number(_loc3_.l);
               _loc5_ = _loc8_;
               _loc6_ = _loc9_;
            }
            _loc10_.Ec = int(Math.round(_loc4_ * 255));
            _loc10_.u = int(Math.round(_loc5_ * 255));
            _loc10_.b = int(Math.round(_loc6_ * 255));
         }
         return _loc10_;
      }
      
      public static function saturationInt(param1:int, param2:Number) : int
      {
         var _loc3_:* = Color.saturation({
            "Ec":param1 >> 16,
            "u":param1 >> 8 & 255,
            "b":param1 & 255
         },param2);
         return int(_loc3_.Ec) << 16 | int(_loc3_.u) << 8 | int(_loc3_.b);
      }
      
      public static function capBrightness(param1:Object, param2:Number) : Object
      {
         var _loc4_:Number = NaN;
         var _loc5_:Number = NaN;
         var _loc6_:Number = NaN;
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         var _loc10_:* = null;
         var _loc11_:Number = NaN;
         var _loc12_:Number = NaN;
         var _loc13_:Number = NaN;
         var _loc14_:int = 0;
         _loc4_ = int(param1.Ec) / 255;
         _loc5_ = int(param1.u) / 255;
         _loc6_ = int(param1.b) / 255;
         _loc7_ = _loc4_ <= _loc5_ && _loc4_ <= _loc6_ ? _loc4_ : (_loc5_ <= _loc6_ ? _loc5_ : _loc6_);
         _loc8_ = _loc4_ >= _loc5_ && _loc4_ >= _loc6_ ? _loc4_ : (_loc5_ >= _loc6_ ? _loc5_ : _loc6_);
         _loc9_ = _loc8_ - _loc7_;
         _loc10_ = {
            "P":0,
            "s":0,
            "l":0
         };
         _loc10_.l = _loc8_;
         if(_loc9_ != 0)
         {
            _loc10_.s = _loc9_ / _loc8_;
            _loc11_ = ((_loc8_ - _loc4_) / 6 + _loc9_ / 2) / _loc9_;
            _loc12_ = ((_loc8_ - _loc5_) / 6 + _loc9_ / 2) / _loc9_;
            _loc13_ = ((_loc8_ - _loc6_) / 6 + _loc9_ / 2) / _loc9_;
            if(_loc4_ == _loc8_)
            {
               _loc10_.P = _loc13_ - _loc12_;
            }
            else if(_loc5_ == _loc8_)
            {
               _loc10_.P = 1 / 3 + _loc11_ - _loc13_;
            }
            else if(_loc6_ == _loc8_)
            {
               _loc10_.P = 2 / 3 + _loc12_ - _loc11_;
            }
            if(Number(_loc10_.P) < 0)
            {
               _loc10_.P = Number(_loc10_.P) + 1;
            }
            if(Number(_loc10_.P) > 1)
            {
               _loc10_.P = Number(_loc10_.P) - 1;
            }
         }
         var _loc3_:* = _loc10_;
         if(Number(_loc3_.l) > param2)
         {
            _loc3_.l = param2;
            _loc10_ = {
               "Ec":0,
               "u":0,
               "b":0
            };
            _loc4_ = 0;
            _loc5_ = 0;
            _loc6_ = 0;
            if(Number(_loc3_.s) == 0)
            {
               _loc10_.Ec = _loc10_.u = int(_loc10_.b = int(int(Math.round(_loc3_.l * 255))));
            }
            else
            {
               _loc7_ = _loc3_.P * 6;
               _loc14_ = int(Math.floor(_loc7_));
               _loc8_ = _loc3_.l * (1 - _loc3_.s);
               _loc9_ = _loc3_.l * (1 - _loc3_.s * (_loc7_ - _loc14_));
               _loc11_ = _loc3_.l * (1 - _loc3_.s * (1 - (_loc7_ - _loc14_)));
               if(_loc14_ == 0)
               {
                  _loc4_ = Number(_loc3_.l);
                  _loc5_ = _loc11_;
                  _loc6_ = _loc8_;
               }
               else if(_loc14_ == 1)
               {
                  _loc4_ = _loc9_;
                  _loc5_ = Number(_loc3_.l);
                  _loc6_ = _loc8_;
               }
               else if(_loc14_ == 2)
               {
                  _loc4_ = _loc8_;
                  _loc5_ = Number(_loc3_.l);
                  _loc6_ = _loc11_;
               }
               else if(_loc14_ == 3)
               {
                  _loc4_ = _loc8_;
                  _loc5_ = _loc9_;
                  _loc6_ = Number(_loc3_.l);
               }
               else if(_loc14_ == 4)
               {
                  _loc4_ = _loc11_;
                  _loc5_ = _loc8_;
                  _loc6_ = Number(_loc3_.l);
               }
               else
               {
                  _loc4_ = Number(_loc3_.l);
                  _loc5_ = _loc8_;
                  _loc6_ = _loc9_;
               }
               _loc10_.Ec = int(Math.round(_loc4_ * 255));
               _loc10_.u = int(Math.round(_loc5_ * 255));
               _loc10_.b = int(Math.round(_loc6_ * 255));
            }
            return _loc10_;
         }
         return param1;
      }
      
      public static function capBrightnessInt(param1:int, param2:Number) : int
      {
         var _loc4_:* = null;
         var _loc5_:Number = NaN;
         var _loc6_:Number = NaN;
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         var _loc10_:Number = NaN;
         var _loc11_:* = null;
         var _loc12_:Number = NaN;
         var _loc13_:Number = NaN;
         var _loc14_:Number = NaN;
         var _loc15_:int = 0;
         _loc4_ = {
            "Ec":param1 >> 16,
            "u":param1 >> 8 & 255,
            "b":param1 & 255
         };
         _loc5_ = int(_loc4_.Ec) / 255;
         _loc6_ = int(_loc4_.u) / 255;
         _loc7_ = int(_loc4_.b) / 255;
         _loc8_ = _loc5_ <= _loc6_ && _loc5_ <= _loc7_ ? _loc5_ : (_loc6_ <= _loc7_ ? _loc6_ : _loc7_);
         _loc9_ = _loc5_ >= _loc6_ && _loc5_ >= _loc7_ ? _loc5_ : (_loc6_ >= _loc7_ ? _loc6_ : _loc7_);
         _loc10_ = _loc9_ - _loc8_;
         _loc11_ = {
            "P":0,
            "s":0,
            "l":0
         };
         _loc11_.l = _loc9_;
         if(_loc10_ != 0)
         {
            _loc11_.s = _loc10_ / _loc9_;
            _loc12_ = ((_loc9_ - _loc5_) / 6 + _loc10_ / 2) / _loc10_;
            _loc13_ = ((_loc9_ - _loc6_) / 6 + _loc10_ / 2) / _loc10_;
            _loc14_ = ((_loc9_ - _loc7_) / 6 + _loc10_ / 2) / _loc10_;
            if(_loc5_ == _loc9_)
            {
               _loc11_.P = _loc14_ - _loc13_;
            }
            else if(_loc6_ == _loc9_)
            {
               _loc11_.P = 1 / 3 + _loc12_ - _loc14_;
            }
            else if(_loc7_ == _loc9_)
            {
               _loc11_.P = 2 / 3 + _loc13_ - _loc12_;
            }
            if(Number(_loc11_.P) < 0)
            {
               _loc11_.P = Number(_loc11_.P) + 1;
            }
            if(Number(_loc11_.P) > 1)
            {
               _loc11_.P = Number(_loc11_.P) - 1;
            }
         }
         var _loc3_:* = _loc11_;
         if(Number(_loc3_.l) > param2)
         {
            _loc3_.l = param2;
            _loc11_ = {
               "Ec":0,
               "u":0,
               "b":0
            };
            _loc5_ = 0;
            _loc6_ = 0;
            _loc7_ = 0;
            if(Number(_loc3_.s) == 0)
            {
               _loc11_.Ec = _loc11_.u = int(_loc11_.b = int(int(Math.round(_loc3_.l * 255))));
            }
            else
            {
               _loc8_ = _loc3_.P * 6;
               _loc15_ = int(Math.floor(_loc8_));
               _loc9_ = _loc3_.l * (1 - _loc3_.s);
               _loc10_ = _loc3_.l * (1 - _loc3_.s * (_loc8_ - _loc15_));
               _loc12_ = _loc3_.l * (1 - _loc3_.s * (1 - (_loc8_ - _loc15_)));
               if(_loc15_ == 0)
               {
                  _loc5_ = Number(_loc3_.l);
                  _loc6_ = _loc12_;
                  _loc7_ = _loc9_;
               }
               else if(_loc15_ == 1)
               {
                  _loc5_ = _loc10_;
                  _loc6_ = Number(_loc3_.l);
                  _loc7_ = _loc9_;
               }
               else if(_loc15_ == 2)
               {
                  _loc5_ = _loc9_;
                  _loc6_ = Number(_loc3_.l);
                  _loc7_ = _loc12_;
               }
               else if(_loc15_ == 3)
               {
                  _loc5_ = _loc9_;
                  _loc6_ = _loc10_;
                  _loc7_ = Number(_loc3_.l);
               }
               else if(_loc15_ == 4)
               {
                  _loc5_ = _loc12_;
                  _loc6_ = _loc9_;
                  _loc7_ = Number(_loc3_.l);
               }
               else
               {
                  _loc5_ = Number(_loc3_.l);
                  _loc6_ = _loc9_;
                  _loc7_ = _loc10_;
               }
               _loc11_.Ec = int(Math.round(_loc5_ * 255));
               _loc11_.u = int(Math.round(_loc6_ * 255));
               _loc11_.b = int(Math.round(_loc7_ * 255));
            }
            _loc4_ = _loc11_;
            return int(_loc4_.Ec) << 16 | int(_loc4_.u) << 8 | int(_loc4_.b);
         }
         return param1;
      }
      
      public static function cap(param1:Object, param2:Number, param3:Number) : Object
      {
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         var _loc10_:Number = NaN;
         var _loc12_:Number = NaN;
         var _loc13_:Number = NaN;
         var _loc14_:Number = NaN;
         var _loc15_:int = 0;
         var _loc5_:Number = int(param1.Ec) / 255;
         var _loc6_:Number = int(param1.u) / 255;
         var _loc7_:Number = int(param1.b) / 255;
         _loc8_ = _loc5_ <= _loc6_ && _loc5_ <= _loc7_ ? _loc5_ : (_loc6_ <= _loc7_ ? _loc6_ : _loc7_);
         _loc9_ = _loc5_ >= _loc6_ && _loc5_ >= _loc7_ ? _loc5_ : (_loc6_ >= _loc7_ ? _loc6_ : _loc7_);
         _loc10_ = _loc9_ - _loc8_;
         var _loc11_:* = {
            "P":0,
            "s":0,
            "l":0
         };
         _loc11_.l = _loc9_;
         if(_loc10_ != 0)
         {
            _loc11_.s = _loc10_ / _loc9_;
            _loc12_ = ((_loc9_ - _loc5_) / 6 + _loc10_ / 2) / _loc10_;
            _loc13_ = ((_loc9_ - _loc6_) / 6 + _loc10_ / 2) / _loc10_;
            _loc14_ = ((_loc9_ - _loc7_) / 6 + _loc10_ / 2) / _loc10_;
            if(_loc5_ == _loc9_)
            {
               _loc11_.P = _loc14_ - _loc13_;
            }
            else if(_loc6_ == _loc9_)
            {
               _loc11_.P = 1 / 3 + _loc12_ - _loc14_;
            }
            else if(_loc7_ == _loc9_)
            {
               _loc11_.P = 2 / 3 + _loc13_ - _loc12_;
            }
            if(Number(_loc11_.P) < 0)
            {
               _loc11_.P = Number(_loc11_.P) + 1;
            }
            if(Number(_loc11_.P) > 1)
            {
               _loc11_.P = Number(_loc11_.P) - 1;
            }
         }
         var _loc4_:* = _loc11_;
         if(Number(_loc4_.s) > param2)
         {
            _loc4_.s = param2;
         }
         if(Number(_loc4_.l) > param3)
         {
            _loc4_.l = param3;
         }
         _loc11_ = {
            "Ec":0,
            "u":0,
            "b":0
         };
         _loc5_ = 0;
         _loc6_ = 0;
         _loc7_ = 0;
         if(Number(_loc4_.s) == 0)
         {
            _loc11_.Ec = _loc11_.u = int(_loc11_.b = int(int(Math.round(_loc4_.l * 255))));
         }
         else
         {
            _loc8_ = _loc4_.P * 6;
            _loc15_ = int(Math.floor(_loc8_));
            _loc9_ = _loc4_.l * (1 - _loc4_.s);
            _loc10_ = _loc4_.l * (1 - _loc4_.s * (_loc8_ - _loc15_));
            _loc12_ = _loc4_.l * (1 - _loc4_.s * (1 - (_loc8_ - _loc15_)));
            if(_loc15_ == 0)
            {
               _loc5_ = Number(_loc4_.l);
               _loc6_ = _loc12_;
               _loc7_ = _loc9_;
            }
            else if(_loc15_ == 1)
            {
               _loc5_ = _loc10_;
               _loc6_ = Number(_loc4_.l);
               _loc7_ = _loc9_;
            }
            else if(_loc15_ == 2)
            {
               _loc5_ = _loc9_;
               _loc6_ = Number(_loc4_.l);
               _loc7_ = _loc12_;
            }
            else if(_loc15_ == 3)
            {
               _loc5_ = _loc9_;
               _loc6_ = _loc10_;
               _loc7_ = Number(_loc4_.l);
            }
            else if(_loc15_ == 4)
            {
               _loc5_ = _loc12_;
               _loc6_ = _loc9_;
               _loc7_ = Number(_loc4_.l);
            }
            else
            {
               _loc5_ = Number(_loc4_.l);
               _loc6_ = _loc9_;
               _loc7_ = _loc10_;
            }
            _loc11_.Ec = int(Math.round(_loc5_ * 255));
            _loc11_.u = int(Math.round(_loc6_ * 255));
            _loc11_.b = int(Math.round(_loc7_ * 255));
         }
         return _loc11_;
      }
      
      public static function capInt(param1:int, param2:Number, param3:Number) : int
      {
         var _loc9_:Number = NaN;
         var _loc10_:Number = NaN;
         var _loc11_:Number = NaN;
         var _loc13_:Number = NaN;
         var _loc14_:Number = NaN;
         var _loc15_:Number = NaN;
         var _loc16_:int = 0;
         var _loc5_:* = {
            "Ec":param1 >> 16,
            "u":param1 >> 8 & 255,
            "b":param1 & 255
         };
         var _loc6_:Number = int(_loc5_.Ec) / 255;
         var _loc7_:Number = int(_loc5_.u) / 255;
         var _loc8_:Number = int(_loc5_.b) / 255;
         _loc9_ = _loc6_ <= _loc7_ && _loc6_ <= _loc8_ ? _loc6_ : (_loc7_ <= _loc8_ ? _loc7_ : _loc8_);
         _loc10_ = _loc6_ >= _loc7_ && _loc6_ >= _loc8_ ? _loc6_ : (_loc7_ >= _loc8_ ? _loc7_ : _loc8_);
         _loc11_ = _loc10_ - _loc9_;
         var _loc12_:* = {
            "P":0,
            "s":0,
            "l":0
         };
         _loc12_.l = _loc10_;
         if(_loc11_ != 0)
         {
            _loc12_.s = _loc11_ / _loc10_;
            _loc13_ = ((_loc10_ - _loc6_) / 6 + _loc11_ / 2) / _loc11_;
            _loc14_ = ((_loc10_ - _loc7_) / 6 + _loc11_ / 2) / _loc11_;
            _loc15_ = ((_loc10_ - _loc8_) / 6 + _loc11_ / 2) / _loc11_;
            if(_loc6_ == _loc10_)
            {
               _loc12_.P = _loc15_ - _loc14_;
            }
            else if(_loc7_ == _loc10_)
            {
               _loc12_.P = 1 / 3 + _loc13_ - _loc15_;
            }
            else if(_loc8_ == _loc10_)
            {
               _loc12_.P = 2 / 3 + _loc14_ - _loc13_;
            }
            if(Number(_loc12_.P) < 0)
            {
               _loc12_.P = Number(_loc12_.P) + 1;
            }
            if(Number(_loc12_.P) > 1)
            {
               _loc12_.P = Number(_loc12_.P) - 1;
            }
         }
         var _loc4_:* = _loc12_;
         if(Number(_loc4_.s) > param2)
         {
            _loc4_.s = param2;
         }
         if(Number(_loc4_.l) > param3)
         {
            _loc4_.l = param3;
         }
         _loc12_ = {
            "Ec":0,
            "u":0,
            "b":0
         };
         _loc6_ = 0;
         _loc7_ = 0;
         _loc8_ = 0;
         if(Number(_loc4_.s) == 0)
         {
            _loc12_.Ec = _loc12_.u = int(_loc12_.b = int(int(Math.round(_loc4_.l * 255))));
         }
         else
         {
            _loc9_ = _loc4_.P * 6;
            _loc16_ = int(Math.floor(_loc9_));
            _loc10_ = _loc4_.l * (1 - _loc4_.s);
            _loc11_ = _loc4_.l * (1 - _loc4_.s * (_loc9_ - _loc16_));
            _loc13_ = _loc4_.l * (1 - _loc4_.s * (1 - (_loc9_ - _loc16_)));
            if(_loc16_ == 0)
            {
               _loc6_ = Number(_loc4_.l);
               _loc7_ = _loc13_;
               _loc8_ = _loc10_;
            }
            else if(_loc16_ == 1)
            {
               _loc6_ = _loc11_;
               _loc7_ = Number(_loc4_.l);
               _loc8_ = _loc10_;
            }
            else if(_loc16_ == 2)
            {
               _loc6_ = _loc10_;
               _loc7_ = Number(_loc4_.l);
               _loc8_ = _loc13_;
            }
            else if(_loc16_ == 3)
            {
               _loc6_ = _loc10_;
               _loc7_ = _loc11_;
               _loc8_ = Number(_loc4_.l);
            }
            else if(_loc16_ == 4)
            {
               _loc6_ = _loc13_;
               _loc7_ = _loc10_;
               _loc8_ = Number(_loc4_.l);
            }
            else
            {
               _loc6_ = Number(_loc4_.l);
               _loc7_ = _loc10_;
               _loc8_ = _loc11_;
            }
            _loc12_.Ec = int(Math.round(_loc6_ * 255));
            _loc12_.u = int(Math.round(_loc7_ * 255));
            _loc12_.b = int(Math.round(_loc8_ * 255));
         }
         _loc5_ = _loc12_;
         return int(_loc5_.Ec) << 16 | int(_loc5_.u) << 8 | int(_loc5_.b);
      }
      
      public static function hue(param1:Object, param2:Number) : Object
      {
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         var _loc11_:Number = NaN;
         var _loc12_:Number = NaN;
         var _loc13_:Number = NaN;
         var _loc14_:int = 0;
         var _loc4_:Number = int(param1.Ec) / 255;
         var _loc5_:Number = int(param1.u) / 255;
         var _loc6_:Number = int(param1.b) / 255;
         _loc7_ = _loc4_ <= _loc5_ && _loc4_ <= _loc6_ ? _loc4_ : (_loc5_ <= _loc6_ ? _loc5_ : _loc6_);
         _loc8_ = _loc4_ >= _loc5_ && _loc4_ >= _loc6_ ? _loc4_ : (_loc5_ >= _loc6_ ? _loc5_ : _loc6_);
         _loc9_ = _loc8_ - _loc7_;
         var _loc10_:* = {
            "P":0,
            "s":0,
            "l":0
         };
         _loc10_.l = _loc8_;
         if(_loc9_ != 0)
         {
            _loc10_.s = _loc9_ / _loc8_;
            _loc11_ = ((_loc8_ - _loc4_) / 6 + _loc9_ / 2) / _loc9_;
            _loc12_ = ((_loc8_ - _loc5_) / 6 + _loc9_ / 2) / _loc9_;
            _loc13_ = ((_loc8_ - _loc6_) / 6 + _loc9_ / 2) / _loc9_;
            if(_loc4_ == _loc8_)
            {
               _loc10_.P = _loc13_ - _loc12_;
            }
            else if(_loc5_ == _loc8_)
            {
               _loc10_.P = 1 / 3 + _loc11_ - _loc13_;
            }
            else if(_loc6_ == _loc8_)
            {
               _loc10_.P = 2 / 3 + _loc12_ - _loc11_;
            }
            if(Number(_loc10_.P) < 0)
            {
               _loc10_.P = Number(_loc10_.P) + 1;
            }
            if(Number(_loc10_.P) > 1)
            {
               _loc10_.P = Number(_loc10_.P) - 1;
            }
         }
         var _loc3_:* = _loc10_;
         _loc3_.P = Number(Number(_loc3_.P) + param2);
         if(Number(_loc3_.P) > 1)
         {
            _loc3_.P = 1;
         }
         if(Number(_loc3_.P) < 0)
         {
            _loc3_.P = 0;
         }
         _loc10_ = {
            "Ec":0,
            "u":0,
            "b":0
         };
         _loc4_ = 0;
         _loc5_ = 0;
         _loc6_ = 0;
         if(Number(_loc3_.s) == 0)
         {
            _loc10_.Ec = _loc10_.u = int(_loc10_.b = int(int(Math.round(_loc3_.l * 255))));
         }
         else
         {
            _loc7_ = _loc3_.P * 6;
            _loc14_ = int(Math.floor(_loc7_));
            _loc8_ = _loc3_.l * (1 - _loc3_.s);
            _loc9_ = _loc3_.l * (1 - _loc3_.s * (_loc7_ - _loc14_));
            _loc11_ = _loc3_.l * (1 - _loc3_.s * (1 - (_loc7_ - _loc14_)));
            if(_loc14_ == 0)
            {
               _loc4_ = Number(_loc3_.l);
               _loc5_ = _loc11_;
               _loc6_ = _loc8_;
            }
            else if(_loc14_ == 1)
            {
               _loc4_ = _loc9_;
               _loc5_ = Number(_loc3_.l);
               _loc6_ = _loc8_;
            }
            else if(_loc14_ == 2)
            {
               _loc4_ = _loc8_;
               _loc5_ = Number(_loc3_.l);
               _loc6_ = _loc11_;
            }
            else if(_loc14_ == 3)
            {
               _loc4_ = _loc8_;
               _loc5_ = _loc9_;
               _loc6_ = Number(_loc3_.l);
            }
            else if(_loc14_ == 4)
            {
               _loc4_ = _loc11_;
               _loc5_ = _loc8_;
               _loc6_ = Number(_loc3_.l);
            }
            else
            {
               _loc4_ = Number(_loc3_.l);
               _loc5_ = _loc8_;
               _loc6_ = _loc9_;
            }
            _loc10_.Ec = int(Math.round(_loc4_ * 255));
            _loc10_.u = int(Math.round(_loc5_ * 255));
            _loc10_.b = int(Math.round(_loc6_ * 255));
         }
         return _loc10_;
      }
      
      public static function §\n\t\x04o\x02§(param1:int, param2:Number) : int
      {
         var _loc3_:* = Color.hue({
            "Ec":param1 >> 16,
            "u":param1 >> 8 & 255,
            "b":param1 & 255
         },param2);
         return int(_loc3_.Ec) << 16 | int(_loc3_.u) << 8 | int(_loc3_.b);
      }
      
      public static function brightnessInt(param1:int, param2:Number) : int
      {
         var _loc3_:* = Color.brightness({
            "Ec":param1 >> 16,
            "u":param1 >> 8 & 255,
            "b":param1 & 255
         },param2);
         return int(_loc3_.Ec) << 16 | int(_loc3_.u) << 8 | int(_loc3_.b);
      }
      
      public static function brightness(param1:Object, param2:Number) : Object
      {
         var _loc4_:Number = NaN;
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         var _loc11_:Number = NaN;
         var _loc12_:Number = NaN;
         var _loc13_:Number = NaN;
         var _loc14_:int = 0;
         _loc4_ = int(param1.Ec) / 255;
         var _loc5_:Number = int(param1.u) / 255;
         var _loc6_:Number = int(param1.b) / 255;
         _loc7_ = _loc4_ <= _loc5_ && _loc4_ <= _loc6_ ? _loc4_ : (_loc5_ <= _loc6_ ? _loc5_ : _loc6_);
         _loc8_ = _loc4_ >= _loc5_ && _loc4_ >= _loc6_ ? _loc4_ : (_loc5_ >= _loc6_ ? _loc5_ : _loc6_);
         _loc9_ = _loc8_ - _loc7_;
         var _loc10_:* = {
            "P":0,
            "s":0,
            "l":0
         };
         _loc10_.l = _loc8_;
         if(_loc9_ != 0)
         {
            _loc10_.s = _loc9_ / _loc8_;
            _loc11_ = ((_loc8_ - _loc4_) / 6 + _loc9_ / 2) / _loc9_;
            _loc12_ = ((_loc8_ - _loc5_) / 6 + _loc9_ / 2) / _loc9_;
            _loc13_ = ((_loc8_ - _loc6_) / 6 + _loc9_ / 2) / _loc9_;
            if(_loc4_ == _loc8_)
            {
               _loc10_.P = _loc13_ - _loc12_;
            }
            else if(_loc5_ == _loc8_)
            {
               _loc10_.P = 1 / 3 + _loc11_ - _loc13_;
            }
            else if(_loc6_ == _loc8_)
            {
               _loc10_.P = 2 / 3 + _loc12_ - _loc11_;
            }
            if(Number(_loc10_.P) < 0)
            {
               _loc10_.P = Number(_loc10_.P) + 1;
            }
            if(Number(_loc10_.P) > 1)
            {
               _loc10_.P = Number(_loc10_.P) - 1;
            }
         }
         var _loc3_:* = _loc10_;
         if(param2 < 0)
         {
            _loc3_.l = Number(Number(_loc3_.l) + param2);
            if(Number(_loc3_.l) < 0)
            {
               _loc3_.l = 0;
            }
         }
         else
         {
            _loc4_ = 1 - _loc3_.l;
            if(_loc4_ > param2)
            {
               _loc3_.l = Number(Number(_loc3_.l) + param2);
            }
            else
            {
               _loc3_.l = 1;
               _loc3_.s = Number(_loc3_.s) - (param2 - _loc4_);
               if(Number(_loc3_.s) < 0)
               {
                  _loc3_.s = 0;
               }
            }
         }
         _loc10_ = {
            "Ec":0,
            "u":0,
            "b":0
         };
         _loc4_ = 0;
         _loc5_ = 0;
         _loc6_ = 0;
         if(Number(_loc3_.s) == 0)
         {
            _loc10_.Ec = _loc10_.u = int(_loc10_.b = int(int(Math.round(_loc3_.l * 255))));
         }
         else
         {
            _loc7_ = _loc3_.P * 6;
            _loc14_ = int(Math.floor(_loc7_));
            _loc8_ = _loc3_.l * (1 - _loc3_.s);
            _loc9_ = _loc3_.l * (1 - _loc3_.s * (_loc7_ - _loc14_));
            _loc11_ = _loc3_.l * (1 - _loc3_.s * (1 - (_loc7_ - _loc14_)));
            if(_loc14_ == 0)
            {
               _loc4_ = Number(_loc3_.l);
               _loc5_ = _loc11_;
               _loc6_ = _loc8_;
            }
            else if(_loc14_ == 1)
            {
               _loc4_ = _loc9_;
               _loc5_ = Number(_loc3_.l);
               _loc6_ = _loc8_;
            }
            else if(_loc14_ == 2)
            {
               _loc4_ = _loc8_;
               _loc5_ = Number(_loc3_.l);
               _loc6_ = _loc11_;
            }
            else if(_loc14_ == 3)
            {
               _loc4_ = _loc8_;
               _loc5_ = _loc9_;
               _loc6_ = Number(_loc3_.l);
            }
            else if(_loc14_ == 4)
            {
               _loc4_ = _loc11_;
               _loc5_ = _loc8_;
               _loc6_ = Number(_loc3_.l);
            }
            else
            {
               _loc4_ = Number(_loc3_.l);
               _loc5_ = _loc8_;
               _loc6_ = _loc9_;
            }
            _loc10_.Ec = int(Math.round(_loc4_ * 255));
            _loc10_.u = int(Math.round(_loc5_ * 255));
            _loc10_.b = int(Math.round(_loc6_ * 255));
         }
         return _loc10_;
      }
      
      public static function desaturate(param1:Object, param2:Number) : Object
      {
         var _loc3_:Number = Number(Number(0.3 * int(param1.Ec) + 0.59 * int(param1.u)) + 0.11 * int(param1.b));
         return {
            "Ec":int(Number(_loc3_ * param2 + int(param1.Ec) * (1 - param2))),
            "u":int(Number(_loc3_ * param2 + int(param1.u) * (1 - param2))),
            "b":int(Number(_loc3_ * param2 + int(param1.b) * (1 - param2)))
         };
      }
      
      public static function §Rg\t\x02§(param1:int, param2:Number) : int
      {
         var _loc4_:* = {
            "Ec":param1 >> 16,
            "u":param1 >> 8 & 255,
            "b":param1 & 255
         };
         var _loc5_:Number = Number(Number(0.3 * int(_loc4_.Ec) + 0.59 * int(_loc4_.u)) + 0.11 * int(_loc4_.b));
         var _loc3_:* = {
            "Ec":int(Number(_loc5_ * param2 + int(_loc4_.Ec) * (1 - param2))),
            "u":int(Number(_loc5_ * param2 + int(_loc4_.u) * (1 - param2))),
            "b":int(Number(_loc5_ * param2 + int(_loc4_.b) * (1 - param2)))
         };
         return int(_loc3_.Ec) << 16 | int(_loc3_.u) << 8 | int(_loc3_.b);
      }
      
      public static function getAlpha(param1:int) : uint
      {
         return param1 >> 24;
      }
      
      public static function addAlphaF(param1:int, param2:Object = undefined) : uint
      {
         if(param2 == null)
         {
            param2 = 1;
         }
         return int(param2 * 255) << 24 | param1;
      }
      
      public static function addAlphaI(param1:int, param2:Object = undefined) : uint
      {
         if(param2 == null)
         {
            param2 = 255;
         }
         return param2 << 24 | param1;
      }
      
      public static function randomColor(param1:Number, param2:Object = undefined, param3:Object = undefined) : int
      {
         var _loc10_:int = 0;
         var _loc11_:Number = NaN;
         var _loc12_:Number = NaN;
         var _loc13_:Number = NaN;
         var _loc14_:Number = NaN;
         if(param2 == null)
         {
            param2 = 1;
         }
         if(param3 == null)
         {
            param3 = 1;
         }
         var _loc4_:* = {
            "P":param1,
            "s":param2,
            "l":param3
         };
         var _loc6_:* = {
            "Ec":0,
            "u":0,
            "b":0
         };
         var _loc7_:Number = 0;
         var _loc8_:Number = 0;
         var _loc9_:Number = 0;
         if(Number(_loc4_.s) == 0)
         {
            _loc6_.Ec = _loc6_.u = int(_loc6_.b = int(int(Math.round(_loc4_.l * 255))));
         }
         else
         {
            _loc11_ = _loc4_.P * 6;
            _loc10_ = int(Math.floor(_loc11_));
            _loc12_ = _loc4_.l * (1 - _loc4_.s);
            _loc13_ = _loc4_.l * (1 - _loc4_.s * (_loc11_ - _loc10_));
            _loc14_ = _loc4_.l * (1 - _loc4_.s * (1 - (_loc11_ - _loc10_)));
            if(_loc10_ == 0)
            {
               _loc7_ = Number(_loc4_.l);
               _loc8_ = _loc14_;
               _loc9_ = _loc12_;
            }
            else if(_loc10_ == 1)
            {
               _loc7_ = _loc13_;
               _loc8_ = Number(_loc4_.l);
               _loc9_ = _loc12_;
            }
            else if(_loc10_ == 2)
            {
               _loc7_ = _loc12_;
               _loc8_ = Number(_loc4_.l);
               _loc9_ = _loc14_;
            }
            else if(_loc10_ == 3)
            {
               _loc7_ = _loc12_;
               _loc8_ = _loc13_;
               _loc9_ = Number(_loc4_.l);
            }
            else if(_loc10_ == 4)
            {
               _loc7_ = _loc14_;
               _loc8_ = _loc12_;
               _loc9_ = Number(_loc4_.l);
            }
            else
            {
               _loc7_ = Number(_loc4_.l);
               _loc8_ = _loc12_;
               _loc9_ = _loc13_;
            }
            _loc6_.Ec = int(Math.round(_loc7_ * 255));
            _loc6_.u = int(Math.round(_loc8_ * 255));
            _loc6_.b = int(Math.round(_loc9_ * 255));
         }
         var _loc5_:* = _loc6_;
         return int(_loc5_.Ec) << 16 | int(_loc5_.u) << 8 | int(_loc5_.b);
      }
      
      public static function getRainbowColor(param1:Number, param2:Object = undefined, param3:Object = undefined) : int
      {
         var _loc10_:int = 0;
         var _loc11_:Number = NaN;
         var _loc12_:Number = NaN;
         var _loc13_:Number = NaN;
         var _loc14_:Number = NaN;
         if(param2 == null)
         {
            param2 = 1;
         }
         if(param3 == null)
         {
            param3 = 1;
         }
         var _loc4_:* = {
            "P":param1,
            "s":param2,
            "l":param3
         };
         var _loc6_:* = {
            "Ec":0,
            "u":0,
            "b":0
         };
         var _loc7_:Number = 0;
         var _loc8_:Number = 0;
         var _loc9_:Number = 0;
         if(Number(_loc4_.s) == 0)
         {
            _loc6_.Ec = _loc6_.u = int(_loc6_.b = int(int(Math.round(_loc4_.l * 255))));
         }
         else
         {
            _loc11_ = _loc4_.P * 6;
            _loc10_ = int(Math.floor(_loc11_));
            _loc12_ = _loc4_.l * (1 - _loc4_.s);
            _loc13_ = _loc4_.l * (1 - _loc4_.s * (_loc11_ - _loc10_));
            _loc14_ = _loc4_.l * (1 - _loc4_.s * (1 - (_loc11_ - _loc10_)));
            if(_loc10_ == 0)
            {
               _loc7_ = Number(_loc4_.l);
               _loc8_ = _loc14_;
               _loc9_ = _loc12_;
            }
            else if(_loc10_ == 1)
            {
               _loc7_ = _loc13_;
               _loc8_ = Number(_loc4_.l);
               _loc9_ = _loc12_;
            }
            else if(_loc10_ == 2)
            {
               _loc7_ = _loc12_;
               _loc8_ = Number(_loc4_.l);
               _loc9_ = _loc14_;
            }
            else if(_loc10_ == 3)
            {
               _loc7_ = _loc12_;
               _loc8_ = _loc13_;
               _loc9_ = Number(_loc4_.l);
            }
            else if(_loc10_ == 4)
            {
               _loc7_ = _loc14_;
               _loc8_ = _loc12_;
               _loc9_ = Number(_loc4_.l);
            }
            else
            {
               _loc7_ = Number(_loc4_.l);
               _loc8_ = _loc12_;
               _loc9_ = _loc13_;
            }
            _loc6_.Ec = int(Math.round(_loc7_ * 255));
            _loc6_.u = int(Math.round(_loc8_ * 255));
            _loc6_.b = int(Math.round(_loc9_ * 255));
         }
         var _loc5_:* = _loc6_;
         return int(_loc5_.Ec) << 16 | int(_loc5_.u) << 8 | int(_loc5_.b);
      }
      
      public static function getRgbRatio(param1:Object = undefined, param2:Object = undefined) : Object
      {
         var _loc4_:int = 0;
         if(param1 != null)
         {
            _loc4_ = param1;
            §§push({
               "Ec":_loc4_ >> 16,
               "u":_loc4_ >> 8 & 255,
               "b":_loc4_ & 255
            });
         }
         else
         {
            §§push(param2);
         }
         var _loc3_:* = §§pop();
         _loc4_ = int(_loc3_.b) > int(_loc3_.u) && int(_loc3_.b) > int(_loc3_.Ec) ? int(_loc3_.b) : (int(_loc3_.u) > int(_loc3_.Ec) && int(_loc3_.u) > int(_loc3_.b) ? int(_loc3_.u) : int(_loc3_.Ec));
         return {
            "Ec":int(_loc3_.Ec) / _loc4_,
            "u":int(_loc3_.u) / _loc4_,
            "b":int(_loc3_.b) / _loc4_
         };
      }
      
      public static function getLuminosityPerception(param1:Object) : Number
      {
         return Number(Math.sqrt(Number(Number(0.241 * (int(param1.Ec) * int(param1.Ec)) + 0.691 * (int(param1.u) * int(param1.u))) + 0.068 * (int(param1.b) * int(param1.b)))));
      }
      
      public static function autoContrast(param1:int, param2:Object = undefined, param3:Object = undefined) : Object
      {
         if(param2 == null)
         {
            param2 = 0;
         }
         if(param3 == null)
         {
            param3 = 16777215;
         }
         var _loc4_:* = {
            "Ec":param1 >> 16,
            "u":param1 >> 8 & 255,
            "b":param1 & 255
         };
         return Number(Math.sqrt(Number(Number(0.241 * (int(_loc4_.Ec) * int(_loc4_.Ec)) + 0.691 * (int(_loc4_.u) * int(_loc4_.u))) + 0.068 * (int(_loc4_.b) * int(_loc4_.b))))) >= 180 ? param2 : param3;
      }
      
      public static function getLuminosity(param1:Object = undefined, param2:Object = undefined) : Number
      {
         var _loc3_:Number = NaN;
         var _loc4_:Number = NaN;
         var _loc5_:Number = NaN;
         var _loc6_:Number = NaN;
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         var _loc9_:* = null;
         var _loc10_:Number = NaN;
         var _loc11_:Number = NaN;
         var _loc12_:Number = NaN;
         var _loc13_:int = 0;
         var _loc14_:* = null;
         if(param1 != null)
         {
            _loc3_ = int(param1.Ec) / 255;
            _loc4_ = int(param1.u) / 255;
            _loc5_ = int(param1.b) / 255;
            _loc6_ = _loc3_ <= _loc4_ && _loc3_ <= _loc5_ ? _loc3_ : (_loc4_ <= _loc5_ ? _loc4_ : _loc5_);
            _loc7_ = _loc3_ >= _loc4_ && _loc3_ >= _loc5_ ? _loc3_ : (_loc4_ >= _loc5_ ? _loc4_ : _loc5_);
            _loc8_ = _loc7_ - _loc6_;
            _loc9_ = {
               "P":0,
               "s":0,
               "l":0
            };
            _loc9_.l = _loc7_;
            if(_loc8_ != 0)
            {
               _loc9_.s = _loc8_ / _loc7_;
               _loc10_ = ((_loc7_ - _loc3_) / 6 + _loc8_ / 2) / _loc8_;
               _loc11_ = ((_loc7_ - _loc4_) / 6 + _loc8_ / 2) / _loc8_;
               _loc12_ = ((_loc7_ - _loc5_) / 6 + _loc8_ / 2) / _loc8_;
               if(_loc3_ == _loc7_)
               {
                  _loc9_.P = _loc12_ - _loc11_;
               }
               else if(_loc4_ == _loc7_)
               {
                  _loc9_.P = 1 / 3 + _loc10_ - _loc12_;
               }
               else if(_loc5_ == _loc7_)
               {
                  _loc9_.P = 2 / 3 + _loc11_ - _loc10_;
               }
               if(Number(_loc9_.P) < 0)
               {
                  _loc9_.P = Number(_loc9_.P) + 1;
               }
               if(Number(_loc9_.P) > 1)
               {
                  _loc9_.P = Number(_loc9_.P) - 1;
               }
            }
            §§push(Number(_loc9_.l));
         }
         else
         {
            _loc13_ = param2;
            _loc9_ = {
               "Ec":_loc13_ >> 16,
               "u":_loc13_ >> 8 & 255,
               "b":_loc13_ & 255
            };
            _loc3_ = int(_loc9_.Ec) / 255;
            _loc4_ = int(_loc9_.u) / 255;
            _loc5_ = int(_loc9_.b) / 255;
            _loc6_ = _loc3_ <= _loc4_ && _loc3_ <= _loc5_ ? _loc3_ : (_loc4_ <= _loc5_ ? _loc4_ : _loc5_);
            _loc7_ = _loc3_ >= _loc4_ && _loc3_ >= _loc5_ ? _loc3_ : (_loc4_ >= _loc5_ ? _loc4_ : _loc5_);
            _loc8_ = _loc7_ - _loc6_;
            _loc14_ = {
               "P":0,
               "s":0,
               "l":0
            };
            _loc14_.l = _loc7_;
            if(_loc8_ != 0)
            {
               _loc14_.s = _loc8_ / _loc7_;
               _loc10_ = ((_loc7_ - _loc3_) / 6 + _loc8_ / 2) / _loc8_;
               _loc11_ = ((_loc7_ - _loc4_) / 6 + _loc8_ / 2) / _loc8_;
               _loc12_ = ((_loc7_ - _loc5_) / 6 + _loc8_ / 2) / _loc8_;
               if(_loc3_ == _loc7_)
               {
                  _loc14_.P = _loc12_ - _loc11_;
               }
               else if(_loc4_ == _loc7_)
               {
                  _loc14_.P = 1 / 3 + _loc10_ - _loc12_;
               }
               else if(_loc5_ == _loc7_)
               {
                  _loc14_.P = 2 / 3 + _loc11_ - _loc10_;
               }
               if(Number(_loc14_.P) < 0)
               {
                  _loc14_.P = Number(_loc14_.P) + 1;
               }
               if(Number(_loc14_.P) > 1)
               {
                  _loc14_.P = Number(_loc14_.P) - 1;
               }
            }
            §§push(Number(_loc14_.l));
         }
         return §§pop();
      }
      
      public static function setLuminosity(param1:Object, param2:Number) : Object
      {
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         var _loc11_:Number = NaN;
         var _loc12_:Number = NaN;
         var _loc13_:Number = NaN;
         var _loc14_:int = 0;
         var _loc4_:Number = int(param1.Ec) / 255;
         var _loc5_:Number = int(param1.u) / 255;
         var _loc6_:Number = int(param1.b) / 255;
         _loc7_ = _loc4_ <= _loc5_ && _loc4_ <= _loc6_ ? _loc4_ : (_loc5_ <= _loc6_ ? _loc5_ : _loc6_);
         _loc8_ = _loc4_ >= _loc5_ && _loc4_ >= _loc6_ ? _loc4_ : (_loc5_ >= _loc6_ ? _loc5_ : _loc6_);
         _loc9_ = _loc8_ - _loc7_;
         var _loc10_:* = {
            "P":0,
            "s":0,
            "l":0
         };
         _loc10_.l = _loc8_;
         if(_loc9_ != 0)
         {
            _loc10_.s = _loc9_ / _loc8_;
            _loc11_ = ((_loc8_ - _loc4_) / 6 + _loc9_ / 2) / _loc9_;
            _loc12_ = ((_loc8_ - _loc5_) / 6 + _loc9_ / 2) / _loc9_;
            _loc13_ = ((_loc8_ - _loc6_) / 6 + _loc9_ / 2) / _loc9_;
            if(_loc4_ == _loc8_)
            {
               _loc10_.P = _loc13_ - _loc12_;
            }
            else if(_loc5_ == _loc8_)
            {
               _loc10_.P = 1 / 3 + _loc11_ - _loc13_;
            }
            else if(_loc6_ == _loc8_)
            {
               _loc10_.P = 2 / 3 + _loc12_ - _loc11_;
            }
            if(Number(_loc10_.P) < 0)
            {
               _loc10_.P = Number(_loc10_.P) + 1;
            }
            if(Number(_loc10_.P) > 1)
            {
               _loc10_.P = Number(_loc10_.P) - 1;
            }
         }
         var _loc3_:* = _loc10_;
         _loc3_.l = param2;
         _loc10_ = {
            "Ec":0,
            "u":0,
            "b":0
         };
         _loc4_ = 0;
         _loc5_ = 0;
         _loc6_ = 0;
         if(Number(_loc3_.s) == 0)
         {
            _loc10_.Ec = _loc10_.u = int(_loc10_.b = int(int(Math.round(_loc3_.l * 255))));
         }
         else
         {
            _loc7_ = _loc3_.P * 6;
            _loc14_ = int(Math.floor(_loc7_));
            _loc8_ = _loc3_.l * (1 - _loc3_.s);
            _loc9_ = _loc3_.l * (1 - _loc3_.s * (_loc7_ - _loc14_));
            _loc11_ = _loc3_.l * (1 - _loc3_.s * (1 - (_loc7_ - _loc14_)));
            if(_loc14_ == 0)
            {
               _loc4_ = Number(_loc3_.l);
               _loc5_ = _loc11_;
               _loc6_ = _loc8_;
            }
            else if(_loc14_ == 1)
            {
               _loc4_ = _loc9_;
               _loc5_ = Number(_loc3_.l);
               _loc6_ = _loc8_;
            }
            else if(_loc14_ == 2)
            {
               _loc4_ = _loc8_;
               _loc5_ = Number(_loc3_.l);
               _loc6_ = _loc11_;
            }
            else if(_loc14_ == 3)
            {
               _loc4_ = _loc8_;
               _loc5_ = _loc9_;
               _loc6_ = Number(_loc3_.l);
            }
            else if(_loc14_ == 4)
            {
               _loc4_ = _loc11_;
               _loc5_ = _loc8_;
               _loc6_ = Number(_loc3_.l);
            }
            else
            {
               _loc4_ = Number(_loc3_.l);
               _loc5_ = _loc8_;
               _loc6_ = _loc9_;
            }
            _loc10_.Ec = int(Math.round(_loc4_ * 255));
            _loc10_.u = int(Math.round(_loc5_ * 255));
            _loc10_.b = int(Math.round(_loc6_ * 255));
         }
         return _loc10_;
      }
      
      public static function setLuminosityInt(param1:int, param2:Number) : int
      {
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         var _loc10_:Number = NaN;
         var _loc12_:Number = NaN;
         var _loc13_:Number = NaN;
         var _loc14_:Number = NaN;
         var _loc15_:int = 0;
         var _loc4_:* = {
            "Ec":param1 >> 16,
            "u":param1 >> 8 & 255,
            "b":param1 & 255
         };
         var _loc5_:Number = int(_loc4_.Ec) / 255;
         var _loc6_:Number = int(_loc4_.u) / 255;
         var _loc7_:Number = int(_loc4_.b) / 255;
         _loc8_ = _loc5_ <= _loc6_ && _loc5_ <= _loc7_ ? _loc5_ : (_loc6_ <= _loc7_ ? _loc6_ : _loc7_);
         _loc9_ = _loc5_ >= _loc6_ && _loc5_ >= _loc7_ ? _loc5_ : (_loc6_ >= _loc7_ ? _loc6_ : _loc7_);
         _loc10_ = _loc9_ - _loc8_;
         var _loc11_:* = {
            "P":0,
            "s":0,
            "l":0
         };
         _loc11_.l = _loc9_;
         if(_loc10_ != 0)
         {
            _loc11_.s = _loc10_ / _loc9_;
            _loc12_ = ((_loc9_ - _loc5_) / 6 + _loc10_ / 2) / _loc10_;
            _loc13_ = ((_loc9_ - _loc6_) / 6 + _loc10_ / 2) / _loc10_;
            _loc14_ = ((_loc9_ - _loc7_) / 6 + _loc10_ / 2) / _loc10_;
            if(_loc5_ == _loc9_)
            {
               _loc11_.P = _loc14_ - _loc13_;
            }
            else if(_loc6_ == _loc9_)
            {
               _loc11_.P = 1 / 3 + _loc12_ - _loc14_;
            }
            else if(_loc7_ == _loc9_)
            {
               _loc11_.P = 2 / 3 + _loc13_ - _loc12_;
            }
            if(Number(_loc11_.P) < 0)
            {
               _loc11_.P = Number(_loc11_.P) + 1;
            }
            if(Number(_loc11_.P) > 1)
            {
               _loc11_.P = Number(_loc11_.P) - 1;
            }
         }
         var _loc3_:* = _loc11_;
         _loc3_.l = param2;
         _loc11_ = {
            "Ec":0,
            "u":0,
            "b":0
         };
         _loc5_ = 0;
         _loc6_ = 0;
         _loc7_ = 0;
         if(Number(_loc3_.s) == 0)
         {
            _loc11_.Ec = _loc11_.u = int(_loc11_.b = int(int(Math.round(_loc3_.l * 255))));
         }
         else
         {
            _loc8_ = _loc3_.P * 6;
            _loc15_ = int(Math.floor(_loc8_));
            _loc9_ = _loc3_.l * (1 - _loc3_.s);
            _loc10_ = _loc3_.l * (1 - _loc3_.s * (_loc8_ - _loc15_));
            _loc12_ = _loc3_.l * (1 - _loc3_.s * (1 - (_loc8_ - _loc15_)));
            if(_loc15_ == 0)
            {
               _loc5_ = Number(_loc3_.l);
               _loc6_ = _loc12_;
               _loc7_ = _loc9_;
            }
            else if(_loc15_ == 1)
            {
               _loc5_ = _loc10_;
               _loc6_ = Number(_loc3_.l);
               _loc7_ = _loc9_;
            }
            else if(_loc15_ == 2)
            {
               _loc5_ = _loc9_;
               _loc6_ = Number(_loc3_.l);
               _loc7_ = _loc12_;
            }
            else if(_loc15_ == 3)
            {
               _loc5_ = _loc9_;
               _loc6_ = _loc10_;
               _loc7_ = Number(_loc3_.l);
            }
            else if(_loc15_ == 4)
            {
               _loc5_ = _loc12_;
               _loc6_ = _loc9_;
               _loc7_ = Number(_loc3_.l);
            }
            else
            {
               _loc5_ = Number(_loc3_.l);
               _loc6_ = _loc9_;
               _loc7_ = _loc10_;
            }
            _loc11_.Ec = int(Math.round(_loc5_ * 255));
            _loc11_.u = int(Math.round(_loc6_ * 255));
            _loc11_.b = int(Math.round(_loc7_ * 255));
         }
         _loc4_ = _loc11_;
         return int(_loc4_.Ec) << 16 | int(_loc4_.u) << 8 | int(_loc4_.b);
      }
      
      public static function offsetColor(param1:Object, param2:int) : Object
      {
         return {
            "Ec":int(Number(Math.max(0,Number(Math.min(255,int(param1.Ec) + param2))))),
            "u":int(Number(Math.max(0,Number(Math.min(255,int(param1.u) + param2))))),
            "b":int(Number(Math.max(0,Number(Math.min(255,int(param1.b) + param2)))))
         };
      }
      
      public static function §\t\nmn§(param1:Object, param2:int) : Object
      {
         return {
            "Ec":int(Number(Math.max(0,Number(Math.min(255,int(param1.Ec) + param2))))),
            "u":int(Number(Math.max(0,Number(Math.min(255,int(param1.u) + param2))))),
            "b":int(Number(Math.max(0,Number(Math.min(255,int(param1.b) + param2))))),
            "a":int(param1.a)
         };
      }
      
      public static function offsetColorInt(param1:int, param2:int) : int
      {
         var _loc4_:* = {
            "Ec":param1 >> 16,
            "u":param1 >> 8 & 255,
            "b":param1 & 255
         };
         var _loc3_:* = {
            "Ec":int(Number(Math.max(0,Number(Math.min(255,int(_loc4_.Ec) + param2))))),
            "u":int(Number(Math.max(0,Number(Math.min(255,int(_loc4_.u) + param2))))),
            "b":int(Number(Math.max(0,Number(Math.min(255,int(_loc4_.b) + param2)))))
         };
         return int(_loc3_.Ec) << 16 | int(_loc3_.u) << 8 | int(_loc3_.b);
      }
      
      public static function interpolatePal(param1:Array, param2:Array, param3:Number) : Array
      {
         var _loc7_:int = 0;
         var _loc8_:* = null;
         var _loc9_:* = null;
         var _loc10_:Number = NaN;
         var _loc4_:Array = [];
         var _loc5_:int = 0;
         var _loc6_:int = int(param1.length);
         while(_loc5_ < _loc6_)
         {
            _loc7_ = _loc5_++;
            _loc8_ = param1[_loc7_];
            _loc9_ = param2[_loc7_];
            _loc10_ = param3;
            _loc10_ = Number(Math.min(1,Number(Math.max(0,_loc10_))));
            _loc4_[_loc7_] = {
               "Ec":int(Number(int(_loc8_.Ec) + (int(_loc9_.Ec) - int(_loc8_.Ec)) * _loc10_)),
               "u":int(Number(int(_loc8_.u) + (int(_loc9_.u) - int(_loc8_.u)) * _loc10_)),
               "b":int(Number(int(_loc8_.b) + (int(_loc9_.b) - int(_loc8_.b)) * _loc10_))
            };
         }
         return _loc4_;
      }
      
      public static function interpolate(param1:Object, param2:Object, param3:Number) : Object
      {
         param3 = Number(Math.min(1,Number(Math.max(0,param3))));
         return {
            "Ec":int(Number(int(param1.Ec) + (int(param2.Ec) - int(param1.Ec)) * param3)),
            "u":int(Number(int(param1.u) + (int(param2.u) - int(param1.u)) * param3)),
            "b":int(Number(int(param1.b) + (int(param2.b) - int(param1.b)) * param3))
         };
      }
      
      public static function interpolateInt(param1:int, param2:int, param3:Number) : int
      {
         var _loc5_:* = {
            "Ec":param1 >> 16,
            "u":param1 >> 8 & 255,
            "b":param1 & 255
         };
         var _loc6_:* = {
            "Ec":param2 >> 16,
            "u":param2 >> 8 & 255,
            "b":param2 & 255
         };
         var _loc7_:Number = param3;
         _loc7_ = Number(Math.min(1,Number(Math.max(0,_loc7_))));
         var _loc4_:* = {
            "Ec":int(Number(int(_loc5_.Ec) + (int(_loc6_.Ec) - int(_loc5_.Ec)) * _loc7_)),
            "u":int(Number(int(_loc5_.u) + (int(_loc6_.u) - int(_loc5_.u)) * _loc7_)),
            "b":int(Number(int(_loc5_.b) + (int(_loc6_.b) - int(_loc5_.b)) * _loc7_))
         };
         return int(_loc4_.Ec) << 16 | int(_loc4_.u) << 8 | int(_loc4_.b);
      }
      
      public static function darken(param1:int, param2:Number) : int
      {
         var _loc4_:* = {
            "Ec":param1 >> 16,
            "u":param1 >> 8 & 255,
            "b":param1 & 255
         };
         var _loc5_:* = Color.BLACK;
         var _loc6_:Number = param2;
         _loc6_ = Number(Math.min(1,Number(Math.max(0,_loc6_))));
         var _loc3_:* = {
            "Ec":int(Number(int(_loc4_.Ec) + (int(_loc5_.Ec) - int(_loc4_.Ec)) * _loc6_)),
            "u":int(Number(int(_loc4_.u) + (int(_loc5_.u) - int(_loc4_.u)) * _loc6_)),
            "b":int(Number(int(_loc4_.b) + (int(_loc5_.b) - int(_loc4_.b)) * _loc6_))
         };
         return int(_loc3_.Ec) << 16 | int(_loc3_.u) << 8 | int(_loc3_.b);
      }
      
      public static function lighten(param1:int, param2:Number) : int
      {
         var _loc4_:* = {
            "Ec":param1 >> 16,
            "u":param1 >> 8 & 255,
            "b":param1 & 255
         };
         var _loc5_:* = Color.WHITE;
         var _loc6_:Number = param2;
         _loc6_ = Number(Math.min(1,Number(Math.max(0,_loc6_))));
         var _loc3_:* = {
            "Ec":int(Number(int(_loc4_.Ec) + (int(_loc5_.Ec) - int(_loc4_.Ec)) * _loc6_)),
            "u":int(Number(int(_loc4_.u) + (int(_loc5_.u) - int(_loc4_.u)) * _loc6_)),
            "b":int(Number(int(_loc4_.b) + (int(_loc5_.b) - int(_loc4_.b)) * _loc6_))
         };
         return int(_loc3_.Ec) << 16 | int(_loc3_.u) << 8 | int(_loc3_.b);
      }
      
      public static function getDarkenCT(param1:Number) : ColorTransform
      {
         var _loc2_:ColorTransform = new ColorTransform();
         _loc2_.redMultiplier = _loc2_.greenMultiplier = Number(_loc2_.blueMultiplier = Number(1 - param1));
         return _loc2_;
      }
      
      public static function getSimpleCT(param1:Object = undefined, param2:Object = undefined, param3:Object = undefined) : ColorTransform
      {
         var _loc4_:int = 0;
         if(param1 == null)
         {
            _loc4_ = param2;
            param1 = {
               "Ec":_loc4_ >> 16,
               "u":_loc4_ >> 8 & 255,
               "b":_loc4_ & 255
            };
         }
         var _loc5_:ColorTransform = new ColorTransform();
         _loc5_.redOffset = int(param1.Ec) - 127;
         _loc5_.greenOffset = int(param1.u) - 127;
         _loc5_.blueOffset = int(param1.b) - 127;
         if(param3 != null)
         {
            _loc5_.alphaMultiplier = param3;
         }
         return _loc5_;
      }
      
      public static function getColorizeCT(param1:Object = undefined, param2:Object = undefined, param3:Number = undefined) : ColorTransform
      {
         var _loc4_:int = 0;
         if(param1 == null)
         {
            _loc4_ = param2;
            param1 = {
               "Ec":_loc4_ >> 16,
               "u":_loc4_ >> 8 & 255,
               "b":_loc4_ & 255
            };
         }
         var _loc5_:ColorTransform = new ColorTransform();
         _loc5_.redOffset = int(param1.Ec) * param3;
         _loc5_.greenOffset = int(param1.u) * param3;
         _loc5_.blueOffset = int(param1.b) * param3;
         _loc5_.redMultiplier = 1 - param3;
         _loc5_.greenMultiplier = 1 - param3;
         _loc5_.blueMultiplier = 1 - param3;
         return _loc5_;
      }
      
      public static function getContrastFilter(param1:Number) : ColorMatrixFilter
      {
         var _loc2_:Number = Number(1 + param1 * 1.5);
         var _loc3_:Number = -64 * param1;
         var _loc4_:Array = [_loc2_,0,0,0,_loc3_,0,_loc2_,0,0,_loc3_,0,0,_loc2_,0,_loc3_,0,0,0,1,0];
         return new ColorMatrixFilter(_loc4_);
      }
      
      public static function getSaturationFilter(param1:Number) : ColorMatrixFilter
      {
         var _loc3_:* = null;
         var _loc4_:* = null as Array;
         var _loc5_:* = null as Array;
         var _loc6_:* = null as Array;
         var _loc7_:* = null as Array;
         var _loc8_:* = null as Array;
         var _loc9_:* = null as Array;
         var _loc10_:Number = NaN;
         var _loc11_:* = null as Array;
         var _loc12_:int = 0;
         var _loc13_:int = 0;
         var _loc14_:int = 0;
         if(param1 > 0)
         {
            §§push([Number(1 + param1),-param1,0,0,0,-param1,Number(1 + param1),0,0,0,0,-param1,Number(1 + param1),0,0,0,0,0,1,0]);
         }
         else
         {
            _loc3_ = -param1;
            _loc4_ = [1,0,0,0,0];
            _loc5_ = [0,1,0,0,0];
            _loc6_ = [0,0,1,0,0];
            _loc7_ = [0,0,0,1,0];
            _loc8_ = [0.3,0.59,0.11,0,0];
            _loc9_ = [];
            _loc10_ = _loc3_;
            _loc11_ = [];
            _loc12_ = 0;
            _loc13_ = int(_loc4_.length);
            while(_loc12_ < _loc13_)
            {
               _loc14_ = _loc12_++;
               _loc11_[_loc14_] = Number(Number(_loc4_[_loc14_]) + (_loc8_[_loc14_] - _loc4_[_loc14_]) * _loc10_);
            }
            _loc9_ = _loc9_.concat(_loc11_);
            _loc10_ = _loc3_;
            _loc11_ = [];
            _loc12_ = 0;
            _loc13_ = int(_loc5_.length);
            while(_loc12_ < _loc13_)
            {
               _loc14_ = _loc12_++;
               _loc11_[_loc14_] = Number(Number(_loc5_[_loc14_]) + (_loc8_[_loc14_] - _loc5_[_loc14_]) * _loc10_);
            }
            _loc9_ = _loc9_.concat(_loc11_);
            _loc10_ = _loc3_;
            _loc11_ = [];
            _loc12_ = 0;
            _loc13_ = int(_loc6_.length);
            while(_loc12_ < _loc13_)
            {
               _loc14_ = _loc12_++;
               _loc11_[_loc14_] = Number(Number(_loc6_[_loc14_]) + (_loc8_[_loc14_] - _loc6_[_loc14_]) * _loc10_);
            }
            _loc9_ = _loc9_.concat(_loc11_);
            _loc9_ = _loc9_.concat(_loc7_);
            §§push(_loc9_);
         }
         var _loc2_:Array = §§pop();
         return new ColorMatrixFilter(_loc2_);
      }
      
      public static function getInterpolatedCT(param1:Object, param2:Object, param3:Number) : ColorTransform
      {
         var _loc6_:int = 0;
         var _loc5_:Number = param3;
         _loc5_ = Number(Math.min(1,Number(Math.max(0,_loc5_))));
         var _loc4_:* = {
            "Ec":int(Number(int(param1.Ec) + (int(param2.Ec) - int(param1.Ec)) * _loc5_)),
            "u":int(Number(int(param1.u) + (int(param2.u) - int(param1.u)) * _loc5_)),
            "b":int(Number(int(param1.b) + (int(param2.b) - int(param1.b)) * _loc5_))
         };
         if(_loc4_ == null)
         {
            _loc6_ = null;
            _loc4_ = {
               "Ec":_loc6_ >> 16,
               "u":_loc6_ >> 8 & 255,
               "b":_loc6_ & 255
            };
         }
         var _loc7_:ColorTransform = new ColorTransform();
         _loc7_.redOffset = int(_loc4_.Ec) - 127;
         _loc7_.greenOffset = int(_loc4_.u) - 127;
         _loc7_.blueOffset = int(_loc4_.b) - 127;
         return _loc7_;
      }
      
      public static function getPaletteAverage(param1:Array) : Object
      {
         var _loc4_:* = null;
         if(int(param1.length) < 0)
         {
            return Reflect.copy(Color.BLACK);
         }
         var _loc2_:* = {
            "Ec":0,
            "u":0,
            "b":0
         };
         var _loc3_:int = 0;
         while(_loc3_ < int(param1.length))
         {
            _loc4_ = param1[_loc3_];
            _loc3_++;
            _loc2_.Ec = int(_loc2_.Ec) + int(_loc4_.Ec);
            _loc2_.u = int(_loc2_.u) + int(_loc4_.u);
            _loc2_.b = int(_loc2_.b) + int(_loc4_.b);
         }
         return {
            "Ec":int(int(_loc2_.Ec) / int(param1.length)),
            "u":int(int(_loc2_.u) / int(param1.length)),
            "b":int(int(_loc2_.b) / int(param1.length))
         };
      }
      
      public static function getColorizeMatrixFilter(param1:int, param2:Object = undefined, param3:Object = undefined) : ColorMatrixFilter
      {
         if(param2 == null)
         {
            param2 = 1;
         }
         if(param3 == null)
         {
            param3 = 1;
         }
         var _loc4_:* = {
            "Ec":param1 >> 16,
            "u":param1 >> 8 & 255,
            "b":param1 & 255
         };
         var _loc5_:Number = param2 * int(_loc4_.Ec) / 255;
         var _loc6_:Number = param2 * int(_loc4_.u) / 255;
         var _loc7_:Number = param2 * int(_loc4_.b) / 255;
         var _loc8_:Array = [Number(param3 + _loc5_),_loc5_,_loc5_,0,0,_loc6_,Number(param3 + _loc6_),_loc6_,0,0,_loc7_,_loc7_,Number(param3 + _loc7_),0,0,0,0,0,1,0];
         return new ColorMatrixFilter(_loc8_);
      }
      
      public static function getDesaturateMatrix(param1:Object = undefined) : Array
      {
         var _loc12_:int = 0;
         if(param1 == null)
         {
            param1 = 1;
         }
         var _loc2_:Array = [1,0,0,0,0];
         var _loc3_:Array = [0,1,0,0,0];
         var _loc4_:Array = [0,0,1,0,0];
         var _loc5_:Array = [0,0,0,1,0];
         var _loc6_:Array = [0.3,0.59,0.11,0,0];
         var _loc7_:Array = [];
         var _loc8_:Number = param1;
         var _loc9_:Array = [];
         var _loc10_:int = 0;
         var _loc11_:int = int(_loc2_.length);
         while(_loc10_ < _loc11_)
         {
            _loc12_ = _loc10_++;
            _loc9_[_loc12_] = Number(Number(_loc2_[_loc12_]) + (_loc6_[_loc12_] - _loc2_[_loc12_]) * _loc8_);
         }
         _loc7_ = _loc7_.concat(_loc9_);
         _loc8_ = param1;
         _loc9_ = [];
         _loc10_ = 0;
         _loc11_ = int(_loc3_.length);
         while(_loc10_ < _loc11_)
         {
            _loc12_ = _loc10_++;
            _loc9_[_loc12_] = Number(Number(_loc3_[_loc12_]) + (_loc6_[_loc12_] - _loc3_[_loc12_]) * _loc8_);
         }
         _loc7_ = _loc7_.concat(_loc9_);
         _loc8_ = param1;
         _loc9_ = [];
         _loc10_ = 0;
         _loc11_ = int(_loc4_.length);
         while(_loc10_ < _loc11_)
         {
            _loc12_ = _loc10_++;
            _loc9_[_loc12_] = Number(Number(_loc4_[_loc12_]) + (_loc6_[_loc12_] - _loc4_[_loc12_]) * _loc8_);
         }
         _loc7_ = _loc7_.concat(_loc9_);
         return _loc7_.concat(_loc5_);
      }
      
      public static function interpolateArrays(param1:Array, param2:Array, param3:Number) : Array
      {
         var _loc7_:int = 0;
         var _loc4_:Array = [];
         var _loc5_:int = 0;
         var _loc6_:int = int(param1.length);
         while(_loc5_ < _loc6_)
         {
            _loc7_ = _loc5_++;
            _loc4_[_loc7_] = Number(Number(param1[_loc7_]) + (param2[_loc7_] - param1[_loc7_]) * param3);
         }
         return _loc4_;
      }
      
      public static function replaceChannel(param1:BitmapData, param2:Boolean, param3:Boolean, param4:Boolean, param5:int, param6:Object = undefined) : void
      {
         var _loc9_:* = null as BitmapData;
         var _loc10_:* = null;
         var _loc11_:* = null as BitmapData;
         var _loc12_:* = null as BitmapData;
         var _loc14_:* = null as BitmapData;
         if(param6 == null)
         {
            param6 = 1.5;
         }
         var _loc7_:Point = new Point(0,0);
         if(param2)
         {
            _loc9_ = param1.clone();
            _loc10_ = {
               "a":0,
               "Ec":(!!param2 ? 0 : 1) * 255,
               "u":255,
               "b":255
            };
            _loc9_.threshold(_loc9_,_loc9_.rect,new Point(0,0),">",0,0,int(_loc10_.a) << 24 | int(_loc10_.Ec) << 16 | int(_loc10_.u) << 8 | int(_loc10_.b));
            §§push(_loc9_);
         }
         else
         {
            §§push(null);
         }
         var _loc8_:BitmapData = §§pop();
         if(param3)
         {
            _loc11_ = param1.clone();
            _loc10_ = {
               "a":0,
               "Ec":255,
               "u":(!!param3 ? 0 : 1) * 255,
               "b":255
            };
            _loc11_.threshold(_loc11_,_loc11_.rect,new Point(0,0),">",0,0,int(_loc10_.a) << 24 | int(_loc10_.Ec) << 16 | int(_loc10_.u) << 8 | int(_loc10_.b));
            §§push(_loc11_);
         }
         else
         {
            §§push(null);
         }
         _loc9_ = §§pop();
         if(param4)
         {
            _loc12_ = param1.clone();
            _loc10_ = {
               "a":0,
               "Ec":255,
               "u":255,
               "b":(!!param4 ? 0 : 1) * 255
            };
            _loc12_.threshold(_loc12_,_loc12_.rect,new Point(0,0),">",0,0,int(_loc10_.a) << 24 | int(_loc10_.Ec) << 16 | int(_loc10_.u) << 8 | int(_loc10_.b));
            §§push(_loc12_);
         }
         else
         {
            §§push(null);
         }
         _loc11_ = §§pop();
         _loc12_ = null;
         var _loc13_:Boolean = !!param2 && param3 || !!param2 && param4 || !!param3 && param4;
         if(_loc13_)
         {
            _loc14_ = param1.clone();
            _loc10_ = {
               "a":0,
               "Ec":(!!param2 ? 0 : 1) * 255,
               "u":(!!param3 ? 0 : 1) * 255,
               "b":(!!param4 ? 0 : 1) * 255
            };
            _loc14_.threshold(_loc14_,_loc14_.rect,new Point(0,0),">",0,0,int(_loc10_.a) << 24 | int(_loc10_.Ec) << 16 | int(_loc10_.u) << 8 | int(_loc10_.b));
            _loc12_ = _loc14_;
            if(param2)
            {
               _loc10_ = _loc12_.compare(_loc8_);
               _loc12_.fillRect(_loc12_.rect,0);
               if(Type.§typeof§(_loc10_) != ValueType.TInt)
               {
                  _loc12_.fillRect(_loc12_.rect,0);
                  _loc14_ = _loc10_;
                  _loc12_.draw(_loc14_);
                  _loc14_.dispose();
               }
            }
            if(param3)
            {
               _loc10_ = _loc12_.compare(_loc9_);
               _loc12_.fillRect(_loc12_.rect,0);
               if(Type.§typeof§(_loc10_) != ValueType.TInt)
               {
                  _loc12_.fillRect(_loc12_.rect,0);
                  _loc14_ = _loc10_;
                  _loc12_.draw(_loc14_);
                  _loc14_.dispose();
               }
            }
            if(param4)
            {
               _loc10_ = _loc12_.compare(_loc11_);
               _loc12_.fillRect(_loc12_.rect,0);
               if(Type.§typeof§(_loc10_) != ValueType.TInt)
               {
                  _loc12_.fillRect(_loc12_.rect,0);
                  _loc14_ = _loc10_;
                  _loc12_.draw(_loc14_);
                  _loc14_.dispose();
               }
            }
         }
         else
         {
            if(param2)
            {
               _loc12_ = _loc8_;
            }
            if(param3)
            {
               _loc12_ = _loc9_;
            }
            if(param4)
            {
               _loc12_ = _loc11_;
            }
         }
         _loc10_ = {
            "Ec":param5 >> 16,
            "u":param5 >> 8 & 255,
            "b":param5 & 255
         };
         var _loc15_:Number = !!_loc13_ ? 0.5 : Number(1);
         var _loc16_:Number = _loc15_ * int(_loc10_.Ec) / 255 * param6;
         var _loc17_:Number = _loc15_ * int(_loc10_.u) / 255 * param6;
         var _loc18_:Number = _loc15_ * int(_loc10_.b) / 255 * param6;
         var _loc19_:int = !!param2 ? 1 : 0;
         var _loc20_:int = !!param3 ? 1 : 0;
         var _loc21_:int = !!param4 ? 1 : 0;
         var _loc22_:Array = [_loc19_ * _loc16_,_loc20_ * _loc16_,_loc21_ * _loc16_,0,0,_loc19_ * _loc17_,_loc20_ * _loc17_,_loc21_ * _loc17_,0,0,_loc19_ * _loc18_,_loc20_ * _loc18_,_loc21_ * _loc18_,0,0,0,0,0,1,0];
         _loc12_.applyFilter(_loc12_,_loc12_.rect,_loc7_,new ColorMatrixFilter(_loc22_));
         param1.draw(_loc12_);
         if(_loc8_ != null)
         {
            _loc8_.dispose();
         }
         if(_loc9_ != null)
         {
            _loc9_.dispose();
         }
         if(_loc11_ != null)
         {
            _loc11_.dispose();
         }
         _loc12_.dispose();
      }
      
      public static function extractChannel(param1:BitmapData, param2:Boolean, param3:Boolean, param4:Boolean) : BitmapData
      {
         var _loc5_:BitmapData = param1.clone();
         var _loc6_:* = {
            "a":0,
            "Ec":(!!param2 ? 0 : 1) * 255,
            "u":(!!param3 ? 0 : 1) * 255,
            "b":(!!param4 ? 0 : 1) * 255
         };
         _loc5_.threshold(_loc5_,_loc5_.rect,new Point(0,0),">",0,0,int(_loc6_.a) << 24 | int(_loc6_.Ec) << 16 | int(_loc6_.u) << 8 | int(_loc6_.b));
         return _loc5_;
      }
      
      public static function compareBitmaps(param1:BitmapData, param2:BitmapData) : void
      {
         var _loc4_:* = null as BitmapData;
         var _loc3_:* = param1.compare(param2);
         param1.fillRect(param1.rect,0);
         if(Type.§typeof§(_loc3_) != ValueType.TInt)
         {
            param1.fillRect(param1.rect,0);
            _loc4_ = _loc3_;
            param1.draw(_loc4_);
            _loc4_.dispose();
         }
      }
      
      public static function getChannelMask(param1:int, param2:int, param3:int) : int
      {
         var _loc4_:* = {
            "a":0,
            "Ec":param1 * 255,
            "u":param2 * 255,
            "b":param3 * 255
         };
         return int(_loc4_.a) << 24 | int(_loc4_.Ec) << 16 | int(_loc4_.u) << 8 | int(_loc4_.b);
      }
      
      public static function makeNicePalette(param1:int, param2:Object = undefined, param3:Object = undefined, param4:Object = undefined) : Array
      {
         var _loc6_:* = null;
         var _loc12_:int = 0;
         var _loc13_:* = null;
         var _loc14_:Number = NaN;
         if(param2 == null)
         {
            param2 = 0;
         }
         if(param4 == null)
         {
            param4 = false;
         }
         var _loc5_:* = {
            "Ec":param1 >> 16,
            "u":param1 >> 8 & 255,
            "b":param1 & 255
         };
         if(param3 == null)
         {
            _loc6_ = {
               "Ec":int(Number(Math.max(0,Number(Math.min(255,int(_loc5_.Ec) + 100))))),
               "u":int(Number(Math.max(0,Number(Math.min(255,int(_loc5_.u) + 100))))),
               "b":int(Number(Math.max(0,Number(Math.min(255,int(_loc5_.b) + 100)))))
            };
            param3 = int(_loc6_.Ec) << 16 | int(_loc6_.u) << 8 | int(_loc6_.b);
         }
         var _loc7_:int = param2;
         _loc6_ = {
            "Ec":_loc7_ >> 16,
            "u":_loc7_ >> 8 & 255,
            "b":_loc7_ & 255
         };
         _loc7_ = param3;
         var _loc8_:* = {
            "Ec":_loc7_ >> 16,
            "u":_loc7_ >> 8 & 255,
            "b":_loc7_ & 255
         };
         var _loc9_:Array = [];
         _loc7_ = 200;
         var _loc10_:int = 256 - _loc7_;
         var _loc11_:int = 0;
         while(_loc11_ < 256)
         {
            _loc12_ = _loc11_++;
            if(_loc12_ < _loc7_)
            {
               _loc14_ = _loc12_ / _loc7_;
               _loc14_ = Number(Math.min(1,Number(Math.max(0,_loc14_))));
               _loc13_ = {
                  "Ec":int(Number(int(_loc6_.Ec) + (int(_loc5_.Ec) - int(_loc6_.Ec)) * _loc14_)),
                  "u":int(Number(int(_loc6_.u) + (int(_loc5_.u) - int(_loc6_.u)) * _loc14_)),
                  "b":int(Number(int(_loc6_.b) + (int(_loc5_.b) - int(_loc6_.b)) * _loc14_))
               };
               _loc9_[_loc12_] = int(_loc13_.Ec) << 16 | int(_loc13_.u) << 8 | int(_loc13_.b);
            }
            else
            {
               _loc14_ = (_loc12_ - _loc7_) / _loc10_;
               _loc14_ = Number(Math.min(1,Number(Math.max(0,_loc14_))));
               _loc13_ = {
                  "Ec":int(Number(int(_loc5_.Ec) + (int(_loc8_.Ec) - int(_loc5_.Ec)) * _loc14_)),
                  "u":int(Number(int(_loc5_.u) + (int(_loc8_.u) - int(_loc5_.u)) * _loc14_)),
                  "b":int(Number(int(_loc5_.b) + (int(_loc8_.b) - int(_loc5_.b)) * _loc14_))
               };
               _loc9_[_loc12_] = int(_loc13_.Ec) << 16 | int(_loc13_.u) << 8 | int(_loc13_.b);
            }
            if(param4)
            {
               _loc9_[_loc12_] = -16777216 | int(_loc9_[_loc12_]);
            }
         }
         return _loc9_;
      }
      
      public static function makePalette(param1:Array) : Array
      {
         var _loc5_:int = 0;
         var _loc6_:Number = NaN;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:* = null;
         var _loc10_:* = null;
         var _loc11_:* = null;
         var _loc12_:Number = NaN;
         var _loc2_:Array = [];
         var _loc3_:Number = 256 / (int(param1.length) - 1);
         var _loc4_:int = 0;
         while(_loc4_ < 256)
         {
            _loc5_ = _loc4_++;
            _loc6_ = _loc5_ / _loc3_;
            _loc7_ = int(param1[int(_loc6_)]);
            _loc8_ = int(param1[int(_loc6_) + 1]);
            _loc10_ = {
               "Ec":_loc7_ >> 16,
               "u":_loc7_ >> 8 & 255,
               "b":_loc7_ & 255
            };
            _loc11_ = {
               "Ec":_loc8_ >> 16,
               "u":_loc8_ >> 8 & 255,
               "b":_loc8_ & 255
            };
            _loc12_ = _loc6_ - int(_loc6_);
            _loc12_ = Number(Math.min(1,Number(Math.max(0,_loc12_))));
            _loc9_ = {
               "Ec":int(Number(int(_loc10_.Ec) + (int(_loc11_.Ec) - int(_loc10_.Ec)) * _loc12_)),
               "u":int(Number(int(_loc10_.u) + (int(_loc11_.u) - int(_loc10_.u)) * _loc12_)),
               "b":int(Number(int(_loc10_.b) + (int(_loc11_.b) - int(_loc10_.b)) * _loc12_))
            };
            _loc2_[_loc5_] = int(_loc9_.Ec) << 16 | int(_loc9_.u) << 8 | int(_loc9_.b);
         }
         return _loc2_;
      }
      
      public static function paintBitmap(param1:BitmapData, param2:Array, param3:Array, param4:Array, param5:Array = undefined, param6:Array = undefined, param7:Array = undefined) : void
      {
         var _loc10_:uint = 0;
         var _loc11_:uint = 0;
         var _loc12_:int = 0;
         var _loc13_:int = 0;
         var _loc14_:int = 0;
         var _loc15_:int = 0;
         var _loc8_:Rectangle = param1.rect;
         var _loc9_:ByteArray = param1.getPixels(_loc8_);
         _loc9_.position = 0;
         if(_loc9_.bytesAvailable > 0)
         {
            ApplicationDomain.currentDomain.domainMemory = _loc9_;
            _loc10_ = 0;
            _loc11_ = _loc9_.bytesAvailable;
            while(_loc10_ < _loc11_)
            {
               if(li8(_loc10_) > 0)
               {
                  _loc12_ = li8(_loc10_ + 1);
                  _loc13_ = li8(_loc10_ + 2);
                  _loc14_ = li8(_loc10_ + 3);
                  if(_loc12_ != _loc13_ || _loc13_ != _loc14_ || _loc12_ != _loc14_)
                  {
                     _loc15_ = _loc13_ == 0 && _loc14_ == 0 ? int(param2[_loc12_]) : (_loc12_ == 0 && _loc14_ == 0 ? int(param3[_loc13_]) : (_loc12_ == 0 && _loc13_ == 0 ? int(param4[_loc14_]) : (_loc12_ != 0 && _loc13_ != 0 ? int(param5[_loc12_]) : (_loc12_ != 0 && _loc14_ != 0 ? int(param6[_loc12_]) : (_loc13_ != 0 && _loc14_ != 0 ? int(param7[_loc13_]) : 16711935)))));
                     si8(_loc15_ >> 16,_loc10_ + 1);
                     si8(_loc15_ >> 8,_loc10_ + 2);
                     si8(_loc15_,_loc10_ + 3);
                  }
               }
               _loc10_ = _loc10_ + 4;
            }
            param1.setPixels(_loc8_,_loc9_);
         }
      }
      
      public static function paintBitmapGrays(param1:BitmapData, param2:Array) : void
      {
         var _loc5_:uint = 0;
         var _loc6_:uint = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc3_:Rectangle = param1.rect;
         var _loc4_:ByteArray = param1.getPixels(_loc3_);
         _loc4_.position = 0;
         if(_loc4_.bytesAvailable > 0)
         {
            ApplicationDomain.currentDomain.domainMemory = _loc4_;
            _loc5_ = 0;
            _loc6_ = _loc4_.bytesAvailable;
            while(_loc5_ < _loc6_)
            {
               if(li8(_loc5_) > 0)
               {
                  _loc7_ = li8(_loc5_ + 1);
                  _loc8_ = li8(_loc5_ + 2);
                  _loc9_ = li8(_loc5_ + 3);
                  if(_loc7_ == _loc8_ && _loc8_ == _loc9_)
                  {
                     _loc10_ = int(param2[_loc7_]);
                     si8(_loc10_ >> 16,_loc5_ + 1);
                     si8(_loc10_ >> 8,_loc5_ + 2);
                     si8(_loc10_,_loc5_ + 3);
                  }
               }
               _loc5_ = _loc5_ + 4;
            }
            param1.setPixels(_loc3_,_loc4_);
         }
      }
      
      public static function pickColor(param1:BitmapData, param2:Rectangle) : Object
      {
         var _loc3_:ByteArray = param1.getPixels(param2);
         var _loc4_:* = {
            "a":0,
            "Ec":0,
            "u":0,
            "b":0
         };
         var _loc5_:int = 0;
         var _loc6_:int = _loc3_.length;
         while(_loc5_ < _loc6_)
         {
            _loc4_.a = Number(Number(_loc4_.a) + int(_loc3_[_loc5_++]));
            _loc4_.Ec = Number(Number(_loc4_.Ec) + int(_loc3_[_loc5_++]));
            _loc4_.u = Number(Number(_loc4_.u) + int(_loc3_[_loc5_++]));
            _loc4_.b = Number(Number(_loc4_.b) + int(_loc3_[_loc5_++]));
         }
         var _loc7_:Number = param2.width * param2.height;
         var _loc8_:* = {
            "a":int(_loc4_.a / _loc7_),
            "Ec":int(_loc4_.Ec / _loc7_),
            "u":int(_loc4_.u / _loc7_),
            "b":int(_loc4_.b / _loc7_)
         };
         return {
            "a":int(_loc4_.a / _loc7_),
            "Ec":int(_loc4_.Ec / _loc7_),
            "u":int(_loc4_.u / _loc7_),
            "b":int(_loc4_.b / _loc7_)
         };
      }
      
      public static function drawPalette(param1:Graphics, param2:Object = undefined, param3:Object = undefined, param4:Array = undefined, param5:Array = undefined) : void
      {
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:* = null;
         if(param2 == null)
         {
            param2 = 32;
         }
         if(param3 == null)
         {
            param3 = 32;
         }
         if(param4 != null)
         {
            _loc6_ = 0;
            _loc7_ = int(param4.length);
            while(_loc6_ < _loc7_)
            {
               _loc8_ = _loc6_++;
               param1.beginFill(int(param4[_loc8_]),1);
               param1.drawRect(_loc8_ * param2,0,param2,param3);
               param1.endFill();
            }
         }
         else
         {
            _loc6_ = 0;
            _loc7_ = int(param5.length);
            while(_loc6_ < _loc7_)
            {
               _loc8_ = _loc6_++;
               _loc9_ = param5[_loc8_];
               param1.beginFill(int(_loc9_.Ec) << 16 | int(_loc9_.u) << 8 | int(_loc9_.b),1);
               param1.drawRect(_loc8_ * param2,0,param2,param3);
               param1.endFill();
            }
         }
      }
   }
}
