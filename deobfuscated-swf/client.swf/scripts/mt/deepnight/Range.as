package mt.deepnight
{
   import flash.Boot;
   
   public class Range
   {
       
      
      public var min:int;
      
      public var max:int;
      
      public var inclusive:Boolean;
      
      public function Range(param1:int = 0, param2:int = 0, param3:Boolean = false)
      {
         if(Boot.skip_constructor)
         {
            return;
         }
         if(param1 >= param2)
         {
            §J#Z4§(param1);
            §&&*'\x02§(param2);
         }
         else
         {
            §J#Z4§(param2);
            §&&*'\x02§(param1);
         }
         inclusive = param3;
      }
      
      public static function §\bE\x0e\x07\x02§(param1:int, param2:int) : Range
      {
         return new Range(param1,param2,true);
      }
      
      public static function §]\b\x14B\x02§(param1:int, param2:int) : Range
      {
         return new Range(param1,param2,false);
      }
      
      public static function single(param1:int) : Range
      {
         return new Range(param1,param1,true);
      }
      
      public function toString() : String
      {
         return "X" + min + "=>" + max + (!!inclusive ? "]" : "X");
      }
      
      public function §&&*'\x02§(param1:int) : int
      {
         min = param1;
         if(min > max)
         {
            Boot.lastError = new Error();
            throw "invalid range " + Std.string(this);
         }
         return min;
      }
      
      public function §J#Z4§(param1:int) : int
      {
         max = param1;
         if(max < min)
         {
            Boot.lastError = new Error();
            throw "invalid range " + Std.string(this);
         }
         return max;
      }
      
      public function §\x1fT,\x02\x03§(param1:int) : int
      {
         return int(§&&*'\x02§(param1));
      }
      
      public function §\x0e\x02N\x1a\x03§(param1:int) : int
      {
         return int(§J#Z4§(param1));
      }
      
      public function iter() : IntIter
      {
         return !!inclusive ? new IntIter(min,max + 1) : new IntIter(min,max);
      }
      
      public function isOut(param1:int) : Boolean
      {
         return !(param1 >= min && (!!inclusive && param1 <= max || !inclusive && param1 < max));
      }
      
      public function §\x05\x07=\x01§(param1:int) : Boolean
      {
         return param1 >= min && (!!inclusive && param1 <= max || !inclusive && param1 < max);
      }
      
      public function §F;Wi\x03§() : int
      {
         return max - min + (!!inclusive ? 1 : 0);
      }
      
      public function draw(param1:Object = undefined) : int
      {
         if(param1 == null)
         {
            param1 = Std.random;
         }
         if(min == max)
         {
            return min;
         }
         if(inclusive)
         {
            return min + int(param1(max - min + 1));
         }
         return min + int(param1(max - min));
      }
      
      public function clone() : Range
      {
         return new Range(min,max,inclusive);
      }
   }
}
