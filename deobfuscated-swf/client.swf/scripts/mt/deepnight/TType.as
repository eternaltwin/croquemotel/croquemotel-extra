package mt.deepnight
{
   import flash.Boot;
   
   public final class TType
   {
      
      public static const __isenum:Boolean = true;
      
      public static var __constructs__ = ["TLinear","TLoop","TLoopEaseIn","TLoopEaseOut","TEase","TEaseIn","TEaseOut","TBurn","TBurnIn","TBurnOut","TZigZag","TRand","TShake","TShakeBoth","TJump","TElasticEnd"];
      
      public static var TZigZag:TType;
      
      public static var TShakeBoth:TType;
      
      public static var TShake:TType;
      
      public static var TRand:TType;
      
      public static var TLoopEaseOut:TType;
      
      public static var TLoopEaseIn:TType;
      
      public static var TLoop:TType;
      
      public static var TLinear:TType;
      
      public static var TJump:TType;
      
      public static var TElasticEnd:TType;
      
      public static var TEaseOut:TType;
      
      public static var TEaseIn:TType;
      
      public static var TEase:TType;
      
      public static var TBurnOut:TType;
      
      public static var TBurnIn:TType;
      
      public static var TBurn:TType;
       
      
      public var tag:String;
      
      public var index:int;
      
      public var params:Array;
      
      public const __enum__:Boolean = true;
      
      public function TType(param1:String, param2:int, param3:*)
      {
         tag = param1;
         index = param2;
         params = param3;
      }
      
      public final function toString() : String
      {
         return Boot.enum_to_string(this);
      }
   }
}
