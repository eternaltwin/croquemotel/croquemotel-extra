package mt.deepnight
{
   import flash.Boot;
   import haxe.Log;
   
   public class Tweenie
   {
      
      public static var init__:Boolean;
      
      public static var DEFAULT_DURATION:Number;
       
      
      public var tlist:List;
      
      public var errorHandler:Function;
      
      public var baseFPS:int;
      
      public function Tweenie(param1:Object = undefined)
      {
         if(param1 == null)
         {
            param1 = 30;
         }
         if(Boot.skip_constructor)
         {
            return;
         }
         baseFPS = param1;
         tlist = new List();
         errorHandler = onError;
      }
      
      public static function fastPow2(param1:Number) : Number
      {
         return param1 * param1;
      }
      
      public static function fastPow3(param1:Number) : Number
      {
         return param1 * param1 * param1;
      }
      
      public static function bezier(param1:Number, param2:Number, param3:Number, param4:Number, param5:Number) : Number
      {
         var _loc6_:Number = 1 - param1;
         _loc6_ = 1 - param1;
         return Number(Number(Number(_loc6_ * _loc6_ * _loc6_ * param2 + 3 * param1 * (_loc6_ * _loc6_) * param3) + 3 * (param1 * param1) * (1 - param1) * param4) + param1 * param1 * param1 * param5);
      }
      
      public function update(param1:Object = undefined) : void
      {
         var _loc3_:* = null;
         var _loc4_:Number = NaN;
         var _loc5_:Number = NaN;
         var _loc6_:Number = NaN;
         if(param1 == null)
         {
            param1 = 1;
         }
         var _loc2_:* = tlist.iterator();
         while(_loc2_.§\n\x1cT[\x02§())
         {
            _loc3_ = _loc2_.next();
            _loc4_ = _loc3_.to - _loc3_.from;
            if(_loc3_.type == TType.TRand)
            {
               _loc3_.ln = Number(Number(_loc3_.ln) + (int(Std.random(100)) < 33 ? _loc3_.speed * param1 : Number(0)));
            }
            else
            {
               _loc3_.ln = Number(Number(_loc3_.ln) + _loc3_.speed * param1);
            }
            _loc3_.n = Number(_loc3_.interpolate(Number(_loc3_.ln)));
            if(Number(_loc3_.ln) < 1)
            {
               if(_loc3_.type != TType.TShake && _loc3_.type != TType.TShakeBoth)
               {
                  §§push(Number(Number(_loc3_.from) + _loc3_.n * _loc4_));
               }
               else if(_loc3_.type == TType.TShake)
               {
                  _loc6_ = _loc3_.n * _loc4_;
                  §§push(Number(Number(_loc3_.from) + Math.random() * (_loc6_ < 0 ? -_loc6_ : _loc6_) * (_loc4_ > 0 ? 1 : -1)));
               }
               else
               {
                  §§push(Number(Number(_loc3_.from) + Math.random() * _loc3_.n * _loc4_ * (int(Std.random(2)) * 2 - 1)));
               }
               _loc5_ = §§pop();
               if(_loc3_.fl_pixel)
               {
                  _loc5_ = int(Math.round(_loc5_));
               }
               Reflect.setProperty(_loc3_.parent,_loc3_.§oV\x13'\x03§,_loc5_);
               if(_loc3_.onUpdate != null)
               {
                  _loc3_.onUpdate();
               }
               if(_loc3_.onUpdateT != null)
               {
                  _loc3_.onUpdateT(Number(_loc3_.ln));
               }
            }
            else
            {
               terminateTween(_loc3_,true);
            }
         }
      }
      
      public function terminateTween(param1:Object, param2:Object = undefined) : void
      {
         var _loc4_:* = null;
         var _loc5_:Number = NaN;
         if(param2 == null)
         {
            param2 = false;
         }
         var _loc3_:Number = Number(Number(param1.from) + (param1.to - param1.from) * param1.interpolate(1));
         if(param1.fl_pixel)
         {
            _loc3_ = int(Math.round(_loc3_));
         }
         Reflect.setProperty(param1.parent,param1.§oV\x13'\x03§,_loc3_);
         if(param1.onUpdate != null)
         {
            param1.onUpdate();
         }
         if(param1.onUpdateT != null)
         {
            param1.onUpdateT(1);
         }
         if(param1.onEnd != null)
         {
            _loc4_ = param1.onEnd;
            param1.onEnd = null;
            _loc4_();
         }
         if(!!param2 && (int(param1.plays) == -1 || int(param1.plays) > 1))
         {
            if(int(param1.plays) != -1)
            {
               param1.plays = int(param1.plays) - 1;
            }
            param1.n = param1.ln = Number(0);
         }
         else
         {
            tlist.remove(param1);
         }
      }
      
      public function terminateAll() : void
      {
         var _loc2_:* = null;
         var _loc3_:Number = NaN;
         var _loc4_:Number = NaN;
         var _loc5_:Number = NaN;
         var _loc1_:* = tlist.iterator();
         while(_loc1_.§\n\x1cT[\x02§())
         {
            _loc2_ = _loc1_.next();
            _loc2_.ln = 1;
         }
         _loc1_ = tlist.iterator();
         while(_loc1_.§\n\x1cT[\x02§())
         {
            _loc2_ = _loc1_.next();
            _loc3_ = _loc2_.to - _loc2_.from;
            if(_loc2_.type == TType.TRand)
            {
               _loc2_.ln = Number(Number(_loc2_.ln) + (int(Std.random(100)) < 33 ? Number(_loc2_.speed) : Number(0)));
            }
            else
            {
               _loc2_.ln = Number(Number(_loc2_.ln) + Number(_loc2_.speed));
            }
            _loc2_.n = Number(_loc2_.interpolate(Number(_loc2_.ln)));
            if(Number(_loc2_.ln) < 1)
            {
               if(_loc2_.type != TType.TShake && _loc2_.type != TType.TShakeBoth)
               {
                  §§push(Number(Number(_loc2_.from) + _loc2_.n * _loc3_));
               }
               else if(_loc2_.type == TType.TShake)
               {
                  _loc5_ = _loc2_.n * _loc3_;
                  §§push(Number(Number(_loc2_.from) + Math.random() * (_loc5_ < 0 ? -_loc5_ : _loc5_) * (_loc3_ > 0 ? 1 : -1)));
               }
               else
               {
                  §§push(Number(Number(_loc2_.from) + Math.random() * _loc2_.n * _loc3_ * (int(Std.random(2)) * 2 - 1)));
               }
               _loc4_ = §§pop();
               if(_loc2_.fl_pixel)
               {
                  _loc4_ = int(Math.round(_loc4_));
               }
               Reflect.setProperty(_loc2_.parent,_loc2_.§oV\x13'\x03§,_loc4_);
               if(_loc2_.onUpdate != null)
               {
                  _loc2_.onUpdate();
               }
               if(_loc2_.onUpdateT != null)
               {
                  _loc2_.onUpdateT(Number(_loc2_.ln));
               }
            }
            else
            {
               terminateTween(_loc2_,true);
            }
         }
      }
      
      public function terminate(param1:*, param2:String = undefined) : void
      {
         var _loc4_:* = null;
         var _loc3_:* = tlist.iterator();
         while(_loc3_.§\n\x1cT[\x02§())
         {
            _loc4_ = _loc3_.next();
            if(_loc4_.parent == param1 && (param2 == null || param2 == _loc4_.§oV\x13'\x03§))
            {
               terminateTween(_loc4_);
            }
         }
      }
      
      public function setErrorHandler(param1:Function) : void
      {
         errorHandler = param1;
      }
      
      public function onUpdate(param1:Object, param2:Number) : void
      {
         if(param1.onUpdate != null)
         {
            param1.onUpdate();
         }
         if(param1.onUpdateT != null)
         {
            param1.onUpdateT(param2);
         }
      }
      
      public function onError(param1:String) : void
      {
         Log.trace(param1,{
            "fileName":"Tweenie.hx",
            "lineNumber":56,
            "className":"mt.deepnight.Tweenie",
            "methodName":"onError"
         });
      }
      
      public function onEnd(param1:Object) : void
      {
         var _loc2_:* = null;
         if(param1.onEnd != null)
         {
            _loc2_ = param1.onEnd;
            param1.onEnd = null;
            _loc2_();
         }
      }
      
      public function §t\x1cf\x03§(param1:*, param2:String = undefined) : void
      {
         var _loc4_:* = null;
         var _loc3_:* = tlist.iterator();
         while(_loc3_.§\n\x1cT[\x02§())
         {
            _loc4_ = _loc3_.next();
            if(_loc4_.parent == param1 && (param2 == null || param2 == _loc4_.§oV\x13'\x03§))
            {
               tlist.remove(_loc4_);
            }
         }
      }
      
      public function getInterpolateFunction(param1:TType) : Function
      {
         switch(int(param1.index))
         {
            case 0:
               break;
            case 1:
               break;
            case 2:
               break;
            case 3:
               break;
            case 4:
               break;
            case 5:
               break;
            case 6:
               break;
            case 7:
               break;
            case 8:
               break;
            case 9:
               break;
            case 10:
               break;
            case 11:
               break;
            case 12:
               break;
            case 13:
               break;
            case 14:
               break;
            case 15:
         }
         return null;
      }
      
      public function §=Vr§(param1:*, param2:String) : Boolean
      {
         var _loc4_:* = null;
         var _loc3_:* = tlist.iterator();
         while(_loc3_.§\n\x1cT[\x02§())
         {
            _loc4_ = _loc3_.next();
            if(_loc4_.parent == param1 && _loc4_.§oV\x13'\x03§ == param2)
            {
               return true;
            }
         }
         return false;
      }
      
      public function §delete§(param1:*) : void
      {
         var _loc3_:* = null;
         var _loc2_:* = tlist.iterator();
         while(_loc2_.§\n\x1cT[\x02§())
         {
            _loc3_ = _loc2_.next();
            if(_loc3_.parent == param1)
            {
               tlist.remove(_loc3_);
            }
         }
      }
      
      public function create_(param1:*, param2:String, param3:Number, param4:TType = undefined, param5:Object = undefined) : Object
      {
         var _loc8_:* = null;
         if(param5 == null)
         {
            param5 = Tweenie.DEFAULT_DURATION;
         }
         if(param1 == null)
         {
            errorHandler("tween creation failed : null parent, v=" + param2 + " tp=" + Std.string(param4));
         }
         if(param4 == null)
         {
            param4 = TType.TEase;
         }
         var _loc6_:TType = null;
         var _loc7_:* = tlist.iterator();
         while(_loc7_.§\n\x1cT[\x02§())
         {
            _loc8_ = _loc7_.next();
            if(_loc8_.parent == param1 && _loc8_.§oV\x13'\x03§ == param2)
            {
               _loc6_ = _loc8_.type;
               tlist.remove(_loc8_);
            }
         }
         if(_loc6_ != null)
         {
            if(param4 == TType.TEase && (_loc6_ == TType.TEase || _loc6_ == TType.TEaseOut))
            {
               param4 = TType.TEaseOut;
            }
         }
         _loc7_ = {
            "parent":param1,
            "oV\x13\'\x03":param2,
            "n":0,
            "ln":0,
            "speed":1 / (param5 * baseFPS / 1000),
            "from":Number(Reflect.§2|p\x1f\x02§(param1,param2)),
            "to":param3,
            "type":param4,
            "fl_pixel":false,
            "plays":1,
            "onUpdate":null,
            "onUpdateT":null,
            "onEnd":null,
            "interpolate":getInterpolateFunction(param4)
         };
         if(Number(_loc7_.from) == Number(_loc7_.to))
         {
            _loc7_.ln = 1;
         }
         tlist.add(_loc7_);
         return _loc7_;
      }
      
      public function create(param1:*, param2:String, param3:Number, param4:TType = undefined, param5:Object = undefined) : Object
      {
         return Boolean(Reflect.§wXJ\x01§(param1,param2)) ? create_(param1,param2,param3,param4,param5) : create_(param1,Boot.__unprotect__(param2),param3,param4,param5);
      }
      
      public function count() : int
      {
         return tlist.length;
      }
   }
}
