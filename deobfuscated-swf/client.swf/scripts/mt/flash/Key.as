package mt.flash
{
   import flash.Lib;
   import flash.display.Stage;
   import flash.events.Event;
   import flash.events.KeyboardEvent;
   import flash.external.ExternalInterface;
   
   public class Key
   {
      
      public static var init__:Boolean;
      
      public static var fl_initDone:Boolean = false;
      
      public static var §bvH\x02§:Array;
      
      public static var ktime:int = 0;
       
      
      public function Key()
      {
      }
      
      public static function init() : void
      {
         var _loc1_:Stage = Lib.current.stage;
         if(Key.fl_initDone)
         {
            _loc1_.removeEventListener(Event.ENTER_FRAME,Key.onEnterFrame);
            _loc1_.addEventListener(Event.ENTER_FRAME,Key.onEnterFrame);
            return;
         }
         Key.fl_initDone = true;
         _loc1_.addEventListener(KeyboardEvent.KEY_DOWN,function(param1:KeyboardEvent):void
         {
            return Key.onKey(true,param1);
         });
         _loc1_.addEventListener(KeyboardEvent.KEY_UP,function(param1:KeyboardEvent):void
         {
            return Key.onKey(false,param1);
         });
         _loc1_.addEventListener(Event.DEACTIVATE,function(param1:*):void
         {
            Key.§bvH\x02§ = [];
         });
         _loc1_.addEventListener(Event.ENTER_FRAME,Key.onEnterFrame);
      }
      
      public static function onEnterFrame(param1:*) : void
      {
         Key.ktime = Key.ktime + 1;
      }
      
      public static function onKey(param1:Boolean, param2:KeyboardEvent) : void
      {
         Key.event(param2.keyCode,param1);
      }
      
      public static function event(param1:int, param2:Boolean) : void
      {
         Key.§bvH\x02§[param1] = !!param2 ? Key.ktime : null;
         if(param1 == 18 && !param2)
         {
            Key.§bvH\x02§[17] = null;
         }
      }
      
      public static function isDown(param1:int) : Boolean
      {
         return Key.§bvH\x02§[param1] != null;
      }
      
      public static function isToggled(param1:int) : Boolean
      {
         return Key.§bvH\x02§[param1] == Key.ktime;
      }
      
      public static function §'\nZu§(param1:String) : Object
      {
         var _loc3_:* = null;
         try
         {
            ExternalInterface.addCallback("onKeyEvent",Key.event);
            _loc3_ = ExternalInterface.call("function() { var fla = window.document[\'" + param1 + "\']; if( fla == null ) return false; document.onkeydown = function(e) { if( e == null ) e = window.event; fla.onKeyEvent(e.keyCode,true); }; document.onkeyup = function(e) { if( e == null ) e = window.event; fla.onKeyEvent(e.keyCode,false); }; return true; }");
            if(_loc3_ == null)
            {
               _loc3_ = false;
            }
            return _loc3_;
         }
         catch(_loc_e_:*)
         {
         }
      }
   }
}
