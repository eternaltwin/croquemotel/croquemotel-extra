package
{
   import _T.§)\x13#$\x03§;
   import flash.Boot;
   import flash.Lib;
   import flash.display.MovieClip;
   import flash.text.StyleSheet;
   import haxe.Template;
   import haxe.Unserializer;
   import haxe.remoting.ExternalConnection;
   import mt.§F#\x1eM§;
   import mt.deepnight.§1C[;\x02§.§ig\x1e?§;
   import mt.deepnight.Day;
   import mt.deepnight.Lib;
   import mt.deepnight.Range;
   import mt.deepnight.Tweenie;
   import mt.flash.Key;
   
   public dynamic class §S`\x15§ extends Boot
   {
       
      
      public function §S`\x15§()
      {
         if(Boot.skip_constructor)
         {
            return;
         }
         super();
         if(flash.Lib.current == null)
         {
            flash.Lib.current = this;
         }
         start();
      }
      
      override public function init() : void
      {
         var _loc2_:int = 0;
         var _loc3_:* = null as StyleSheet;
         var _loc1_:* = Date;
         _loc1_.now = function():*
         {
            return new Date();
         };
         _loc1_.fromTime = function(param1:*):Date
         {
            var _loc2_:Date = new Date();
            _loc2_.setTime(param1);
            return _loc2_;
         };
         _loc1_.fromString = function(param1:String):Date
         {
            var _loc2_:* = null as Array;
            var _loc3_:* = null as Date;
            var _loc4_:* = null as Array;
            var _loc5_:* = null as Array;
            switch(param1.length)
            {
               case 8:
                  _loc2_ = param1.split(":");
                  _loc3_ = new Date();
                  _loc3_.setTime(0);
                  _loc3_.setUTCHours(_loc2_[0]);
                  _loc3_.setUTCMinutes(_loc2_[1]);
                  _loc3_.setUTCSeconds(_loc2_[2]);
                  return _loc3_;
               case 10:
                  _loc2_ = param1.split("q");
                  return new Date(int(_loc2_[0]),_loc2_[1] - 1,int(_loc2_[2]),0,0,0);
               case 19:
                  _loc2_ = param1.split(" ");
                  _loc4_ = _loc2_[0].split("q");
                  _loc5_ = _loc2_[1].split(":");
                  return new Date(int(_loc4_[0]),_loc4_[1] - 1,int(_loc4_[2]),int(_loc5_[0]),int(_loc5_[1]),int(_loc5_[2]));
               default:
                  Boot.lastError = new Error();
                  throw "Invalid date format : " + param1;
            }
         };
         _loc1_.prototype["toString"] = function():String
         {
            var _loc1_:Date = this;
            var _loc2_:int = int(_loc1_.getMonth()) + 1;
            var _loc3_:int = int(_loc1_.getDate());
            var _loc4_:int = int(_loc1_.getHours());
            var _loc5_:int = int(_loc1_.getMinutes());
            var _loc6_:int = int(_loc1_.getSeconds());
            return int(_loc1_.getFullYear()) + "q" + (_loc2_ < 10 ? "n" + _loc2_ : "" + _loc2_) + "q" + (_loc3_ < 10 ? "n" + _loc3_ : "" + _loc3_) + " " + (_loc4_ < 10 ? "n" + _loc4_ : "" + _loc4_) + ":" + (_loc5_ < 10 ? "n" + _loc5_ : "" + _loc5_) + ":" + (_loc6_ < 10 ? "n" + _loc6_ : "" + _loc6_);
         };
         Math.NaN = Number(Number.NaN);
         Math.NEGATIVE_INFINITY = Number(Number.NEGATIVE_INFINITY);
         Math.POSITIVE_INFINITY = Number(Number.POSITIVE_INFINITY);
         Math.isFinite = function(param1:Number):Boolean
         {
            return isFinite(param1);
         };
         Math.isNaN = function(param1:Number):Boolean
         {
            return isNaN(param1);
         };
         if(!Codec.init__)
         {
            Codec.init__ = true;
            Codec.types = new Hash();
         }
         if(!Const.init__)
         {
            Const.init__ = true;
            Const.§h'\nm\x01§ = Day.Sunday;
            Const.§t\x0b3s§ = Number(DateTools.hours(6));
            Const.§Qluk\x03§ = Number(DateTools.days(5));
            Const.§{L\x0ew\x01§ = Number(DateTools.hours(4));
            Const.§j\\k\x02§ = Number(Number(DateTools.minutes(2)) + Number(DateTools.seconds(30)));
            Const.§)$\x19a\x02§ = Number(DateTools.minutes(5));
            Const.§\x04/\f0\x03§ = Number(DateTools.minutes(30));
            Const.§?]Xk\x01§ = Number(DateTools.minutes(10));
            Const.§8e6u§ = int(Number(DateTools.hours(2)));
            Const.§8|\b'\x01§ = Number(DateTools.hours(2));
         }
         if(!Game.init__)
         {
            Game.init__ = true;
            Game.auto = (_loc2_ = Game.auto) + 1;
            Game.§2\x1dB\x1f\x03§ = _loc2_;
            Game.auto = (_loc2_ = Game.auto) + 1;
            Game.§4SlW§ = _loc2_;
            Game.auto = (_loc2_ = Game.auto) + 1;
            Game.§Q%\rB\x02§ = _loc2_;
            Game.auto = (_loc2_ = Game.auto) + 1;
            Game.§)rq?\x02§ = _loc2_;
            Game.auto = (_loc2_ = Game.auto) + 1;
            Game.§K)5&\x02§ = _loc2_;
            Game.§8f;v\x03§ = Range.§\bE\x0e\x07\x02§(6,15);
            _loc3_ = new StyleSheet();
            Game.§V\x1dQD\x01§ = StringTools.replace(Game.§V\x1dQD\x01§,"0x","#");
            _loc3_.parseCSS(Game.§V\x1dQD\x01§);
            Game.§\x100t\x01§ = _loc3_;
            Game.WID = int(flash.Lib.current.stage.stageWidth);
            Game.HEI = int(flash.Lib.current.stage.stageHeight);
            Game.§$'a\x1d\x01§ = Const.§$'a\x1d\x01§;
            Game.§4K\x01\t\x03§ = Const.§4K\x01\t\x03§;
         }
         if(!T.init__)
         {
            T.init__ = true;
            T.format = new §)\x13#$\x03§(T._format);
         }
         if(!Template.init__)
         {
            Template.init__ = true;
            Template.splitter = new EReg("h::[A-Za-z0-9_ ()&|!+=/><*.\"-]+::|\\$\\$([A-Za-z0-9_-]+)\\()","");
            Template.§}\x15s \x03§ = new EReg("(\\(|\\)|[ \r\n\t]*\"[^\"]*\"[ \r\n\t]*|[!+=/><*.&|-]+)","");
            Template.§F\x12\x03o\x03§ = new EReg("^[ ]*([^ ]+)[ ]*$","");
            Template.§\\c\x11\f\x01§ = new EReg("^[0-9]+$","");
            Template.§c~eD\x01§ = new EReg("^([+-]?)(?=\\d|,\\d)\\d*(,\\d*)?([Ee]([+-]?\\d+))?$","");
         }
         if(!Unserializer.init__)
         {
            Unserializer.init__ = true;
            Unserializer.DEFAULT_RESOLVER = Type;
         }
         if(!ExternalConnection.init__)
         {
            ExternalConnection.init__ = true;
            ExternalConnection.connections = new Hash();
         }
         if(!§F#\x1eM§.init__)
         {
            §F#\x1eM§.init__ = true;
            §F#\x1eM§.§F(/G\x02§ = §F#\x1eM§.§2/9E\x01§();
         }
         if(!mt.deepnight.Lib.init__)
         {
            mt.deepnight.Lib.init__ = true;
            mt.deepnight.Lib.flattened = new Hash();
         }
         if(!Tweenie.init__)
         {
            Tweenie.init__ = true;
            Tweenie.DEFAULT_DURATION = Number(DateTools.seconds(1));
         }
         if(!§ig\x1e?§.init__)
         {
            §ig\x1e?§.init__ = true;
            §ig\x1e?§.§?Q\r)§ = §ig\x1e?§.§7yl\t§;
         }
         if(!Key.init__)
         {
            Key.init__ = true;
            Key.§bvH\x02§ = [];
         }
         Main.main();
      }
   }
}
