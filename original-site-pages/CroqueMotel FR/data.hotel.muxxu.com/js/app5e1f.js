var $hxClasses = $hxClasses || {},$estr = function() { return js.Boot.__string_rec(this,''); };
function $extend(from, fields) {
	function inherit() {}; inherit.prototype = from; var proto = new inherit();
	for (var name in fields) proto[name] = fields[name];
	return proto;
}
var EReg = $hxClasses["EReg"] = function(r,opt) {
	opt = opt.split("u").join("");
	this.r = new RegExp(r,opt);
};
EReg.__name__ = ["EReg"];
EReg.prototype = {
	customReplace: function(s,f) {
		var buf = new StringBuf();
		while(true) {
			if(!this.match(s)) break;
			buf.b += Std.string(this.matchedLeft());
			buf.b += Std.string(f(this));
			s = this.matchedRight();
		}
		buf.b += Std.string(s);
		return buf.b;
	}
	,replace: function(s,by) {
		return s.replace(this.r,by);
	}
	,split: function(s) {
		var d = "#__delim__#";
		return s.replace(this.r,d).split(d);
	}
	,matchedPos: function() {
		if(this.r.m == null) throw "No string matched";
		return { pos : this.r.m.index, len : this.r.m[0].length};
	}
	,matchedRight: function() {
		if(this.r.m == null) throw "No string matched";
		var sz = this.r.m.index + this.r.m[0].length;
		return this.r.s.substr(sz,this.r.s.length - sz);
	}
	,matchedLeft: function() {
		if(this.r.m == null) throw "No string matched";
		return this.r.s.substr(0,this.r.m.index);
	}
	,matched: function(n) {
		return this.r.m != null && n >= 0 && n < this.r.m.length?this.r.m[n]:(function($this) {
			var $r;
			throw "EReg::matched";
			return $r;
		}(this));
	}
	,match: function(s) {
		if(this.r.global) this.r.lastIndex = 0;
		this.r.m = this.r.exec(s);
		this.r.s = s;
		return this.r.m != null;
	}
	,r: null
	,__class__: EReg
}
var Hash = $hxClasses["Hash"] = function() {
	this.h = { };
};
Hash.__name__ = ["Hash"];
Hash.prototype = {
	toString: function() {
		var s = new StringBuf();
		s.b += "{";
		var it = this.keys();
		while( it.hasNext() ) {
			var i = it.next();
			s.b += Std.string(i);
			s.b += " => ";
			s.b += Std.string(Std.string(this.get(i)));
			if(it.hasNext()) s.b += ", ";
		}
		s.b += "}";
		return s.b;
	}
	,iterator: function() {
		return { ref : this.h, it : this.keys(), hasNext : function() {
			return this.it.hasNext();
		}, next : function() {
			var i = this.it.next();
			return this.ref["$" + i];
		}};
	}
	,keys: function() {
		var a = [];
		for( var key in this.h ) {
		if(this.h.hasOwnProperty(key)) a.push(key.substr(1));
		}
		return HxOverrides.iter(a);
	}
	,remove: function(key) {
		key = "$" + key;
		if(!this.h.hasOwnProperty(key)) return false;
		delete(this.h[key]);
		return true;
	}
	,exists: function(key) {
		return this.h.hasOwnProperty("$" + key);
	}
	,get: function(key) {
		return this.h["$" + key];
	}
	,set: function(key,value) {
		this.h["$" + key] = value;
	}
	,h: null
	,__class__: Hash
}
var HxOverrides = $hxClasses["HxOverrides"] = function() { }
HxOverrides.__name__ = ["HxOverrides"];
HxOverrides.dateStr = function(date) {
	var m = date.getMonth() + 1;
	var d = date.getDate();
	var h = date.getHours();
	var mi = date.getMinutes();
	var s = date.getSeconds();
	return date.getFullYear() + "-" + (m < 10?"0" + m:"" + m) + "-" + (d < 10?"0" + d:"" + d) + " " + (h < 10?"0" + h:"" + h) + ":" + (mi < 10?"0" + mi:"" + mi) + ":" + (s < 10?"0" + s:"" + s);
}
HxOverrides.strDate = function(s) {
	switch(s.length) {
	case 8:
		var k = s.split(":");
		var d = new Date();
		d.setTime(0);
		d.setUTCHours(k[0]);
		d.setUTCMinutes(k[1]);
		d.setUTCSeconds(k[2]);
		return d;
	case 10:
		var k = s.split("-");
		return new Date(k[0],k[1] - 1,k[2],0,0,0);
	case 19:
		var k = s.split(" ");
		var y = k[0].split("-");
		var t = k[1].split(":");
		return new Date(y[0],y[1] - 1,y[2],t[0],t[1],t[2]);
	default:
		throw "Invalid date format : " + s;
	}
}
HxOverrides.cca = function(s,index) {
	var x = s.cca(index);
	if(x != x) return undefined;
	return x;
}
HxOverrides.substr = function(s,pos,len) {
	if(pos != null && pos != 0 && len != null && len < 0) return "";
	if(len == null) len = s.length;
	if(pos < 0) {
		pos = s.length + pos;
		if(pos < 0) pos = 0;
	} else if(len < 0) len = s.length + len - pos;
	return s.substr(pos,len);
}
HxOverrides.remove = function(a,obj) {
	var i = 0;
	var l = a.length;
	while(i < l) {
		if(a[i] == obj) {
			a.splice(i,1);
			return true;
		}
		i++;
	}
	return false;
}
HxOverrides.iter = function(a) {
	return { cur : 0, arr : a, hasNext : function() {
		return this.cur < this.arr.length;
	}, next : function() {
		return this.arr[this.cur++];
	}};
}
var IntHash = $hxClasses["IntHash"] = function() {
	this.h = { };
};
IntHash.__name__ = ["IntHash"];
IntHash.prototype = {
	toString: function() {
		var s = new StringBuf();
		s.b += "{";
		var it = this.keys();
		while( it.hasNext() ) {
			var i = it.next();
			s.b += Std.string(i);
			s.b += " => ";
			s.b += Std.string(Std.string(this.get(i)));
			if(it.hasNext()) s.b += ", ";
		}
		s.b += "}";
		return s.b;
	}
	,iterator: function() {
		return { ref : this.h, it : this.keys(), hasNext : function() {
			return this.it.hasNext();
		}, next : function() {
			var i = this.it.next();
			return this.ref[i];
		}};
	}
	,keys: function() {
		var a = [];
		for( var key in this.h ) {
		if(this.h.hasOwnProperty(key)) a.push(key | 0);
		}
		return HxOverrides.iter(a);
	}
	,remove: function(key) {
		if(!this.h.hasOwnProperty(key)) return false;
		delete(this.h[key]);
		return true;
	}
	,exists: function(key) {
		return this.h.hasOwnProperty(key);
	}
	,get: function(key) {
		return this.h[key];
	}
	,set: function(key,value) {
		this.h[key] = value;
	}
	,h: null
	,__class__: IntHash
}
var IntIter = $hxClasses["IntIter"] = function(min,max) {
	this.min = min;
	this.max = max;
};
IntIter.__name__ = ["IntIter"];
IntIter.prototype = {
	next: function() {
		return this.min++;
	}
	,hasNext: function() {
		return this.min < this.max;
	}
	,max: null
	,min: null
	,__class__: IntIter
}
var haxe = haxe || {}
if(!haxe.remoting) haxe.remoting = {}
haxe.remoting.Context = $hxClasses["haxe.remoting.Context"] = function() {
	this.objects = new Hash();
};
haxe.remoting.Context.__name__ = ["haxe","remoting","Context"];
haxe.remoting.Context.share = function(name,obj) {
	var ctx = new haxe.remoting.Context();
	ctx.addObject(name,obj);
	return ctx;
}
haxe.remoting.Context.prototype = {
	call: function(path,params) {
		if(path.length < 2) throw "Invalid path '" + path.join(".") + "'";
		var inf = this.objects.get(path[0]);
		if(inf == null) throw "No such object " + path[0];
		var o = inf.obj;
		var m = Reflect.field(o,path[1]);
		if(path.length > 2) {
			if(!inf.rec) throw "Can't access " + path.join(".");
			var _g1 = 2, _g = path.length;
			while(_g1 < _g) {
				var i = _g1++;
				o = m;
				m = Reflect.field(o,path[i]);
			}
		}
		if(!Reflect.isFunction(m)) throw "No such method " + path.join(".");
		return m.apply(o,params);
	}
	,addObject: function(name,obj,recursive) {
		this.objects.set(name,{ obj : obj, rec : recursive});
	}
	,objects: null
	,__class__: haxe.remoting.Context
}
var JsMain = $hxClasses["JsMain"] = function() { }
JsMain.__name__ = ["JsMain"];
JsMain.main = function() {
	JsMain.ctx.addObject("api",JsMain);
	JsMain.connect();
}
JsMain.initSideTip = function(h) {
	JsMain.SIDE_HEIGHT = h;
}
JsMain.connect = function() {
	JsMain.cnx = haxe.remoting.ExternalConnection.flashConnect("cnx","hotelClient",JsMain.ctx);
}
JsMain.print = function(className,html) {
	var dom = js.Lib.document.getElementById("sidePanelContent");
	if(dom != null) {
		dom.style.display = "block";
		if(className != "") {
			dom.innerHTML = "";
			var child = js.Lib.document.createElement("div");
			child.className = className;
			dom.appendChild(child);
			child.innerHTML = JsMain.parse(html);
		} else dom.innerHTML = JsMain.parse(html);
		dom.parentNode.style.marginTop = JsMain.SIDE_HEIGHT - dom.parentNode.clientHeight + "px";
	}
}
JsMain.clear = function() {
	var dom = js.Lib.document.getElementById("sidePanelContent");
	if(dom != null) {
		dom.innerHTML = "";
		dom.parentNode.style.marginTop = JsMain.SIDE_HEIGHT - dom.parentNode.clientHeight + "px";
		dom.style.display = "none";
	}
}
JsMain.toggleQuest = function() {
	var dom = js.Lib.document.getElementById("questDetails");
	if(dom != null) {
		if(dom.className.indexOf("collapse") >= 0) dom.className = StringTools.replace(dom.className,"collapse","expand"); else dom.className = StringTools.replace(dom.className,"expand","collapse");
	}
}
JsMain.inject = function(domId,html) {
	var dom = js.Lib.document.getElementById(domId);
	if(dom != null) dom.innerHTML = html;
}
JsMain.empty = function(domId) {
	var dom = js.Lib.document.getElementById(domId);
	if(dom != null) dom.innerHTML = "";
}
JsMain.injectAnim = function(domId,html) {
	var dom = js.Lib.document.getElementById(domId);
	if(dom != null) {
		js.fx.Style.setStyles(dom,{ opacity : 0, display : "block"});
		var anim = new js.fx.Morph(dom,{ opacity : 1});
		anim.fps = 20;
		anim.duration = 600;
		anim.start();
		dom.innerHTML = html;
	}
}
JsMain.isIE = function() {
	return js.Lib.window.navigator.userAgent.toLowerCase().indexOf("msie") >= 0;
}
JsMain.isFirefox = function() {
	return js.Lib.window.navigator.userAgent.toLowerCase().indexOf("firefox") >= 0;
}
JsMain.isLinux = function() {
	return js.Lib.window.navigator.userAgent.toLowerCase().indexOf("linux") >= 0;
}
JsMain.htmlPopUp = function(str) {
	var pop = js.Lib.document.getElementById("htmlPopUp");
	var content = js.Lib.document.getElementById("htmlPopUpContent");
	if(pop != null && content != null) {
		pop.style.display = "block";
		content.innerHTML = str;
		if(JsMain.isLinux()) js.Lib.document.getElementById("swf_hotelClient").style.visibility = "hidden";
	}
}
JsMain.closePopUp = function() {
	var pop = js.Lib.document.getElementById("htmlPopUp");
	if(pop != null) {
		pop.style.display = "none";
		JsMain.cnx.resolve("GameJsApi").resolve("_unlockGame").call([]);
		if(JsMain.isLinux()) js.Lib.document.getElementById("swf_hotelClient").style.visibility = "visible";
	}
}
JsMain.clientCall = function() {
	JsMain.cnx.resolve("GameJsApi").resolve("_clientCall").call([]);
}
JsMain.setLog = function(list) {
	var dom = js.Lib.document.getElementById("gameLog");
	if(dom != null) {
		if(list.length > 0) {
			var str = JsMain.parse("<p>" + list.join("</p><p>") + "</p>");
			dom.innerHTML = str;
		} else dom.innerHTML = "";
	}
}
JsMain.setMoney = function(n) {
	JsMain.animateCounter("money",n);
}
JsMain.setFame = function(n) {
	JsMain.animateCounter("fame",n);
}
JsMain.forceMoneyAnim = function(delta) {
	var jq = new js.JQuery("#moneyValue");
	var old = Std.parseInt(jq.html());
	jq.html(Std.string(old - delta));
	JsMain.animateCounter("money",old);
}
JsMain.animateCounter = function(name,n) {
	var val = new js.JQuery("#" + name + "Value");
	var delta = n - Std.parseInt(val.html());
	if(delta != 0) {
		var jq = new js.JQuery("#" + name + "Change");
		jq.stop();
		jq.show();
		jq.css("marginLeft","160px");
		jq.css("opacity","0");
		jq.html((delta > 0?"+":"") + Std.string(delta));
		jq.animate({ marginLeft : "95px", opacity : 1},400,function() {
			val.html(Std.string(n));
		});
		jq.fadeOut(2500);
	}
}
JsMain.parse = function(str) {
	str = str.split("}").join("{");
	var parts = str.split("{");
	str = "";
	var n = 0;
	var _g = 0;
	while(_g < parts.length) {
		var part = parts[_g];
		++_g;
		if(n % 2 == 0) str += part; else str += "&nbsp;<img src='/img/icons/" + part + ".png' alt='[" + part + "]'/>";
		n++;
	}
	return str;
}
JsMain.setQuest = function(title,txt) {
	var dom = js.Lib.document.getElementById("quest");
	var domTitle = js.Lib.document.getElementById("questTitle");
	var domText = js.Lib.document.getElementById("questText");
	if(dom != null && domTitle != null && domText != null) {
		if(title != null) {
			js.fx.Style.setStyles(dom,{ opacity : 0, display : "block"});
			var anim = new js.fx.Morph(dom,{ opacity : 1});
			anim.fps = 20;
			anim.duration = 600;
			anim.start();
			domTitle.innerHTML = title;
			domText.innerHTML = txt;
		} else js.fx.Style.setStyles(dom,{ display : "none"});
	}
}
JsMain.refresh = function() {
	mt.deepnight.deprecated.JsChaos.reloadPage();
}
JsMain.getTune = function(tune) {
	var s = "glandouillard de devellopeur :)";
	if(tune < 50) s = "Ce client ne semble pas trop fortuné...";
	if(tune > 50 && tune <= 100) s = "Ce client n'est vraisemblablement pas sans le sous ! On peut gratter un beau pourboire...";
	if(tune > 100) s = "Ce client a les poches bien pleines, il va falloir le faire banquer !";
	return s;
}
JsMain.getActivity = function(act) {
	switch(act) {
	case "_TR_SAUNA":
		return "Ce client se détend au sauna";
	case "_TR_BASE":
		return "Ce client se repose dans sa chambre";
	}
	return "glandouillard de devellopeur :)";
}
JsMain.onWheel = function(event) {
	try {
		JsMain.cnx.resolve("GameJsApi").resolve("_onWheel").call([mt.deepnight.deprecated.JsChaos.getWheelDelta(event)]);
	} catch( e ) {
	}
	mt.deepnight.deprecated.JsChaos.blockEvent(event);
	return false;
}
JsMain.onClick = function(event) {
	try {
		JsMain.cnx.resolve("GameJsApi").resolve("_onClickPage").call([]);
	} catch( e ) {
	}
	return true;
}
JsMain.onComment = function() {
	try {
		JsMain.cnx.resolve("GameJsApi").resolve("_onComment").call([]);
	} catch( e ) {
	}
}
JsMain.manageEvents = function(flashId) {
	var dom = js.Lib.document.getElementById(flashId);
	mt.deepnight.deprecated.JsChaos.addWheelEvent(dom,JsMain.onWheel);
	var dom1 = js.Lib.document.getElementById("mxtop");
	mt.deepnight.deprecated.JsChaos.addClickEvent(dom1,JsMain.onClick);
}
var List = $hxClasses["List"] = function() {
	this.length = 0;
};
List.__name__ = ["List"];
List.prototype = {
	map: function(f) {
		var b = new List();
		var l = this.h;
		while(l != null) {
			var v = l[0];
			l = l[1];
			b.add(f(v));
		}
		return b;
	}
	,filter: function(f) {
		var l2 = new List();
		var l = this.h;
		while(l != null) {
			var v = l[0];
			l = l[1];
			if(f(v)) l2.add(v);
		}
		return l2;
	}
	,join: function(sep) {
		var s = new StringBuf();
		var first = true;
		var l = this.h;
		while(l != null) {
			if(first) first = false; else s.b += Std.string(sep);
			s.b += Std.string(l[0]);
			l = l[1];
		}
		return s.b;
	}
	,toString: function() {
		var s = new StringBuf();
		var first = true;
		var l = this.h;
		s.b += "{";
		while(l != null) {
			if(first) first = false; else s.b += ", ";
			s.b += Std.string(Std.string(l[0]));
			l = l[1];
		}
		s.b += "}";
		return s.b;
	}
	,iterator: function() {
		return { h : this.h, hasNext : function() {
			return this.h != null;
		}, next : function() {
			if(this.h == null) return null;
			var x = this.h[0];
			this.h = this.h[1];
			return x;
		}};
	}
	,remove: function(v) {
		var prev = null;
		var l = this.h;
		while(l != null) {
			if(l[0] == v) {
				if(prev == null) this.h = l[1]; else prev[1] = l[1];
				if(this.q == l) this.q = prev;
				this.length--;
				return true;
			}
			prev = l;
			l = l[1];
		}
		return false;
	}
	,clear: function() {
		this.h = null;
		this.q = null;
		this.length = 0;
	}
	,isEmpty: function() {
		return this.h == null;
	}
	,pop: function() {
		if(this.h == null) return null;
		var x = this.h[0];
		this.h = this.h[1];
		if(this.h == null) this.q = null;
		this.length--;
		return x;
	}
	,last: function() {
		return this.q == null?null:this.q[0];
	}
	,first: function() {
		return this.h == null?null:this.h[0];
	}
	,push: function(item) {
		var x = [item,this.h];
		this.h = x;
		if(this.q == null) this.q = x;
		this.length++;
	}
	,add: function(item) {
		var x = [item];
		if(this.h == null) this.h = x; else this.q[1] = x;
		this.q = x;
		this.length++;
	}
	,length: null
	,q: null
	,h: null
	,__class__: List
}
var Reflect = $hxClasses["Reflect"] = function() { }
Reflect.__name__ = ["Reflect"];
Reflect.hasField = function(o,field) {
	return Object.prototype.hasOwnProperty.call(o,field);
}
Reflect.field = function(o,field) {
	var v = null;
	try {
		v = o[field];
	} catch( e ) {
	}
	return v;
}
Reflect.setField = function(o,field,value) {
	o[field] = value;
}
Reflect.getProperty = function(o,field) {
	var tmp;
	return o == null?null:o.__properties__ && (tmp = o.__properties__["get_" + field])?o[tmp]():o[field];
}
Reflect.setProperty = function(o,field,value) {
	var tmp;
	if(o.__properties__ && (tmp = o.__properties__["set_" + field])) o[tmp](value); else o[field] = value;
}
Reflect.callMethod = function(o,func,args) {
	return func.apply(o,args);
}
Reflect.fields = function(o) {
	var a = [];
	if(o != null) {
		var hasOwnProperty = Object.prototype.hasOwnProperty;
		for( var f in o ) {
		if(hasOwnProperty.call(o,f)) a.push(f);
		}
	}
	return a;
}
Reflect.isFunction = function(f) {
	return typeof(f) == "function" && !(f.__name__ || f.__ename__);
}
Reflect.compare = function(a,b) {
	return a == b?0:a > b?1:-1;
}
Reflect.compareMethods = function(f1,f2) {
	if(f1 == f2) return true;
	if(!Reflect.isFunction(f1) || !Reflect.isFunction(f2)) return false;
	return f1.scope == f2.scope && f1.method == f2.method && f1.method != null;
}
Reflect.isObject = function(v) {
	if(v == null) return false;
	var t = typeof(v);
	return t == "string" || t == "object" && !v.__enum__ || t == "function" && (v.__name__ || v.__ename__);
}
Reflect.deleteField = function(o,f) {
	if(!Reflect.hasField(o,f)) return false;
	delete(o[f]);
	return true;
}
Reflect.copy = function(o) {
	var o2 = { };
	var _g = 0, _g1 = Reflect.fields(o);
	while(_g < _g1.length) {
		var f = _g1[_g];
		++_g;
		o2[f] = Reflect.field(o,f);
	}
	return o2;
}
Reflect.makeVarArgs = function(f) {
	return function() {
		var a = Array.prototype.slice.call(arguments);
		return f(a);
	};
}
var Std = $hxClasses["Std"] = function() { }
Std.__name__ = ["Std"];
Std["is"] = function(v,t) {
	return js.Boot.__instanceof(v,t);
}
Std.string = function(s) {
	return js.Boot.__string_rec(s,"");
}
Std["int"] = function(x) {
	return x | 0;
}
Std.parseInt = function(x) {
	var v = parseInt(x,10);
	if(v == 0 && (HxOverrides.cca(x,1) == 120 || HxOverrides.cca(x,1) == 88)) v = parseInt(x);
	if(isNaN(v)) return null;
	return v;
}
Std.parseFloat = function(x) {
	return parseFloat(x);
}
Std.random = function(x) {
	return x <= 0?0:Math.floor(Math.random() * x);
}
var StringBuf = $hxClasses["StringBuf"] = function() {
	this.b = "";
};
StringBuf.__name__ = ["StringBuf"];
StringBuf.prototype = {
	toString: function() {
		return this.b;
	}
	,addSub: function(s,pos,len) {
		this.b += HxOverrides.substr(s,pos,len);
	}
	,addChar: function(c) {
		this.b += String.fromCharCode(c);
	}
	,add: function(x) {
		this.b += Std.string(x);
	}
	,b: null
	,__class__: StringBuf
}
var StringTools = $hxClasses["StringTools"] = function() { }
StringTools.__name__ = ["StringTools"];
StringTools.urlEncode = function(s) {
	return encodeURIComponent(s);
}
StringTools.urlDecode = function(s) {
	return decodeURIComponent(s.split("+").join(" "));
}
StringTools.htmlEscape = function(s,quotes) {
	s = s.split("&").join("&amp;").split("<").join("&lt;").split(">").join("&gt;");
	return quotes?s.split("\"").join("&quot;").split("'").join("&#039;"):s;
}
StringTools.htmlUnescape = function(s) {
	return s.split("&gt;").join(">").split("&lt;").join("<").split("&quot;").join("\"").split("&#039;").join("'").split("&amp;").join("&");
}
StringTools.startsWith = function(s,start) {
	return s.length >= start.length && HxOverrides.substr(s,0,start.length) == start;
}
StringTools.endsWith = function(s,end) {
	var elen = end.length;
	var slen = s.length;
	return slen >= elen && HxOverrides.substr(s,slen - elen,elen) == end;
}
StringTools.isSpace = function(s,pos) {
	var c = HxOverrides.cca(s,pos);
	return c >= 9 && c <= 13 || c == 32;
}
StringTools.ltrim = function(s) {
	var l = s.length;
	var r = 0;
	while(r < l && StringTools.isSpace(s,r)) r++;
	if(r > 0) return HxOverrides.substr(s,r,l - r); else return s;
}
StringTools.rtrim = function(s) {
	var l = s.length;
	var r = 0;
	while(r < l && StringTools.isSpace(s,l - r - 1)) r++;
	if(r > 0) return HxOverrides.substr(s,0,l - r); else return s;
}
StringTools.trim = function(s) {
	return StringTools.ltrim(StringTools.rtrim(s));
}
StringTools.rpad = function(s,c,l) {
	var sl = s.length;
	var cl = c.length;
	while(sl < l) if(l - sl < cl) {
		s += HxOverrides.substr(c,0,l - sl);
		sl = l;
	} else {
		s += c;
		sl += cl;
	}
	return s;
}
StringTools.lpad = function(s,c,l) {
	var ns = "";
	var sl = s.length;
	if(sl >= l) return s;
	var cl = c.length;
	while(sl < l) if(l - sl < cl) {
		ns += HxOverrides.substr(c,0,l - sl);
		sl = l;
	} else {
		ns += c;
		sl += cl;
	}
	return ns + s;
}
StringTools.replace = function(s,sub,by) {
	return s.split(sub).join(by);
}
StringTools.hex = function(n,digits) {
	var s = "";
	var hexChars = "0123456789ABCDEF";
	do {
		s = hexChars.charAt(n & 15) + s;
		n >>>= 4;
	} while(n > 0);
	if(digits != null) while(s.length < digits) s = "0" + s;
	return s;
}
StringTools.fastCodeAt = function(s,index) {
	return s.cca(index);
}
StringTools.isEOF = function(c) {
	return c != c;
}
var ValueType = $hxClasses["ValueType"] = { __ename__ : ["ValueType"], __constructs__ : ["TNull","TInt","TFloat","TBool","TObject","TFunction","TClass","TEnum","TUnknown"] }
ValueType.TNull = ["TNull",0];
ValueType.TNull.toString = $estr;
ValueType.TNull.__enum__ = ValueType;
ValueType.TInt = ["TInt",1];
ValueType.TInt.toString = $estr;
ValueType.TInt.__enum__ = ValueType;
ValueType.TFloat = ["TFloat",2];
ValueType.TFloat.toString = $estr;
ValueType.TFloat.__enum__ = ValueType;
ValueType.TBool = ["TBool",3];
ValueType.TBool.toString = $estr;
ValueType.TBool.__enum__ = ValueType;
ValueType.TObject = ["TObject",4];
ValueType.TObject.toString = $estr;
ValueType.TObject.__enum__ = ValueType;
ValueType.TFunction = ["TFunction",5];
ValueType.TFunction.toString = $estr;
ValueType.TFunction.__enum__ = ValueType;
ValueType.TClass = function(c) { var $x = ["TClass",6,c]; $x.__enum__ = ValueType; $x.toString = $estr; return $x; }
ValueType.TEnum = function(e) { var $x = ["TEnum",7,e]; $x.__enum__ = ValueType; $x.toString = $estr; return $x; }
ValueType.TUnknown = ["TUnknown",8];
ValueType.TUnknown.toString = $estr;
ValueType.TUnknown.__enum__ = ValueType;
var Type = $hxClasses["Type"] = function() { }
Type.__name__ = ["Type"];
Type.getClass = function(o) {
	if(o == null) return null;
	return o.__class__;
}
Type.getEnum = function(o) {
	if(o == null) return null;
	return o.__enum__;
}
Type.getSuperClass = function(c) {
	return c.__super__;
}
Type.getClassName = function(c) {
	var a = c.__name__;
	return a.join(".");
}
Type.getEnumName = function(e) {
	var a = e.__ename__;
	return a.join(".");
}
Type.resolveClass = function(name) {
	var cl = $hxClasses[name];
	if(cl == null || !cl.__name__) return null;
	return cl;
}
Type.resolveEnum = function(name) {
	var e = $hxClasses[name];
	if(e == null || !e.__ename__) return null;
	return e;
}
Type.createInstance = function(cl,args) {
	switch(args.length) {
	case 0:
		return new cl();
	case 1:
		return new cl(args[0]);
	case 2:
		return new cl(args[0],args[1]);
	case 3:
		return new cl(args[0],args[1],args[2]);
	case 4:
		return new cl(args[0],args[1],args[2],args[3]);
	case 5:
		return new cl(args[0],args[1],args[2],args[3],args[4]);
	case 6:
		return new cl(args[0],args[1],args[2],args[3],args[4],args[5]);
	case 7:
		return new cl(args[0],args[1],args[2],args[3],args[4],args[5],args[6]);
	case 8:
		return new cl(args[0],args[1],args[2],args[3],args[4],args[5],args[6],args[7]);
	default:
		throw "Too many arguments";
	}
	return null;
}
Type.createEmptyInstance = function(cl) {
	function empty() {}; empty.prototype = cl.prototype;
	return new empty();
}
Type.createEnum = function(e,constr,params) {
	var f = Reflect.field(e,constr);
	if(f == null) throw "No such constructor " + constr;
	if(Reflect.isFunction(f)) {
		if(params == null) throw "Constructor " + constr + " need parameters";
		return f.apply(e,params);
	}
	if(params != null && params.length != 0) throw "Constructor " + constr + " does not need parameters";
	return f;
}
Type.createEnumIndex = function(e,index,params) {
	var c = e.__constructs__[index];
	if(c == null) throw index + " is not a valid enum constructor index";
	return Type.createEnum(e,c,params);
}
Type.getInstanceFields = function(c) {
	var a = [];
	for(var i in c.prototype) a.push(i);
	HxOverrides.remove(a,"__class__");
	HxOverrides.remove(a,"__properties__");
	return a;
}
Type.getClassFields = function(c) {
	var a = Reflect.fields(c);
	HxOverrides.remove(a,"__name__");
	HxOverrides.remove(a,"__interfaces__");
	HxOverrides.remove(a,"__properties__");
	HxOverrides.remove(a,"__super__");
	HxOverrides.remove(a,"prototype");
	return a;
}
Type.getEnumConstructs = function(e) {
	var a = e.__constructs__;
	return a.slice();
}
Type["typeof"] = function(v) {
	switch(typeof(v)) {
	case "boolean":
		return ValueType.TBool;
	case "string":
		return ValueType.TClass(String);
	case "number":
		if(Math.ceil(v) == v % 2147483648.0) return ValueType.TInt;
		return ValueType.TFloat;
	case "object":
		if(v == null) return ValueType.TNull;
		var e = v.__enum__;
		if(e != null) return ValueType.TEnum(e);
		var c = v.__class__;
		if(c != null) return ValueType.TClass(c);
		return ValueType.TObject;
	case "function":
		if(v.__name__ || v.__ename__) return ValueType.TObject;
		return ValueType.TFunction;
	case "undefined":
		return ValueType.TNull;
	default:
		return ValueType.TUnknown;
	}
}
Type.enumEq = function(a,b) {
	if(a == b) return true;
	try {
		if(a[0] != b[0]) return false;
		var _g1 = 2, _g = a.length;
		while(_g1 < _g) {
			var i = _g1++;
			if(!Type.enumEq(a[i],b[i])) return false;
		}
		var e = a.__enum__;
		if(e != b.__enum__ || e == null) return false;
	} catch( e ) {
		return false;
	}
	return true;
}
Type.enumConstructor = function(e) {
	return e[0];
}
Type.enumParameters = function(e) {
	return e.slice(2);
}
Type.enumIndex = function(e) {
	return e[1];
}
Type.allEnums = function(e) {
	var all = [];
	var cst = e.__constructs__;
	var _g = 0;
	while(_g < cst.length) {
		var c = cst[_g];
		++_g;
		var v = Reflect.field(e,c);
		if(!Reflect.isFunction(v)) all.push(v);
	}
	return all;
}
haxe.Serializer = $hxClasses["haxe.Serializer"] = function() {
	this.buf = new StringBuf();
	this.cache = new Array();
	this.useCache = haxe.Serializer.USE_CACHE;
	this.useEnumIndex = haxe.Serializer.USE_ENUM_INDEX;
	this.shash = new Hash();
	this.scount = 0;
};
haxe.Serializer.__name__ = ["haxe","Serializer"];
haxe.Serializer.run = function(v) {
	var s = new haxe.Serializer();
	s.serialize(v);
	return s.toString();
}
haxe.Serializer.prototype = {
	serializeException: function(e) {
		this.buf.b += "x";
		this.serialize(e);
	}
	,serialize: function(v) {
		var $e = (Type["typeof"](v));
		switch( $e[1] ) {
		case 0:
			this.buf.b += "n";
			break;
		case 1:
			if(v == 0) {
				this.buf.b += "z";
				return;
			}
			this.buf.b += "i";
			this.buf.b += Std.string(v);
			break;
		case 2:
			if(Math.isNaN(v)) this.buf.b += "k"; else if(!Math.isFinite(v)) this.buf.b += Std.string(v < 0?"m":"p"); else {
				this.buf.b += "d";
				this.buf.b += Std.string(v);
			}
			break;
		case 3:
			this.buf.b += Std.string(v?"t":"f");
			break;
		case 6:
			var c = $e[2];
			if(c == String) {
				this.serializeString(v);
				return;
			}
			if(this.useCache && this.serializeRef(v)) return;
			switch(c) {
			case Array:
				var ucount = 0;
				this.buf.b += "a";
				var l = v.length;
				var _g = 0;
				while(_g < l) {
					var i = _g++;
					if(v[i] == null) ucount++; else {
						if(ucount > 0) {
							if(ucount == 1) this.buf.b += "n"; else {
								this.buf.b += "u";
								this.buf.b += Std.string(ucount);
							}
							ucount = 0;
						}
						this.serialize(v[i]);
					}
				}
				if(ucount > 0) {
					if(ucount == 1) this.buf.b += "n"; else {
						this.buf.b += "u";
						this.buf.b += Std.string(ucount);
					}
				}
				this.buf.b += "h";
				break;
			case List:
				this.buf.b += "l";
				var v1 = v;
				var $it0 = v1.iterator();
				while( $it0.hasNext() ) {
					var i = $it0.next();
					this.serialize(i);
				}
				this.buf.b += "h";
				break;
			case Date:
				var d = v;
				this.buf.b += "v";
				this.buf.b += Std.string(HxOverrides.dateStr(d));
				break;
			case Hash:
				this.buf.b += "b";
				var v1 = v;
				var $it1 = v1.keys();
				while( $it1.hasNext() ) {
					var k = $it1.next();
					this.serializeString(k);
					this.serialize(v1.get(k));
				}
				this.buf.b += "h";
				break;
			case IntHash:
				this.buf.b += "q";
				var v1 = v;
				var $it2 = v1.keys();
				while( $it2.hasNext() ) {
					var k = $it2.next();
					this.buf.b += ":";
					this.buf.b += Std.string(k);
					this.serialize(v1.get(k));
				}
				this.buf.b += "h";
				break;
			case haxe.io.Bytes:
				var v1 = v;
				var i = 0;
				var max = v1.length - 2;
				var charsBuf = new StringBuf();
				var b64 = haxe.Serializer.BASE64;
				while(i < max) {
					var b1 = v1.b[i++];
					var b2 = v1.b[i++];
					var b3 = v1.b[i++];
					charsBuf.b += Std.string(b64.charAt(b1 >> 2));
					charsBuf.b += Std.string(b64.charAt((b1 << 4 | b2 >> 4) & 63));
					charsBuf.b += Std.string(b64.charAt((b2 << 2 | b3 >> 6) & 63));
					charsBuf.b += Std.string(b64.charAt(b3 & 63));
				}
				if(i == max) {
					var b1 = v1.b[i++];
					var b2 = v1.b[i++];
					charsBuf.b += Std.string(b64.charAt(b1 >> 2));
					charsBuf.b += Std.string(b64.charAt((b1 << 4 | b2 >> 4) & 63));
					charsBuf.b += Std.string(b64.charAt(b2 << 2 & 63));
				} else if(i == max + 1) {
					var b1 = v1.b[i++];
					charsBuf.b += Std.string(b64.charAt(b1 >> 2));
					charsBuf.b += Std.string(b64.charAt(b1 << 4 & 63));
				}
				var chars = charsBuf.b;
				this.buf.b += "s";
				this.buf.b += Std.string(chars.length);
				this.buf.b += ":";
				this.buf.b += Std.string(chars);
				break;
			default:
				this.cache.pop();
				if(v.hxSerialize != null) {
					this.buf.b += "C";
					this.serializeString(Type.getClassName(c));
					this.cache.push(v);
					v.hxSerialize(this);
					this.buf.b += "g";
				} else {
					this.buf.b += "c";
					this.serializeString(Type.getClassName(c));
					this.cache.push(v);
					this.serializeFields(v);
				}
			}
			break;
		case 4:
			if(this.useCache && this.serializeRef(v)) return;
			this.buf.b += "o";
			this.serializeFields(v);
			break;
		case 7:
			var e = $e[2];
			if(this.useCache && this.serializeRef(v)) return;
			this.cache.pop();
			this.buf.b += Std.string(this.useEnumIndex?"j":"w");
			this.serializeString(Type.getEnumName(e));
			if(this.useEnumIndex) {
				this.buf.b += ":";
				this.buf.b += Std.string(v[1]);
			} else this.serializeString(v[0]);
			this.buf.b += ":";
			var l = v.length;
			this.buf.b += Std.string(l - 2);
			var _g = 2;
			while(_g < l) {
				var i = _g++;
				this.serialize(v[i]);
			}
			this.cache.push(v);
			break;
		case 5:
			throw "Cannot serialize function";
			break;
		default:
			throw "Cannot serialize " + Std.string(v);
		}
	}
	,serializeFields: function(v) {
		var _g = 0, _g1 = Reflect.fields(v);
		while(_g < _g1.length) {
			var f = _g1[_g];
			++_g;
			this.serializeString(f);
			this.serialize(Reflect.field(v,f));
		}
		this.buf.b += "g";
	}
	,serializeRef: function(v) {
		var vt = typeof(v);
		var _g1 = 0, _g = this.cache.length;
		while(_g1 < _g) {
			var i = _g1++;
			var ci = this.cache[i];
			if(typeof(ci) == vt && ci == v) {
				this.buf.b += "r";
				this.buf.b += Std.string(i);
				return true;
			}
		}
		this.cache.push(v);
		return false;
	}
	,serializeString: function(s) {
		var x = this.shash.get(s);
		if(x != null) {
			this.buf.b += "R";
			this.buf.b += Std.string(x);
			return;
		}
		this.shash.set(s,this.scount++);
		this.buf.b += "y";
		s = StringTools.urlEncode(s);
		this.buf.b += Std.string(s.length);
		this.buf.b += ":";
		this.buf.b += Std.string(s);
	}
	,toString: function() {
		return this.buf.b;
	}
	,useEnumIndex: null
	,useCache: null
	,scount: null
	,shash: null
	,cache: null
	,buf: null
	,__class__: haxe.Serializer
}
haxe.Unserializer = $hxClasses["haxe.Unserializer"] = function(buf) {
	this.buf = buf;
	this.length = buf.length;
	this.pos = 0;
	this.scache = new Array();
	this.cache = new Array();
	var r = haxe.Unserializer.DEFAULT_RESOLVER;
	if(r == null) {
		r = Type;
		haxe.Unserializer.DEFAULT_RESOLVER = r;
	}
	this.setResolver(r);
};
haxe.Unserializer.__name__ = ["haxe","Unserializer"];
haxe.Unserializer.initCodes = function() {
	var codes = new Array();
	var _g1 = 0, _g = haxe.Unserializer.BASE64.length;
	while(_g1 < _g) {
		var i = _g1++;
		codes[haxe.Unserializer.BASE64.cca(i)] = i;
	}
	return codes;
}
haxe.Unserializer.run = function(v) {
	return new haxe.Unserializer(v).unserialize();
}
haxe.Unserializer.prototype = {
	unserialize: function() {
		switch(this.buf.cca(this.pos++)) {
		case 110:
			return null;
		case 116:
			return true;
		case 102:
			return false;
		case 122:
			return 0;
		case 105:
			return this.readDigits();
		case 100:
			var p1 = this.pos;
			while(true) {
				var c = this.buf.cca(this.pos);
				if(c >= 43 && c < 58 || c == 101 || c == 69) this.pos++; else break;
			}
			return Std.parseFloat(HxOverrides.substr(this.buf,p1,this.pos - p1));
		case 121:
			var len = this.readDigits();
			if(this.buf.cca(this.pos++) != 58 || this.length - this.pos < len) throw "Invalid string length";
			var s = HxOverrides.substr(this.buf,this.pos,len);
			this.pos += len;
			s = StringTools.urlDecode(s);
			this.scache.push(s);
			return s;
		case 107:
			return Math.NaN;
		case 109:
			return Math.NEGATIVE_INFINITY;
		case 112:
			return Math.POSITIVE_INFINITY;
		case 97:
			var buf = this.buf;
			var a = new Array();
			this.cache.push(a);
			while(true) {
				var c = this.buf.cca(this.pos);
				if(c == 104) {
					this.pos++;
					break;
				}
				if(c == 117) {
					this.pos++;
					var n = this.readDigits();
					a[a.length + n - 1] = null;
				} else a.push(this.unserialize());
			}
			return a;
		case 111:
			var o = { };
			this.cache.push(o);
			this.unserializeObject(o);
			return o;
		case 114:
			var n = this.readDigits();
			if(n < 0 || n >= this.cache.length) throw "Invalid reference";
			return this.cache[n];
		case 82:
			var n = this.readDigits();
			if(n < 0 || n >= this.scache.length) throw "Invalid string reference";
			return this.scache[n];
		case 120:
			throw this.unserialize();
			break;
		case 99:
			var name = this.unserialize();
			var cl = this.resolver.resolveClass(name);
			if(cl == null) throw "Class not found " + name;
			var o = Type.createEmptyInstance(cl);
			this.cache.push(o);
			this.unserializeObject(o);
			return o;
		case 119:
			var name = this.unserialize();
			var edecl = this.resolver.resolveEnum(name);
			if(edecl == null) throw "Enum not found " + name;
			var e = this.unserializeEnum(edecl,this.unserialize());
			this.cache.push(e);
			return e;
		case 106:
			var name = this.unserialize();
			var edecl = this.resolver.resolveEnum(name);
			if(edecl == null) throw "Enum not found " + name;
			this.pos++;
			var index = this.readDigits();
			var tag = Type.getEnumConstructs(edecl)[index];
			if(tag == null) throw "Unknown enum index " + name + "@" + index;
			var e = this.unserializeEnum(edecl,tag);
			this.cache.push(e);
			return e;
		case 108:
			var l = new List();
			this.cache.push(l);
			var buf = this.buf;
			while(this.buf.cca(this.pos) != 104) l.add(this.unserialize());
			this.pos++;
			return l;
		case 98:
			var h = new Hash();
			this.cache.push(h);
			var buf = this.buf;
			while(this.buf.cca(this.pos) != 104) {
				var s = this.unserialize();
				h.set(s,this.unserialize());
			}
			this.pos++;
			return h;
		case 113:
			var h = new IntHash();
			this.cache.push(h);
			var buf = this.buf;
			var c = this.buf.cca(this.pos++);
			while(c == 58) {
				var i = this.readDigits();
				h.set(i,this.unserialize());
				c = this.buf.cca(this.pos++);
			}
			if(c != 104) throw "Invalid IntHash format";
			return h;
		case 118:
			var d = HxOverrides.strDate(HxOverrides.substr(this.buf,this.pos,19));
			this.cache.push(d);
			this.pos += 19;
			return d;
		case 115:
			var len = this.readDigits();
			var buf = this.buf;
			if(this.buf.cca(this.pos++) != 58 || this.length - this.pos < len) throw "Invalid bytes length";
			var codes = haxe.Unserializer.CODES;
			if(codes == null) {
				codes = haxe.Unserializer.initCodes();
				haxe.Unserializer.CODES = codes;
			}
			var i = this.pos;
			var rest = len & 3;
			var size = (len >> 2) * 3 + (rest >= 2?rest - 1:0);
			var max = i + (len - rest);
			var bytes = haxe.io.Bytes.alloc(size);
			var bpos = 0;
			while(i < max) {
				var c1 = codes[buf.cca(i++)];
				var c2 = codes[buf.cca(i++)];
				bytes.b[bpos++] = (c1 << 2 | c2 >> 4) & 255;
				var c3 = codes[buf.cca(i++)];
				bytes.b[bpos++] = (c2 << 4 | c3 >> 2) & 255;
				var c4 = codes[buf.cca(i++)];
				bytes.b[bpos++] = (c3 << 6 | c4) & 255;
			}
			if(rest >= 2) {
				var c1 = codes[buf.cca(i++)];
				var c2 = codes[buf.cca(i++)];
				bytes.b[bpos++] = (c1 << 2 | c2 >> 4) & 255;
				if(rest == 3) {
					var c3 = codes[buf.cca(i++)];
					bytes.b[bpos++] = (c2 << 4 | c3 >> 2) & 255;
				}
			}
			this.pos += len;
			this.cache.push(bytes);
			return bytes;
		case 67:
			var name = this.unserialize();
			var cl = this.resolver.resolveClass(name);
			if(cl == null) throw "Class not found " + name;
			var o = Type.createEmptyInstance(cl);
			this.cache.push(o);
			o.hxUnserialize(this);
			if(this.buf.cca(this.pos++) != 103) throw "Invalid custom data";
			return o;
		default:
		}
		this.pos--;
		throw "Invalid char " + this.buf.charAt(this.pos) + " at position " + this.pos;
	}
	,unserializeEnum: function(edecl,tag) {
		if(this.buf.cca(this.pos++) != 58) throw "Invalid enum format";
		var nargs = this.readDigits();
		if(nargs == 0) return Type.createEnum(edecl,tag);
		var args = new Array();
		while(nargs-- > 0) args.push(this.unserialize());
		return Type.createEnum(edecl,tag,args);
	}
	,unserializeObject: function(o) {
		while(true) {
			if(this.pos >= this.length) throw "Invalid object";
			if(this.buf.cca(this.pos) == 103) break;
			var k = this.unserialize();
			if(!js.Boot.__instanceof(k,String)) throw "Invalid object key";
			var v = this.unserialize();
			o[k] = v;
		}
		this.pos++;
	}
	,readDigits: function() {
		var k = 0;
		var s = false;
		var fpos = this.pos;
		while(true) {
			var c = this.buf.cca(this.pos);
			if(c != c) break;
			if(c == 45) {
				if(this.pos != fpos) break;
				s = true;
				this.pos++;
				continue;
			}
			if(c < 48 || c > 57) break;
			k = k * 10 + (c - 48);
			this.pos++;
		}
		if(s) k *= -1;
		return k;
	}
	,get: function(p) {
		return this.buf.cca(p);
	}
	,getResolver: function() {
		return this.resolver;
	}
	,setResolver: function(r) {
		if(r == null) this.resolver = { resolveClass : function(_) {
			return null;
		}, resolveEnum : function(_) {
			return null;
		}}; else this.resolver = r;
	}
	,resolver: null
	,scache: null
	,cache: null
	,length: null
	,pos: null
	,buf: null
	,__class__: haxe.Unserializer
}
if(!haxe.io) haxe.io = {}
haxe.io.Bytes = $hxClasses["haxe.io.Bytes"] = function(length,b) {
	this.length = length;
	this.b = b;
};
haxe.io.Bytes.__name__ = ["haxe","io","Bytes"];
haxe.io.Bytes.alloc = function(length) {
	var a = new Array();
	var _g = 0;
	while(_g < length) {
		var i = _g++;
		a.push(0);
	}
	return new haxe.io.Bytes(length,a);
}
haxe.io.Bytes.ofString = function(s) {
	var a = new Array();
	var _g1 = 0, _g = s.length;
	while(_g1 < _g) {
		var i = _g1++;
		var c = s.cca(i);
		if(c <= 127) a.push(c); else if(c <= 2047) {
			a.push(192 | c >> 6);
			a.push(128 | c & 63);
		} else if(c <= 65535) {
			a.push(224 | c >> 12);
			a.push(128 | c >> 6 & 63);
			a.push(128 | c & 63);
		} else {
			a.push(240 | c >> 18);
			a.push(128 | c >> 12 & 63);
			a.push(128 | c >> 6 & 63);
			a.push(128 | c & 63);
		}
	}
	return new haxe.io.Bytes(a.length,a);
}
haxe.io.Bytes.ofData = function(b) {
	return new haxe.io.Bytes(b.length,b);
}
haxe.io.Bytes.fastGet = function(b,pos) {
	return b[pos];
}
haxe.io.Bytes.prototype = {
	getData: function() {
		return this.b;
	}
	,toHex: function() {
		var s = new StringBuf();
		var chars = [];
		var str = "0123456789abcdef";
		var _g1 = 0, _g = str.length;
		while(_g1 < _g) {
			var i = _g1++;
			chars.push(HxOverrides.cca(str,i));
		}
		var _g1 = 0, _g = this.length;
		while(_g1 < _g) {
			var i = _g1++;
			var c = this.b[i];
			s.b += String.fromCharCode(chars[c >> 4]);
			s.b += String.fromCharCode(chars[c & 15]);
		}
		return s.b;
	}
	,toString: function() {
		return this.readString(0,this.length);
	}
	,readString: function(pos,len) {
		if(pos < 0 || len < 0 || pos + len > this.length) throw haxe.io.Error.OutsideBounds;
		var s = "";
		var b = this.b;
		var fcc = String.fromCharCode;
		var i = pos;
		var max = pos + len;
		while(i < max) {
			var c = b[i++];
			if(c < 128) {
				if(c == 0) break;
				s += fcc(c);
			} else if(c < 224) s += fcc((c & 63) << 6 | b[i++] & 127); else if(c < 240) {
				var c2 = b[i++];
				s += fcc((c & 31) << 12 | (c2 & 127) << 6 | b[i++] & 127);
			} else {
				var c2 = b[i++];
				var c3 = b[i++];
				s += fcc((c & 15) << 18 | (c2 & 127) << 12 | c3 << 6 & 127 | b[i++] & 127);
			}
		}
		return s;
	}
	,compare: function(other) {
		var b1 = this.b;
		var b2 = other.b;
		var len = this.length < other.length?this.length:other.length;
		var _g = 0;
		while(_g < len) {
			var i = _g++;
			if(b1[i] != b2[i]) return b1[i] - b2[i];
		}
		return this.length - other.length;
	}
	,sub: function(pos,len) {
		if(pos < 0 || len < 0 || pos + len > this.length) throw haxe.io.Error.OutsideBounds;
		return new haxe.io.Bytes(len,this.b.slice(pos,pos + len));
	}
	,blit: function(pos,src,srcpos,len) {
		if(pos < 0 || srcpos < 0 || len < 0 || pos + len > this.length || srcpos + len > src.length) throw haxe.io.Error.OutsideBounds;
		var b1 = this.b;
		var b2 = src.b;
		if(b1 == b2 && pos > srcpos) {
			var i = len;
			while(i > 0) {
				i--;
				b1[i + pos] = b2[i + srcpos];
			}
			return;
		}
		var _g = 0;
		while(_g < len) {
			var i = _g++;
			b1[i + pos] = b2[i + srcpos];
		}
	}
	,set: function(pos,v) {
		this.b[pos] = v & 255;
	}
	,get: function(pos) {
		return this.b[pos];
	}
	,b: null
	,length: null
	,__class__: haxe.io.Bytes
}
haxe.io.Error = $hxClasses["haxe.io.Error"] = { __ename__ : ["haxe","io","Error"], __constructs__ : ["Blocked","Overflow","OutsideBounds","Custom"] }
haxe.io.Error.Blocked = ["Blocked",0];
haxe.io.Error.Blocked.toString = $estr;
haxe.io.Error.Blocked.__enum__ = haxe.io.Error;
haxe.io.Error.Overflow = ["Overflow",1];
haxe.io.Error.Overflow.toString = $estr;
haxe.io.Error.Overflow.__enum__ = haxe.io.Error;
haxe.io.Error.OutsideBounds = ["OutsideBounds",2];
haxe.io.Error.OutsideBounds.toString = $estr;
haxe.io.Error.OutsideBounds.__enum__ = haxe.io.Error;
haxe.io.Error.Custom = function(e) { var $x = ["Custom",3,e]; $x.__enum__ = haxe.io.Error; $x.toString = $estr; return $x; }
haxe.remoting.Connection = $hxClasses["haxe.remoting.Connection"] = function() { }
haxe.remoting.Connection.__name__ = ["haxe","remoting","Connection"];
haxe.remoting.Connection.prototype = {
	call: null
	,resolve: null
	,__class__: haxe.remoting.Connection
}
haxe.remoting.ExternalConnection = $hxClasses["haxe.remoting.ExternalConnection"] = function(data,path) {
	this.__data = data;
	this.__path = path;
};
haxe.remoting.ExternalConnection.__name__ = ["haxe","remoting","ExternalConnection"];
haxe.remoting.ExternalConnection.__interfaces__ = [haxe.remoting.Connection];
haxe.remoting.ExternalConnection.escapeString = function(s) {
	return s;
}
haxe.remoting.ExternalConnection.doCall = function(name,path,params) {
	try {
		var cnx = haxe.remoting.ExternalConnection.connections.get(name);
		if(cnx == null) throw "Unknown connection : " + name;
		if(cnx.__data.ctx == null) throw "No context shared for the connection " + name;
		var params1 = new haxe.Unserializer(params).unserialize();
		var ret = cnx.__data.ctx.call(path.split("."),params1);
		var s = new haxe.Serializer();
		s.serialize(ret);
		return s.toString() + "#";
	} catch( e ) {
		var s = new haxe.Serializer();
		s.serializeException(e);
		return s.toString();
	}
}
haxe.remoting.ExternalConnection.flashConnect = function(name,flashObjectID,ctx) {
	var cnx = new haxe.remoting.ExternalConnection({ ctx : ctx, name : name, flash : flashObjectID},[]);
	haxe.remoting.ExternalConnection.connections.set(name,cnx);
	return cnx;
}
haxe.remoting.ExternalConnection.prototype = {
	call: function(params) {
		var s = new haxe.Serializer();
		s.serialize(params);
		var params1 = s.toString();
		var data = null;
		var fobj = window.document[this.__data.flash];
		if(fobj == null) fobj = window.document.getElementById(this.__data.flash);
		if(fobj == null) throw "Could not find flash object '" + this.__data.flash + "'";
		try {
			data = fobj.externalRemotingCall(this.__data.name,this.__path.join("."),params1);
		} catch( e ) {
		}
		if(data == null) {
			var domain, pageDomain;
			try {
				domain = fobj.src.split("/")[2];
				pageDomain = js.Lib.window.location.host;
			} catch( e ) {
				domain = null;
				pageDomain = null;
			}
			if(domain != pageDomain) throw "ExternalConnection call failure : SWF need allowDomain('" + pageDomain + "')";
			throw "Call failure : ExternalConnection is not " + "initialized in Flash";
		}
		return new haxe.Unserializer(data).unserialize();
	}
	,close: function() {
		haxe.remoting.ExternalConnection.connections.remove(this.__data.name);
	}
	,resolve: function(field) {
		var e = new haxe.remoting.ExternalConnection(this.__data,this.__path.slice());
		e.__path.push(field);
		return e;
	}
	,__path: null
	,__data: null
	,__class__: haxe.remoting.ExternalConnection
}
var js = js || {}
js.Boot = $hxClasses["js.Boot"] = function() { }
js.Boot.__name__ = ["js","Boot"];
js.Boot.__unhtml = function(s) {
	return s.split("&").join("&amp;").split("<").join("&lt;").split(">").join("&gt;");
}
js.Boot.__trace = function(v,i) {
	var msg = i != null?i.fileName + ":" + i.lineNumber + ": ":"";
	msg += js.Boot.__string_rec(v,"");
	var d;
	if(typeof(document) != "undefined" && (d = document.getElementById("haxe:trace")) != null) d.innerHTML += js.Boot.__unhtml(msg) + "<br/>"; else if(typeof(console) != "undefined" && console.log != null) console.log(msg);
}
js.Boot.__clear_trace = function() {
	var d = document.getElementById("haxe:trace");
	if(d != null) d.innerHTML = "";
}
js.Boot.isClass = function(o) {
	return o.__name__;
}
js.Boot.isEnum = function(e) {
	return e.__ename__;
}
js.Boot.getClass = function(o) {
	return o.__class__;
}
js.Boot.__string_rec = function(o,s) {
	if(o == null) return "null";
	if(s.length >= 5) return "<...>";
	var t = typeof(o);
	if(t == "function" && (o.__name__ || o.__ename__)) t = "object";
	switch(t) {
	case "object":
		if(o instanceof Array) {
			if(o.__enum__) {
				if(o.length == 2) return o[0];
				var str = o[0] + "(";
				s += "\t";
				var _g1 = 2, _g = o.length;
				while(_g1 < _g) {
					var i = _g1++;
					if(i != 2) str += "," + js.Boot.__string_rec(o[i],s); else str += js.Boot.__string_rec(o[i],s);
				}
				return str + ")";
			}
			var l = o.length;
			var i;
			var str = "[";
			s += "\t";
			var _g = 0;
			while(_g < l) {
				var i1 = _g++;
				str += (i1 > 0?",":"") + js.Boot.__string_rec(o[i1],s);
			}
			str += "]";
			return str;
		}
		var tostr;
		try {
			tostr = o.toString;
		} catch( e ) {
			return "???";
		}
		if(tostr != null && tostr != Object.toString) {
			var s2 = o.toString();
			if(s2 != "[object Object]") return s2;
		}
		var k = null;
		var str = "{\n";
		s += "\t";
		var hasp = o.hasOwnProperty != null;
		for( var k in o ) { ;
		if(hasp && !o.hasOwnProperty(k)) {
			continue;
		}
		if(k == "prototype" || k == "__class__" || k == "__super__" || k == "__interfaces__" || k == "__properties__") {
			continue;
		}
		if(str.length != 2) str += ", \n";
		str += s + k + " : " + js.Boot.__string_rec(o[k],s);
		}
		s = s.substring(1);
		str += "\n" + s + "}";
		return str;
	case "function":
		return "<function>";
	case "string":
		return o;
	default:
		return String(o);
	}
}
js.Boot.__interfLoop = function(cc,cl) {
	if(cc == null) return false;
	if(cc == cl) return true;
	var intf = cc.__interfaces__;
	if(intf != null) {
		var _g1 = 0, _g = intf.length;
		while(_g1 < _g) {
			var i = _g1++;
			var i1 = intf[i];
			if(i1 == cl || js.Boot.__interfLoop(i1,cl)) return true;
		}
	}
	return js.Boot.__interfLoop(cc.__super__,cl);
}
js.Boot.__instanceof = function(o,cl) {
	try {
		if(o instanceof cl) {
			if(cl == Array) return o.__enum__ == null;
			return true;
		}
		if(js.Boot.__interfLoop(o.__class__,cl)) return true;
	} catch( e ) {
		if(cl == null) return false;
	}
	switch(cl) {
	case Int:
		return Math.ceil(o%2147483648.0) === o;
	case Float:
		return typeof(o) == "number";
	case Bool:
		return o === true || o === false;
	case String:
		return typeof(o) == "string";
	case Dynamic:
		return true;
	default:
		if(o == null) return false;
		if(cl == Class && o.__name__ != null) return true; else null;
		if(cl == Enum && o.__ename__ != null) return true; else null;
		return o.__enum__ == cl;
	}
}
js.Boot.__cast = function(o,t) {
	if(js.Boot.__instanceof(o,t)) return o; else throw "Cannot cast " + Std.string(o) + " to " + Std.string(t);
}
js.Lib = $hxClasses["js.Lib"] = function() { }
js.Lib.__name__ = ["js","Lib"];
js.Lib.document = null;
js.Lib.window = null;
js.Lib.debug = function() {
	debugger;
}
js.Lib.alert = function(v) {
	alert(js.Boot.__string_rec(v,""));
}
js.Lib.eval = function(code) {
	return eval(code);
}
js.Lib.setErrorHandler = function(f) {
	js.Lib.onerror = f;
}
if(!js.fx) js.fx = {}
js.fx.Anim = $hxClasses["js.fx.Anim"] = function() {
	this.fps = 60;
	this.duration = 250;
	this.transition = js.fx.TransitionFunctions.get(js.fx.Transition.Linear);
};
js.fx.Anim.__name__ = ["js","fx","Anim"];
js.fx.Anim.prototype = {
	startTimer: function() {
		if(this.timer != null) return false;
		this.time = new Date().getTime() - this.time;
		this.timer = js.fx.Timer.periodical((function(f) {
			return function() {
				return f();
			};
		})($bind(this,this.next)),Math.round(1000 / this.fps));
		return true;
	}
	,stopTimer: function() {
		if(this.timer == null) return false;
		this.time = new Date().getTime() - this.time;
		this.timer = js.fx.Timer.clear(this.timer);
		return true;
	}
	,complete: function() {
		if(this.stopTimer()) {
			if(this.onComplete != null) this.onComplete();
			return true;
		}
		return false;
	}
	,next: function() {
		var now = new Date().getTime();
		if(now < this.time + this.duration) {
			var delta = this.transition((now - this.time) / this.duration);
			this.set(delta);
		} else {
			this.set(1);
			this.complete();
		}
	}
	,set: function(delta) {
		throw "set(delta:Float) not implemented";
	}
	,resume: function() {
		this.startTimer();
	}
	,pause: function() {
		this.stopTimer();
	}
	,cancel: function() {
		if(this.stopTimer() && this.onCancel != null) this.onCancel();
	}
	,start: function() {
		this.time = 0;
		this.startTimer();
		if(this.onStart != null) this.onStart();
	}
	,setTransition: function(t) {
		this.transition = js.fx.TransitionFunctions.get(t);
	}
	,onCancel: null
	,onStart: null
	,onComplete: null
	,transition: null
	,timer: null
	,time: null
	,fps: null
	,duration: null
	,__class__: js.fx.Anim
}
js.fx.Morph = $hxClasses["js.fx.Morph"] = function(e,toStyle) {
	js.fx.Anim.call(this);
	this.element = e;
	this.targetStyle = toStyle;
	this.originStyle = js.fx.Style.getStyles(this.element,toStyle);
};
js.fx.Morph.__name__ = ["js","fx","Morph"];
js.fx.Morph.__super__ = js.fx.Anim;
js.fx.Morph.prototype = $extend(js.fx.Anim.prototype,{
	set: function(delta) {
		js.fx.Style.setStyles(this.element,js.fx.Delta.styles(this.originStyle,this.targetStyle,delta));
	}
	,targetStyle: null
	,originStyle: null
	,element: null
	,__class__: js.fx.Morph
});
js.fx.Style = $hxClasses["js.fx.Style"] = function() { }
js.fx.Style.__name__ = ["js","fx","Style"];
js.fx.Style.getDocumentStyles = function(selector) {
	var result = { };
	var found = false;
	var _g1 = 0, _g = js.Lib.document.styleSheets.length;
	while(_g1 < _g) {
		var i = _g1++;
		var sheet = js.Lib.document.styleSheets[i];
		if(sheet.href != null && sheet.href.indexOf("://") != -1 && sheet.href.indexOf(js.Lib.document.domain) == -1) continue;
		var rules = sheet.rules != null?sheet.rules:sheet.cssRules;
		if(rules == null) continue;
		var _g3 = 0, _g2 = rules.length;
		while(_g3 < _g2) {
			var j = _g3++;
			var rule = rules[j];
			if(rule.style == null || rule.selectorText == null || rule.selectorText != selector) continue;
			found = true;
			if(js.fx.Tool.isIE) {
				var _g4 = 0, _g5 = Reflect.fields(rule.style);
				while(_g4 < _g5.length) {
					var f = _g5[_g4];
					++_g4;
					var value = Reflect.field(rule.style,f);
					if(value == null || value == "") continue;
					var value1 = js.fx.Style.parseStyle(value);
					if(value1 != null && value1 != "") result[f] = value1;
				}
			} else {
				var _g5 = 0, _g4 = rule.style.length;
				while(_g5 < _g4) {
					var s = _g5++;
					var style = rule.style[s];
					var camel = js.fx.Style.camel(style);
					var value = Reflect.field(rule.style,camel);
					if(value != null && value != "") result[camel] = js.fx.Style.parseStyle(value);
				}
			}
		}
	}
	if(!found) return null;
	return result;
}
js.fx.Style.getStyles = function(e,template) {
	var getter = null;
	if(e.currentStyle != null) getter = function(key) {
		return e.currentStyle[key];
	};
	if(getter == null) {
		var window = js.Lib.document.defaultView?js.Lib.document.defaultView:js.Lib.document.parentWindow;
		if(window != null && window.getComputedStyle != null) {
			var cs = window.getComputedStyle(e,null);
			if(cs != null) getter = function(key) {
				return cs.getPropertyValue(js.fx.Style.hyphen(key));
			};
		}
	}
	if(getter == null) getter = function(key) {
		return Reflect.field(e,key);
	};
	var result = { };
	var _g = 0, _g1 = Reflect.fields(template);
	while(_g < _g1.length) {
		var f = _g1[_g];
		++_g;
		var currentValue = f == "opacity"?Std.string(js.fx.Style.getOpacity(e)):getter(f);
		var expectedType = Reflect.field(template,f);
		if(js.Boot.__instanceof(expectedType,js.fx.RGBA)) {
			var value = js.fx.RGBA.parse(currentValue);
			if(value == null) value = new js.fx.RGBA(0,0,0,0);
			result[f] = value;
			continue;
		}
		if(js.Boot.__instanceof(expectedType,js.fx.RGB)) {
			var value = js.fx.RGB.parse(currentValue);
			if(value == null) value = new js.fx.RGB(0,0,0);
			result[f] = value;
			continue;
		}
		if(js.Boot.__instanceof(expectedType,js.fx.Unit)) {
			var value = js.fx.Unit.parse(currentValue);
			if(value == null) value = new js.fx.Unit(expectedType.kind,0);
			if(value.kind != expectedType.kind) throw "Unit kind mismatch " + Std.string(value) + " != " + Std.string(expectedType);
			result[f] = value;
			continue;
		}
		if(js.Boot.__instanceof(expectedType,String)) result[f] = currentValue;
		var type = Type["typeof"](expectedType);
		if(type == ValueType.TFloat || f == "opacity") {
			var value = Std.parseFloat(Std.string(currentValue)) + 0.000000001;
			if(value == null) value = 0.000000001;
			result[f] = value;
		} else if(type == ValueType.TInt) {
			var value = Std.parseInt(Std.string(currentValue));
			if(value == null) value = 0;
			result[f] = value;
		}
	}
	return result;
}
js.fx.Style.setStyles = function(e,styles) {
	var _g = 0, _g1 = Reflect.fields(styles);
	while(_g < _g1.length) {
		var f = _g1[_g];
		++_g;
		js.fx.Style.setStyle(e,f,Reflect.field(styles,f));
	}
}
js.fx.Style.setStyle = function(e,property,value) {
	switch(property) {
	case "opacity":
		js.fx.Style.setOpacity(e,value);
		return;
	case "float":
		property = js.fx.Style.REQUIRES_FILTERS?"styleFloat":"cssFloat";
		break;
	case "zIndex":
		value = Std.string(value);
		break;
	}
	e.style[property] = js.fx.Style.styleValueToString(value);
}
js.fx.Style.setOpacity = function(e,opacity) {
	e.style.visibility = opacity == 0.0?"hidden":"visible";
	if(js.fx.Style.REQUIRES_FILTERS) {
		e.style.zoom = 1;
		e.style["-ms-filter"] = "\"progid:DXImageTransform.Microsoft.Alpha(Opacity=" + (opacity * 100 | 0) + ")\"";
		e.style.filter = opacity == 1.0?"":"alpha(opacity=" + (opacity * 100 | 0) + ")";
	}
	e.style.opacity = opacity;
	e.style._opacity = opacity;
}
js.fx.Style.getOpacity = function(element) {
	var v = element.style._opacity;
	if(v == null) return (function($this) {
		var $r;
		switch(element.style.visibility) {
		case "visible":
			$r = 1.0;
			break;
		case "hidden":
			$r = 0.0;
			break;
		case "":
			$r = element.style.display == "none"?0.0:1.0;
			break;
		}
		return $r;
	}(this));
	return v;
}
js.fx.Style.styleValueToString = function(v) {
	return (function($this) {
		var $r;
		var $e = (Type["typeof"](v));
		switch( $e[1] ) {
		case 6:
			var c = $e[2];
			$r = Std.string(v);
			break;
		case 1:
			$r = Std.string(v) + "px";
			break;
		case 2:
			$r = Std.string(v);
			break;
		case 0:
			$r = "";
			break;
		default:
			$r = (function($this) {
				var $r;
				throw "Unsupported js.fx.Style value " + Std.string(Type["typeof"](v));
				return $r;
			}($this));
		}
		return $r;
	}(this));
}
js.fx.Style.parseStyle = function(s) {
	var rgba = js.fx.RGBA.parse(s);
	if(rgba != null) return rgba;
	var rgb = js.fx.RGB.parse(s);
	if(rgb != null) return rgb;
	var unit = js.fx.Unit.parse(s);
	if(unit != null) return unit;
	return s;
}
js.fx.Style.camel = function(hyphen) {
	return new EReg("(-[a-z])","").customReplace(hyphen,function(reg) {
		return reg.matched(1).charAt(1).toUpperCase();
	});
}
js.fx.Style.hyphen = function(camel) {
	return new EReg("([A-Z])","").customReplace(camel,function(reg) {
		return "-" + reg.matched(1).toLowerCase();
	});
}
js.fx.RGBA = $hxClasses["js.fx.RGBA"] = function(r,g,b,a) {
	this.r = r;
	this.g = g;
	this.b = g;
	this.a = a;
};
js.fx.RGBA.__name__ = ["js","fx","RGBA"];
js.fx.RGBA.hexToInt = function(str) {
	str = str.toLowerCase();
	if(str.length == 0) return 0;
	if(str.length == 1) {
		var c = HxOverrides.cca(str,0);
		var a = HxOverrides.cca("a",0);
		if(c >= a) return 10 + c - a;
		var z = HxOverrides.cca("0",0);
		if(c >= z) return c - z;
	}
	if(str.length == 2) return 16 * js.fx.RGBA.hexToInt(str.charAt(0)) + js.fx.RGBA.hexToInt(str.charAt(1));
	return 0;
}
js.fx.RGBA.parse = function(value) {
	var reg = new EReg("^#?([a-f0-9]{1,2})([a-f0-9]{1,2})([a-f0-9]{1,2})([a-f0-9]{1,2})$","i");
	if(reg.match(value)) {
		var result = new js.fx.RGBA(js.fx.RGBA.hexToInt(reg.matched(1)),js.fx.RGBA.hexToInt(reg.matched(2)),js.fx.RGBA.hexToInt(reg.matched(3)),js.fx.RGBA.hexToInt(reg.matched(4)));
		return result;
	}
	var reg1 = new EReg("rgba\\(\\s*(\\d+),\\s*(\\d+),\\s*(\\d+),\\s*(\\d+)\\s*\\)","");
	if(reg1.match(value)) return new js.fx.RGBA(Std.parseInt(reg1.matched(1)),Std.parseInt(reg1.matched(2)),Std.parseInt(reg1.matched(3)),Std.parseInt(reg1.matched(4)));
	return null;
}
js.fx.RGBA.prototype = {
	toString: function() {
		return "rgba(" + this.r + "," + this.g + "," + this.b + "," + this.a + ")";
	}
	,a: null
	,b: null
	,g: null
	,r: null
	,__class__: js.fx.RGBA
}
js.fx.RGB = $hxClasses["js.fx.RGB"] = function(r,g,b) {
	this.r = r;
	this.g = g;
	this.b = g;
};
js.fx.RGB.__name__ = ["js","fx","RGB"];
js.fx.RGB.hexToInt = function(str) {
	str = str.toLowerCase();
	if(str.length == 0) return 0;
	if(str.length == 1) {
		var c = HxOverrides.cca(str,0);
		var a = HxOverrides.cca("a",0);
		if(c >= a) return 10 + c - a;
		var z = HxOverrides.cca("0",0);
		if(c >= z) return c - z;
	}
	if(str.length == 2) return 16 * js.fx.RGB.hexToInt(str.charAt(0)) + js.fx.RGB.hexToInt(str.charAt(1));
	return 0;
}
js.fx.RGB.parse = function(value) {
	var reg = new EReg("^#?([a-f0-9]{1,2})([a-f0-9]{1,2})([a-f0-9]{1,2})$","i");
	if(reg.match(value)) {
		var result = new js.fx.RGB(js.fx.RGB.hexToInt(reg.matched(1)),js.fx.RGB.hexToInt(reg.matched(2)),js.fx.RGB.hexToInt(reg.matched(3)));
		return result;
	}
	var reg1 = new EReg("rgb\\(\\s*(\\d+),\\s*(\\d+),\\s*(\\d+)\\s*\\)","");
	if(reg1.match(value)) return new js.fx.RGB(Std.parseInt(reg1.matched(1)),Std.parseInt(reg1.matched(2)),Std.parseInt(reg1.matched(3)));
	return null;
}
js.fx.RGB.prototype = {
	toString: function() {
		return "rgb(" + this.r + "," + this.g + "," + this.b + ")";
	}
	,b: null
	,g: null
	,r: null
	,__class__: js.fx.RGB
}
js.fx.UnitKind = $hxClasses["js.fx.UnitKind"] = { __ename__ : ["js","fx","UnitKind"], __constructs__ : ["KEm","KPt","KPc","KPx"] }
js.fx.UnitKind.KEm = ["KEm",0];
js.fx.UnitKind.KEm.toString = $estr;
js.fx.UnitKind.KEm.__enum__ = js.fx.UnitKind;
js.fx.UnitKind.KPt = ["KPt",1];
js.fx.UnitKind.KPt.toString = $estr;
js.fx.UnitKind.KPt.__enum__ = js.fx.UnitKind;
js.fx.UnitKind.KPc = ["KPc",2];
js.fx.UnitKind.KPc.toString = $estr;
js.fx.UnitKind.KPc.__enum__ = js.fx.UnitKind;
js.fx.UnitKind.KPx = ["KPx",3];
js.fx.UnitKind.KPx.toString = $estr;
js.fx.UnitKind.KPx.__enum__ = js.fx.UnitKind;
js.fx.Unit = $hxClasses["js.fx.Unit"] = function(k,v) {
	this.kind = k;
	this.value = v;
	this.toString = (function($this) {
		var $r;
		switch( (k)[1] ) {
		case 0:
			$r = $bind($this,$this.emToString);
			break;
		case 1:
			$r = $bind($this,$this.ptToString);
			break;
		case 2:
			$r = $bind($this,$this.pcToString);
			break;
		case 3:
			$r = $bind($this,$this.pxToString);
			break;
		}
		return $r;
	}(this));
};
js.fx.Unit.__name__ = ["js","fx","Unit"];
js.fx.Unit.parse = function(value) {
	var reg = new EReg("^(\\-?[0-9.]+)(em|pt|%|px|)$","i");
	if(!reg.match(value)) return null;
	var val = Std.parseFloat(reg.matched(1));
	var kst = reg.matched(2);
	return (function($this) {
		var $r;
		switch(kst) {
		case "em":
			$r = new js.fx.Unit(js.fx.UnitKind.KEm,val);
			break;
		case "pt":
			$r = new js.fx.Unit(js.fx.UnitKind.KPt,val);
			break;
		case "px":
			$r = new js.fx.Unit(js.fx.UnitKind.KPx,val);
			break;
		case "%":
			$r = new js.fx.Unit(js.fx.UnitKind.KPc,val);
			break;
		default:
			$r = new js.fx.Unit(js.fx.UnitKind.KPx,val);
		}
		return $r;
	}(this));
}
js.fx.Unit.prototype = {
	pxToString: function() {
		return Math.round(this.value) + "px";
	}
	,pcToString: function() {
		return Math.round(this.value * 10) / 10 + "%";
	}
	,ptToString: function() {
		return Math.round(this.value * 10) / 10 + "pt";
	}
	,emToString: function() {
		return Math.round(this.value * 10) / 10 + "em";
	}
	,toString: null
	,value: null
	,kind: null
	,__class__: js.fx.Unit
}
js.fx.Delta = $hxClasses["js.fx.Delta"] = function() { }
js.fx.Delta.__name__ = ["js","fx","Delta"];
js.fx.Delta.getDeltaFunc = function(from,to) {
	return js.Boot.__instanceof(from,js.fx.RGB)?js.fx.Delta.rgb:js.Boot.__instanceof(from,js.fx.RGBA)?js.fx.Delta.rgba:js.Boot.__instanceof(from,js.fx.Unit)?js.fx.Delta.unit:js.Boot.__instanceof(from,String)?js.fx.Delta.string:Type["typeof"](from) == ValueType.TFloat?js.fx.Delta["float"]:Type["typeof"](from) == ValueType.TInt?js.fx.Delta["int"]:js.fx.Delta.unknown;
}
js.fx.Delta.styles = function(from,to,delta) {
	var result = { };
	var _g = 0, _g1 = Reflect.fields(from);
	while(_g < _g1.length) {
		var f = _g1[_g];
		++_g;
		result[f] = js.fx.Delta.any(Reflect.field(from,f),Reflect.field(to,f),delta);
	}
	return result;
}
js.fx.Delta.unknown = function(from,to,delta) {
	return delta < 0.5?from:to;
}
js.fx.Delta.any = function(a,b,delta) {
	return (js.fx.Delta.getDeltaFunc(a,b))(a,b,delta);
}
js.fx.Delta.rgb = function(from,to,delta) {
	return new js.fx.RGB(js.fx.Delta["int"](from.r,to.r,delta),js.fx.Delta["int"](from.g,to.g,delta),js.fx.Delta["int"](from.b,to.b,delta));
}
js.fx.Delta.rgba = function(from,to,delta) {
	return new js.fx.RGBA(js.fx.Delta["int"](from.r,to.r,delta),js.fx.Delta["int"](from.g,to.g,delta),js.fx.Delta["int"](from.b,to.b,delta),js.fx.Delta["int"](from.a,to.a,delta));
}
js.fx.Delta.unit = function(from,to,delta) {
	return new js.fx.Unit(from.kind,js.fx.Delta["float"](from.value,to.value,delta));
}
js.fx.Delta.string = function(from,to,delta) {
	return delta < 0.5?from:to;
}
js.fx.Delta["int"] = function(from,to,delta) {
	return Math.floor((to - from) * delta + from) | 0;
}
js.fx.Delta["float"] = function(from,to,delta) {
	return (to - from) * delta + from;
}
js.fx.Timer = $hxClasses["js.fx.Timer"] = function() {
};
js.fx.Timer.__name__ = ["js","fx","Timer"];
js.fx.Timer.timeout = function(cb,delay) {
	var t = new js.fx.Timer();
	t.data = setTimeout(cb,delay);
	return t;
}
js.fx.Timer.periodical = function(cb,delay) {
	var t = new js.fx.Timer();
	t.data = setInterval(cb,delay);
	return t;
}
js.fx.Timer.clear = function(t) {
	clearTimeout(t.data);
	clearInterval(t.data);
	return null;
}
js.fx.Timer.prototype = {
	data: null
	,__class__: js.fx.Timer
}
js.fx.Tool = $hxClasses["js.fx.Tool"] = function() { }
js.fx.Tool.__name__ = ["js","fx","Tool"];
js.fx.Tool.nextElement = function(node) {
	var next = node.nextSibling;
	while(next != null && next.nodeName != node.nodeName) next = next.nextSibling;
	return next;
}
js.fx.Tool.prevElement = function(node) {
	var prev = node.previousSibling;
	while(prev != null && prev.nodeName != node.nodeName) prev = prev.previousSibling;
	return prev;
}
js.fx.Tool.placeBefore = function(newNode,oldNode) {
	if(newNode.parentNode != null) newNode.parentNode.removeChild(newNode);
	oldNode.parentNode.insertBefore(newNode,oldNode);
}
js.fx.Tool.placeAfter = function(newNode,oldNode) {
	if(newNode.parentNode != null) newNode.parentNode.removeChild(newNode);
	if(oldNode.nextSibling == null) oldNode.parentNode.appendChild(newNode); else oldNode.parentNode.insertBefore(newNode,oldNode.nextSibling);
}
js.fx.Tool.trace = function(x) {
	js.Lib.document.getElementById("trace").innerHTML = Std.string(x);
}
js.fx.Tool.addCssClass = function(e,c) {
	if(e.className.indexOf(c) == -1) e.className = e.className + " " + c;
}
js.fx.Tool.removeCssClass = function(e,c) {
	if(e.className.indexOf(c) != -1) e.className = StringTools.replace(e.className,c,"");
}
js.fx.Tool.preventDefault = function(evt) {
	if(evt == null) evt = js.Lib.window.event;
	if(evt.preventDefault != null) evt.preventDefault(); else evt.returnValue = false;
	return false;
}
js.fx.Tool.cancelBubble = function(evt) {
	if(evt == null) evt = js.Lib.window.event;
	if($bind(evt,evt.stopPropagation) != null) evt.stopPropagation(); else evt.cancelBubble = true;
	return false;
}
js.fx.Tool.findElementsWithClassName = function(parent,tag,className) {
	if(parent == null) parent = js.Lib.document.body;
	if(tag == null) tag = "*";
	var elements = tag == "*" && parent.all != null?parent.all:parent.getElementsByTagName(tag);
	var results = new List();
	var regexp = new EReg("(^|\\s)" + className + "(\\s|$)","");
	var _g1 = 0, _g = elements.length;
	while(_g1 < _g) {
		var i = _g1++;
		if(Std.string(elements[i].className) == "") continue;
		if(regexp.match("" + elements[i].className)) results.push(elements[i]);
	}
	return results;
}
js.fx.Transition = $hxClasses["js.fx.Transition"] = { __ename__ : ["js","fx","Transition"], __constructs__ : ["Linear","Quad","Cubic","Quart","Quint","Pow","Expo","Circ","Sine","Back","Bounce","Elastic"] }
js.fx.Transition.Linear = ["Linear",0];
js.fx.Transition.Linear.toString = $estr;
js.fx.Transition.Linear.__enum__ = js.fx.Transition;
js.fx.Transition.Quad = function(p) { var $x = ["Quad",1,p]; $x.__enum__ = js.fx.Transition; $x.toString = $estr; return $x; }
js.fx.Transition.Cubic = function(p) { var $x = ["Cubic",2,p]; $x.__enum__ = js.fx.Transition; $x.toString = $estr; return $x; }
js.fx.Transition.Quart = function(p) { var $x = ["Quart",3,p]; $x.__enum__ = js.fx.Transition; $x.toString = $estr; return $x; }
js.fx.Transition.Quint = function(p) { var $x = ["Quint",4,p]; $x.__enum__ = js.fx.Transition; $x.toString = $estr; return $x; }
js.fx.Transition.Pow = function(pa) { var $x = ["Pow",5,pa]; $x.__enum__ = js.fx.Transition; $x.toString = $estr; return $x; }
js.fx.Transition.Expo = function(p) { var $x = ["Expo",6,p]; $x.__enum__ = js.fx.Transition; $x.toString = $estr; return $x; }
js.fx.Transition.Circ = function(p) { var $x = ["Circ",7,p]; $x.__enum__ = js.fx.Transition; $x.toString = $estr; return $x; }
js.fx.Transition.Sine = function(p) { var $x = ["Sine",8,p]; $x.__enum__ = js.fx.Transition; $x.toString = $estr; return $x; }
js.fx.Transition.Back = function(p,pa) { var $x = ["Back",9,p,pa]; $x.__enum__ = js.fx.Transition; $x.toString = $estr; return $x; }
js.fx.Transition.Bounce = function(p) { var $x = ["Bounce",10,p]; $x.__enum__ = js.fx.Transition; $x.toString = $estr; return $x; }
js.fx.Transition.Elastic = function(p,pa) { var $x = ["Elastic",11,p,pa]; $x.__enum__ = js.fx.Transition; $x.toString = $estr; return $x; }
js.fx.TransitionFunctions = $hxClasses["js.fx.TransitionFunctions"] = function() { }
js.fx.TransitionFunctions.__name__ = ["js","fx","TransitionFunctions"];
js.fx.TransitionFunctions.transitionParam = function(p,f) {
	return (function($this) {
		var $r;
		switch( (p)[1] ) {
		case 0:
			$r = f;
			break;
		case 1:
			$r = function(pos) {
				return 1 - f(1 - pos);
			};
			break;
		case 2:
			$r = function(pos) {
				return pos <= 0.5?f(2 * pos) / 2:2 - f(2 * (1 - pos)) / 2;
			};
			break;
		}
		return $r;
	}(this));
}
js.fx.TransitionFunctions.get = function(t) {
	return (function($this) {
		var $r;
		var $e = (t);
		switch( $e[1] ) {
		case 0:
			$r = js.fx.TransitionFunctions.linear;
			break;
		case 1:
			var p = $e[2];
			$r = js.fx.TransitionFunctions.transitionParam(p,js.fx.TransitionFunctions.quad);
			break;
		case 2:
			var p = $e[2];
			$r = js.fx.TransitionFunctions.transitionParam(p,js.fx.TransitionFunctions.cubic);
			break;
		case 3:
			var p = $e[2];
			$r = js.fx.TransitionFunctions.transitionParam(p,js.fx.TransitionFunctions.quart);
			break;
		case 4:
			var p = $e[2];
			$r = js.fx.TransitionFunctions.transitionParam(p,js.fx.TransitionFunctions.quint);
			break;
		case 5:
			var p = $e[2];
			$r = (function(f,x) {
				return function(p1) {
					return f(x,p1);
				};
			})(js.fx.TransitionFunctions.pow,p);
			break;
		case 6:
			var p = $e[2];
			$r = js.fx.TransitionFunctions.transitionParam(p,js.fx.TransitionFunctions.expo);
			break;
		case 7:
			var p = $e[2];
			$r = js.fx.TransitionFunctions.transitionParam(p,js.fx.TransitionFunctions.circ);
			break;
		case 8:
			var p = $e[2];
			$r = js.fx.TransitionFunctions.transitionParam(p,js.fx.TransitionFunctions.sine);
			break;
		case 9:
			var pa = $e[3], p = $e[2];
			$r = js.fx.TransitionFunctions.transitionParam(p,(function(f1,pa1) {
				return function(p1) {
					return f1(pa1,p1);
				};
			})(js.fx.TransitionFunctions.back,pa));
			break;
		case 10:
			var p = $e[2];
			$r = js.fx.TransitionFunctions.transitionParam(p,js.fx.TransitionFunctions.bounce);
			break;
		case 11:
			var pa = $e[3], p = $e[2];
			$r = js.fx.TransitionFunctions.transitionParam(p,(function(f2,pa2) {
				return function(p1) {
					return f2(pa2,p1);
				};
			})(js.fx.TransitionFunctions.elastic,pa));
			break;
		}
		return $r;
	}(this));
}
js.fx.TransitionFunctions.linear = function(p) {
	return p;
}
js.fx.TransitionFunctions.pow = function(x,p) {
	if(x == null) x = 6.0;
	return Math.pow(p,x);
}
js.fx.TransitionFunctions.expo = function(p) {
	return Math.pow(2,8 * (p - 1));
}
js.fx.TransitionFunctions.circ = function(p) {
	return 1 - Math.sin(Math.acos(p));
}
js.fx.TransitionFunctions.sine = function(p) {
	return 1 - Math.sin((1 - p) * Math.PI / 2);
}
js.fx.TransitionFunctions.back = function(pa,p) {
	if(pa == null) pa = 1.618;
	return Math.pow(p,2) * ((pa + 1) * p - pa);
}
js.fx.TransitionFunctions.bounce = function(p) {
	var value = null;
	var a = 0.0;
	var b = 1.0;
	while(true) {
		if(p >= (7 - 4 * a) / 11) {
			value = -Math.pow((11 - 6 * a - 11 * p) / 4,2) + b * b;
			break;
		}
		a += b;
		b /= 2.0;
	}
	return value;
}
js.fx.TransitionFunctions.elastic = function(pa,p) {
	if(pa == null) pa = 1.0;
	return Math.pow(2,10 * --p) * Math.cos(20 * p * Math.PI * pa / 3);
}
js.fx.TransitionFunctions.quad = function(p) {
	return Math.pow(p,2);
}
js.fx.TransitionFunctions.cubic = function(p) {
	return Math.pow(p,3);
}
js.fx.TransitionFunctions.quart = function(p) {
	return Math.pow(p,4);
}
js.fx.TransitionFunctions.quint = function(p) {
	return Math.pow(p,5);
}
js.fx.TransitionParam = $hxClasses["js.fx.TransitionParam"] = { __ename__ : ["js","fx","TransitionParam"], __constructs__ : ["In","Out","InOut"] }
js.fx.TransitionParam.In = ["In",0];
js.fx.TransitionParam.In.toString = $estr;
js.fx.TransitionParam.In.__enum__ = js.fx.TransitionParam;
js.fx.TransitionParam.Out = ["Out",1];
js.fx.TransitionParam.Out.toString = $estr;
js.fx.TransitionParam.Out.__enum__ = js.fx.TransitionParam;
js.fx.TransitionParam.InOut = ["InOut",2];
js.fx.TransitionParam.InOut.toString = $estr;
js.fx.TransitionParam.InOut.__enum__ = js.fx.TransitionParam;
var mt = mt || {}
if(!mt.deepnight) mt.deepnight = {}
if(!mt.deepnight.deprecated) mt.deepnight.deprecated = {}
mt.deepnight.deprecated.JsChaos = $hxClasses["mt.deepnight.deprecated.JsChaos"] = function() { }
mt.deepnight.deprecated.JsChaos.__name__ = ["mt","deepnight","deprecated","JsChaos"];
mt.deepnight.deprecated.JsChaos.addWheelEvent = function(dom,cb) {
	mt.deepnight.deprecated.JsChaos.addEvent(dom,"DOMMouseScroll","onmousewheel",cb);
}
mt.deepnight.deprecated.JsChaos.addClickEvent = function(dom,cb) {
	mt.deepnight.deprecated.JsChaos.addEvent(dom,"click","click",cb);
}
mt.deepnight.deprecated.JsChaos.addEvent = function(dom,e1,e2,cb) {
	if(dom == null) throw "invalid DOM";
	if(dom.addEventListener) dom.addEventListener(e1,cb,false); else if(dom.attachEvent) dom.attachEvent(e2,cb,false);
}
mt.deepnight.deprecated.JsChaos.getWheelDelta = function(event) {
	var delta = 0.0;
	if(event.wheelDelta) {
		delta = event.wheelDelta / 120;
		if(js.Lib.window.opera) delta = -delta;
	} else if(event.detail) delta = -event.detail / 3;
	return Math.round(delta);
}
mt.deepnight.deprecated.JsChaos.blockEvent = function(event) {
	if(event == null) event = js.Lib.window.event;
	event.cancelBubble = true;
	event.returnValue = false;
	event.cancel = true;
	if(event.stopPropagation != null) event.stopPropagation();
	if(event.preventDefault != null) event.preventDefault();
	return false;
}
mt.deepnight.deprecated.JsChaos.reloadPage = function() {
	js.Lib.document.location.reload(true);
}
function $iterator(o) { if( o instanceof Array ) return function() { return HxOverrides.iter(o); }; return typeof(o.iterator) == 'function' ? $bind(o,o.iterator) : o.iterator; };
var $_;
function $bind(o,m) { var f = function(){ return f.method.apply(f.scope, arguments); }; f.scope = o; f.method = m; return f; };
if(Array.prototype.indexOf) HxOverrides.remove = function(a,o) {
	var i = a.indexOf(o);
	if(i == -1) return false;
	a.splice(i,1);
	return true;
}; else null;
if(String.prototype.cca == null) String.prototype.cca = String.prototype.charCodeAt;
Math.__name__ = ["Math"];
Math.NaN = Number.NaN;
Math.NEGATIVE_INFINITY = Number.NEGATIVE_INFINITY;
Math.POSITIVE_INFINITY = Number.POSITIVE_INFINITY;
$hxClasses.Math = Math;
Math.isFinite = function(i) {
	return isFinite(i);
};
Math.isNaN = function(i) {
	return isNaN(i);
};
String.prototype.__class__ = $hxClasses.String = String;
String.__name__ = ["String"];
Array.prototype.__class__ = $hxClasses.Array = Array;
Array.__name__ = ["Array"];
Date.prototype.__class__ = $hxClasses.Date = Date;
Date.__name__ = ["Date"];
var Int = $hxClasses.Int = { __name__ : ["Int"]};
var Dynamic = $hxClasses.Dynamic = { __name__ : ["Dynamic"]};
var Float = $hxClasses.Float = Number;
Float.__name__ = ["Float"];
var Bool = $hxClasses.Bool = Boolean;
Bool.__ename__ = ["Bool"];
var Class = $hxClasses.Class = { __name__ : ["Class"]};
var Enum = { };
var Void = $hxClasses.Void = { __ename__ : ["Void"]};
var q = window.jQuery;
js.JQuery = q;
q.fn.iterator = function() {
	return { pos : 0, j : this, hasNext : function() {
		return this.pos < this.j.length;
	}, next : function() {
		return $(this.j[this.pos++]);
	}};
};
if(typeof document != "undefined") js.Lib.document = document;
if(typeof window != "undefined") {
	js.Lib.window = window;
	js.Lib.window.onerror = function(msg,url,line) {
		var f = js.Lib.onerror;
		if(f == null) return false;
		return f(msg,[url + ":" + line]);
	};
}
JsMain.SIDE_HEIGHT = 200;
JsMain.ctx = new haxe.remoting.Context();
JsMain.cnx = null;
haxe.Serializer.USE_CACHE = false;
haxe.Serializer.USE_ENUM_INDEX = false;
haxe.Serializer.BASE64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789%:";
haxe.Unserializer.DEFAULT_RESOLVER = Type;
haxe.Unserializer.BASE64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789%:";
haxe.Unserializer.CODES = null;
haxe.remoting.ExternalConnection.connections = new Hash();
js.Lib.onerror = null;
js.fx.Style.REQUIRES_FILTERS = js.Lib.window.ActiveXObject != null;
js.fx.Tool.isIE = typeof document!='undefined' && document.all != null && typeof window!='undefined' && window.opera == null;
JsMain.main();
